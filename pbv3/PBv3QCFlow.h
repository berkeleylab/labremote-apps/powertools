#ifndef PBV3QCFLOW_H
#define PBV3QCFLOW_H

#include <memory>
#include <nlohmann/json.hpp>
#include <string>

#include "PBv3TB.h"

//! \brief Manages the QC flow of a single powerboard
/**
 * Tracks the following information about a powerboard being tested:
 *
 * - position in the test system
 * - serial number
 * - pass status of key tests
 *
 * The pass status of key tests (linPOL, BER, LV and HV) is used to
 * decide which subsequent tests to run in the QC flow.
 *
 * The class does not save any results to a file. Instead JSON objects
 * corresponding to a test result file are returned. The JSON results should
 * then be decorated with test run information and saved to the right location
 * by the calling program.
 */
class PBv3QCFlow {
 public:
    /**
     * \param pbNum Powerboard position on a panel
     * \param doHV Enable running tests involving the HV power supply
     * \param hbipc Enables Hybrid burn-in suite of tests (skips HV, OFout,
     * linPOL, expands AMAC ID acceptance)
     *
     */
    PBv3QCFlow(uint8_t num = 0, bool doHV = true, bool hbipc = false);

    //! \brief Get position of Powerboard on panel
    uint8_t num() const;

    //! \brief Get serial number
    std::string serial() const;

    //! \brief Set serial number
    void setSerial(const std::string &serial);

    //
    // Test runners

    //! \brief Runs a set of tests to check whether Powerboards are alive
    /**
     * An "alive" Powerboard requires a working linPOL and reliable
     * communication with the AMAC chap.
     *
     * The alive set of tests is defined as follows:
     *  1) Check if linPOL voltage is correct
     *      - Stop on error
     *  2) Check that the AMAC ID is correct
     *  3) Check that communication with AMAC is OK via BER
     *      - Stop on error
     *
     * This test should be run before any other tests as it
     * determine whether they can be run reliably.
     *
     * \param tb Testbench for controlling the powerboards
     *
     * \return Test result in json format.
     */
    nlohmann::json runAliveTests(std::shared_ptr<PBv3TB> tb);

    //! \brief Runs a set of tests to check basic functionality of a Powerboard
    /**
     * No tests are run if the BER test failed during basic tests.
     *
     * The basic set of tests is defined as follows:
     *  1) Test output of all pads
     *  2) Test DC/DC output voltage
     *      - Record problem with LV on error
     *  3) Test toggling of HVmux
     *      - Record problem with HV on error
     *
     * Tuning of the AMACv2 will be run, if necessary.
     *
     * This test should be run before any advanced functionality
     * tests as it enables/disables advanced LV/HV tests based
     * on their basic functionality check. By default, they are
     * disabled.
     *
     * HV tests can be disabled by `doHV`.
     *
     * \param tb Testbench for controlling the powerboards
     *
     * \return Test result in json format.
     */
    nlohmann::json runBasicTests(std::shared_ptr<PBv3TB> tb);

    //! \brief Runs a set of tests to character the powerboard
    /**
     * No tests are run if the BER test failed during basic tests.
     *
     * The following set of tests is rung:
     *  1) Vin response scan
     *  2) (if HV basic test passed) HV scan
     *  3) (if LV basic test passed) DC/DC efficiency scan
     *  4) AMAC AM offset
     *  5) AMAC AM slope
     *  6) AMAC AM Cur10V monitor
     *  7) AMAC AM Cur1V monitor
     *
     * Tuning of the AMACv2 will be run, if necessary.
     *
     * \param tb Testbench for controlling the powerboards
     *
     * \return Test result in json format.
     */
    nlohmann::json runAdvancedTests(std::shared_ptr<PBv3TB> tb);

    //! \brief Tune the powerboard settings
    /**
     * No tests are run if the BER test failed during basic tests.
     *
     * The tuning is also saved in the PBv3QCFlow instance and
     * appended to any test results.
     *
     * \param tb Testbench for controlling the powerboards
     *
     * \return Configuration file in json format
     */
    nlohmann::json runTuning(std::shared_ptr<PBv3TB> tb);

    //! \brief Return if is alive test was passed
    /**
     * This is needed to ensure that odd pbs are only tested, if even ones
     * passed the alive test (cannot enable OF otherwise)
     */
    bool didAliveTestPass();

 private:
    //
    // Test configuration

    //! \brief Powerboard position on test panel
    uint32_t m_num = 0;

    //! \brief Run HV tests
    bool m_doHV = true;

    //! \brief Run hybrid burn-in powerboard carrierboard tests
    bool m_hbipc = false;

    //! \brief Serial number of powerboard
    std::string m_serial;

    //
    // Test pass information

    //! \brief Last available AMAC configuration
    nlohmann::json m_config;

    //! \brief Passed alive funtionality tests
    bool m_passAlive = false;

    //! \brief Passed basic funtionality tests
    bool m_passBasic = false;

    //! \brief Passed OF test (AMAC is alive)
    bool m_passOF = false;

    //! \brief Passed communication test (BER and PADID)
    bool m_passCom = false;

    //! \brief Passed LV test
    bool m_passLV = false;

    //! \brief Passed HV test
    bool m_passHV = false;
};

#endif  // PBV3QCFLOW_H
