#include "PBv3TBMassive.h"

#include <chrono>
#include <cmath>
#include <thread>

#include "Logger.h"

// I2C
#include "I2CDevCom.h"
#include "PCA9548ACom.h"
#include "SPIDevCom.h"

// ADCs
#include "AD799X.h"
#include "LTC2451.h"
#include "MCP3428.h"

// DACs
#include "DAC5571.h"
#include "DAC5574.h"

// IO expanders
#include "MCP23017.h"

// Climate sensors
#include "NTCSensor.h"
#include "SHT85.h"

// Multiplexers
#include "ComIOException.h"
#include "DeviceCalibration.h"
#include "FileCalibration.h"
#include "LinearCalibration.h"
#include "OutOfRangeException.h"
#include "PGA11x.h"

void PBv3TBMassive::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "i2cdev") {
            m_i2cdev = kv.value();
        } else if (kv.key() == "hvcurrmuxdev") {
            m_hvcurrmuxdev = kv.value();
        } else if (kv.key() == "pbdev") {
            m_pbdev = kv.value();
        }
    }

    PBv3TB::setConfiguration(config);
}

void PBv3TBMassive::init() {
    initDevices();
    loadCalibrations();
}

void PBv3TBMassive::initDevices() {
    //
    // Create the I2C mappings

    // Multiplexers
    m_i2c_root = std::make_shared<I2CDevCom>(0x70, m_i2cdev);

    m_i2c_analogue = std::make_shared<PCA9548ACom>(0x4c, 1, m_i2c_root);
    m_mux_hv =
        std::make_shared<PGA11x>(std::make_shared<SPIDevCom>(m_hvcurrmuxdev));

    //
    // ADCs
    m_adc_lv0 = std::make_shared<AD799X>(
        3.3, AD799X::AD7997,
        std::make_shared<PCA9548ACom>(0x21, 5, m_i2c_root));
    m_adc_lv1 = std::make_shared<AD799X>(
        3.3, AD799X::AD7997,
        std::make_shared<PCA9548ACom>(0x22, 5, m_i2c_root));

    m_adc_lvset0 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x68, 5, m_i2c_root));
    m_adc_lvset1 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x6C, 5, m_i2c_root));
    m_adc_lvset2 = std::make_shared<MCP3428>(
        std::make_shared<PCA9548ACom>(0x6A, 5, m_i2c_root));

    m_adc_hv = std::make_shared<LTC2451>(
        3.25 / 3.24e3, std::make_shared<PCA9548ACom>(0x14, 0, m_i2c_root));

    // Carrier card
    m_sel_out = std::make_shared<MCP23017>(
        std::make_shared<PCA9548ACom>(0x20, 2, m_i2c_root));
    m_sel_of = std::make_shared<MCP23017>(
        std::make_shared<PCA9548ACom>(0x21, 2, m_i2c_root));

    //
    // DACs
    double maxcurr = 3.25 * 16.9 / (100 + 16.9) / 100e-3;
    m_dac_0 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9548ACom>(0x4C, 5, m_i2c_root));
    m_dac_1 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9548ACom>(0x4D, 5, m_i2c_root));
    m_dac_2 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9548ACom>(0x4E, 5, m_i2c_root));

    //
    // NTC's
    m_ntc0 =
        std::make_shared<NTCSensor>(ADC_NTC_CH0, m_adc_lv0, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc1 =
        std::make_shared<NTCSensor>(ADC_NTC_CH1, m_adc_lv0, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc2 =
        std::make_shared<NTCSensor>(ADC_NTC_CH2, m_adc_lv1, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc3 =
        std::make_shared<NTCSensor>(ADC_NTC_CH3, m_adc_lv1, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);

    //
    // SHT35 on active board
    m_sht = std::make_shared<SHT85>(
        std::make_shared<PCA9548ACom>(0x44, 4, m_i2c_root));

    //
    // CAL DAC
    setCalDAC(std::make_shared<DAC5571>(1.085, m_i2c_analogue));
}

void PBv3TBMassive::loadCalibrations() {
    //
    // Common power related measurements
    m_adc_pwr->setCalibration(
        PBV3_ADC_CH_VIN,
        std::make_shared<LinearCalibration>(3.25 * (35 + 90.9) / 35, 0x3FF));
    m_adc_pwr->setCalibration(
        PBV3_ADC_CH_VIN_CURR,
        std::make_shared<LinearCalibration>(3.25 / (50 * 0.005), 0x3FF));
    m_adc_pwr->setCalibration(
        PBV3_ADC_CH_P5V_CURR,
        std::make_shared<LinearCalibration>(3.25 / (50 * 0.1), 0x3FF));
    m_adc_pwr->setCalibration(
        PBV3_ADC_CH_M5V_CURR,
        std::make_shared<LinearCalibration>(3.25 / (50 * 0.1), 0x3FF));

    //
    // Load current measurement
    std::dynamic_pointer_cast<MCP3428>(m_adc_lvset0)
        ->setGain(MCP3428::Gain::x4);
    std::dynamic_pointer_cast<MCP3428>(m_adc_lvset1)
        ->setGain(MCP3428::Gain::x4);
    std::dynamic_pointer_cast<MCP3428>(m_adc_lvset2)
        ->setGain(MCP3428::Gain::x4);

    float loadmax = 2.048 / 4 / 100e-3;
    m_adc_lvset0->setCalibration(
        std::make_shared<LinearCalibration>(loadmax, 0x7FFF));
    m_adc_lvset1->setCalibration(
        std::make_shared<LinearCalibration>(loadmax, 0x7FFF));
    m_adc_lvset2->setCalibration(
        std::make_shared<LinearCalibration>(loadmax, 0x7FFF));
}

void PBv3TBMassive::powerTBOn() {
    // Carrier card
    if (checkCarrierCard()) {
        m_sel_of->setIO(0x0);
        m_sel_out->setIO(0x0);
        m_sel_out->setInternalPullUp(0x00FC);
        m_sel_of->setInternalPullUp(0xFFFF);

        m_sel_out->write(
            0xFF03);  // disable carrier card multiplexers (active low enable)
    } else {
        logger(logWARNING) << "Carrier card not plugged in.";
    }
}

bool PBv3TBMassive::setOFin(uint8_t pbNum, bool value) {
    if (pbNum >= m_pbs.size())
        throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);

    // have to go via the other AMAC in this case
    if (this->getPBPanelType() == PBTypeDef::PBType::EC_R3 &&
        !PBTypeDef::isPrimaryR3(this->getPBPanelType(), pbNum)) {
        logger(logINFO)
            << "Steering OF of secondary R3 AMAC connected to position "
            << (int)pbNum << " (R3_1) via primary AMAC connected to position "
            << (int)(pbNum - 1) << " (R3_0)";

        std::shared_ptr<AMACv2> otherAmac = this->getPB(pbNum - 1);

        // check if the other AMAC was initialised (which should have been done)
        if (!otherAmac->isCommIDSet()) {
            // throw an exception if not
            std::string errorMsg = "AMAC " + std::to_string(pbNum - 1) +
                                   " is not initialised, but this is needed to "
                                   "set the OF for AMAC " +
                                   std::to_string(pbNum);
            logger(logERROR) << errorMsg;
            return false;
        }
        otherAmac->wrField(&AMACv2RegMap::RstCntOF, value);
    } else {
        // Select the enable bit
        uint32_t bit = (pbNum > 1) ? (pbNum + 6) : (pbNum);
        uint32_t bitvalue = (value << bit);
        uint32_t bitmask = ~(1 << bit);

        uint32_t selval = m_sel_of->read();
        selval = (selval & bitmask) | bitvalue;
        m_sel_of->write(selval);
    }
    return true;
}

double PBv3TBMassive::getVin() { return m_adc_pwr->read(PBV3_ADC_CH_VIN); }

double PBv3TBMassive::getP5VCurrent() {
    return m_adc_pwr->read(PBV3_ADC_CH_P5V_CURR);
}

double PBv3TBMassive::getM5VCurrent() {
    return m_adc_pwr->read(PBV3_ADC_CH_M5V_CURR);
}

double PBv3TBMassive::getNTC(uint8_t ntc) {
    switch (ntc) {
        case 0:
            m_ntc0->read();
            return m_ntc0->temperature();
        case 1:
            m_ntc1->read();
            return m_ntc1->temperature();
        case 2:
            m_ntc2->read();
            return m_ntc2->temperature();
        case 3:
            m_ntc3->read();
            return m_ntc3->temperature();
        default:
            throw OutOfRangeException(ntc, 0, 3);
    }
}

double PBv3TBMassive::getActiveTemp() {
    m_sht->read();
    return m_sht->temperature();
}

double PBv3TBMassive::getActiveHum() {
    m_sht->read();
    return m_sht->humidity();
}

double PBv3TBMassive::getActiveDew() {
    m_sht->read();
    return m_sht->dewPoint();
}

std::shared_ptr<AMACv2> PBv3TBMassive::getPB(uint8_t pbNum) {
    if (pbNum >= m_pbs.size())
        throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);
    return m_pbs[pbNum];
}

void PBv3TBMassive::loadOn(uint8_t pbNum) {}

void PBv3TBMassive::loadOff(uint8_t pbNum) { setLoad(pbNum, 0); }

double PBv3TBMassive::setLoad(uint8_t pbNum, double load) {
    switch (pbNum) {
        case 0:
            return m_dac_0->set(DAC_CH_LOAD_PB0, load);
            break;

        case 1:
            return m_dac_0->set(DAC_CH_LOAD_PB1, load);
            break;

        case 2:
            return m_dac_0->set(DAC_CH_LOAD_PB2, load);
            break;

        case 3:
            return m_dac_0->set(DAC_CH_LOAD_PB3, load);
            break;

        case 4:
            return m_dac_1->set(DAC_CH_LOAD_PB4, load);
            break;

        case 5:
            return m_dac_1->set(DAC_CH_LOAD_PB5, load);
            break;

        case 6:
            return m_dac_1->set(DAC_CH_LOAD_PB6, load);
            break;

        case 7:
            return m_dac_1->set(DAC_CH_LOAD_PB7, load);
            break;

        case 8:
            return m_dac_2->set(DAC_CH_LOAD_PB8, load);
            break;

        case 9:
            return m_dac_2->set(DAC_CH_LOAD_PB9, load);
            break;

        default:
            // TODO throw exception
            return 0.;
    }
}

void PBv3TBMassive::setLoadCounts(uint8_t pbNum, uint32_t counts) {
    switch (pbNum) {
        case 0:
            m_dac_0->setCount(DAC_CH_LOAD_PB0, counts);
            break;

        case 1:
            m_dac_0->setCount(DAC_CH_LOAD_PB1, counts);
            break;

        case 2:
            m_dac_0->setCount(DAC_CH_LOAD_PB2, counts);
            break;

        case 3:
            m_dac_0->setCount(DAC_CH_LOAD_PB3, counts);
            break;

        case 4:
            m_dac_1->setCount(DAC_CH_LOAD_PB4, counts);
            break;

        case 5:
            m_dac_1->setCount(DAC_CH_LOAD_PB5, counts);
            break;

        case 6:
            m_dac_1->setCount(DAC_CH_LOAD_PB6, counts);
            break;

        case 7:
            m_dac_1->setCount(DAC_CH_LOAD_PB7, counts);
            break;

        case 8:
            m_dac_2->setCount(DAC_CH_LOAD_PB8, counts);
            break;

        case 9:
            m_dac_2->setCount(DAC_CH_LOAD_PB9, counts);
            break;

        default:
            // TODO throw exception
            break;
    }
}

double PBv3TBMassive::getLoad(uint8_t pbNum) {
    return 0.;  // TODO implement
}

double PBv3TBMassive::getVout(uint8_t pbNum) {
    if (pbNum >= m_pbs.size())
        throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);

    // select 1.5V output on carrier card mux
    setCarrierOutput(CARRIER_OUTPUT::VOUT);

    // Enable the output for the requested powerboard
    setCarrierOutputEnable(pbNum, true);

    // Read the value
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    double value = m_adc_common->read();

    // Disable the output for the requested powerboard
    setCarrierOutputEnable(pbNum, false);

    return value;
}

uint32_t PBv3TBMassive::getVoutCount(uint8_t pbNum) {
    switch (pbNum) {
        case 0:
            return m_adc_lv0->readCount(ADC_VOUT_PB0);
            break;

        case 1:
            return m_adc_lv0->readCount(ADC_VOUT_PB1);
            break;

        case 2:
            return m_adc_lv0->readCount(ADC_VOUT_PB2);
            break;

        case 3:
            return m_adc_lv0->readCount(ADC_VOUT_PB3);
            break;

        case 4:
            return m_adc_lv0->readCount(ADC_VOUT_PB4);
            break;

        case 5:
            return m_adc_lv0->readCount(ADC_VOUT_PB5);
            break;

        case 6:
            return m_adc_lv1->readCount(ADC_VOUT_PB6);
            break;

        case 7:
            return m_adc_lv1->readCount(ADC_VOUT_PB7);
            break;

        case 8:
            return m_adc_lv1->readCount(ADC_VOUT_PB8);
            break;

        case 9:
            return m_adc_lv1->readCount(ADC_VOUT_PB9);
            break;

        default:
            // TODO throw exception
            return 0.;
    }
}

double PBv3TBMassive::getIload(uint8_t pbNum) {
    switch (pbNum) {
        case 0:
            return m_adc_lvset0->read(ADC_ILOAD_PB0);
            break;

        case 1:
            return m_adc_lvset0->read(ADC_ILOAD_PB1);
            break;

        case 2:
            return m_adc_lvset0->read(ADC_ILOAD_PB2);
            break;

        case 3:
            return m_adc_lvset0->read(ADC_ILOAD_PB3);
            break;

        case 4:
            return m_adc_lvset1->read(ADC_ILOAD_PB4);
            break;

        case 5:
            return m_adc_lvset1->read(ADC_ILOAD_PB5);
            break;

        case 6:
            return m_adc_lvset1->read(ADC_ILOAD_PB6);
            break;

        case 7:
            return m_adc_lvset1->read(ADC_ILOAD_PB7);
            break;

        case 8:
            return m_adc_lvset2->read(ADC_ILOAD_PB8);
            break;

        case 9:
            return m_adc_lvset2->read(ADC_ILOAD_PB9);
            break;

        default:
            // TODO throw exception
            return 0.;
    }
}

double PBv3TBMassive::getHVoutCurrent(uint8_t pbNum) {
    if (pbNum >= m_pbs.size())
        throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);

    switch (pbNum) {
        case 0:
            m_mux_hv->select(5);
            break;

        case 1:
            m_mux_hv->select(4);
            break;

        case 2:
            m_mux_hv->select(3);
            break;

        case 3:
            m_mux_hv->select(2);
            break;

        case 4:
            m_mux_hv->select(1);
            break;

        case 5:
            m_mux_hv->select(0);
            break;

        case 6:
            m_mux_hv->select(7);
            break;

        case 7:
            m_mux_hv->select(6);
            break;

        case 8:
            m_mux_hv->select(9);
            break;

        case 9:
            m_mux_hv->select(8);
            break;

        default:
            throw OutOfRangeException(pbNum, 0, m_pbs.size() - 1);
            return 0.;
    }

    return m_adc_hv->read();
}

void PBv3TBMassive::setCarrierOutput(PBv3TB::CARRIER_OUTPUT value) {
    uint32_t selval = m_sel_out->read();
    selval = (selval & 0xFF0F) | ((value << 4) & 0x00F0);
    m_sel_out->write(selval);
}

void PBv3TBMassive::setCarrierOutputEnable(uint32_t pbNum, bool value) {
    // Select the enable bit
    uint32_t bit = (pbNum > 1) ? (pbNum + 6) : (pbNum);
    uint32_t bitvalue = ~(value << bit);

    uint32_t selval = m_sel_out->read();
    selval = (selval & 0x00F0) | ((bitvalue)&0xFF0F);
    m_sel_out->write(selval);

    // Select the GND value
    selval = m_sel_of->read();
    selval = (selval & 0xFF0F) | ((pbNum << 4) & 0x00F0);
    m_sel_of->write(selval);
}

double PBv3TBMassive::readCarrierOutput(uint32_t pbNum,
                                        PBv3TB::CARRIER_OUTPUT value) {
    // Selection mux value and enable output
    setCarrierOutput(value);
    setCarrierOutputEnable(pbNum, true);

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    double volts = m_adc_common->read();

    // Disable the output for the requested powerboard
    setCarrierOutputEnable(pbNum, false);

    return volts;
}

bool PBv3TBMassive::checkCarrierCard() {
    try {
        std::shared_ptr<I2CCom> testdevice =
            std::make_shared<PCA9548ACom>(0x20, 2, m_i2c_root);
        testdevice->read_reg8(0x00);
    } catch (const ComIOException &e) {
        return false;
    }
    return true;
}

std::shared_ptr<I2CCom> PBv3TBMassive::getI2CRoot() { return m_i2c_root; }
