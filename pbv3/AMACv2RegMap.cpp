#include "AMACv2RegMap.h"

#include <algorithm>

AMACv2RegMap::AMACv2RegMap() {
    RegisterStatus = {"Status", 0, RO};
    RegisterSerNum = {"SerNum", 31, RO};
    RegisterFlagResets = {"FlagResets", 32, WO};
    SerNum = {"SerNum", &RegisterSerNum, 0, 24, 0x0};
    FlagResetF = {"FlagResetF", &RegisterFlagResets, 16, 1, 0x0};
}

void AMACv2RegMap::init_regmap(Version version) {
    switch (version) {
        case Version::v2:
            init_v2();
            break;
        case Version::star:
            init_star();
            break;
        default:
            throw std::runtime_error("AMACv2RegMap: Unknown version");
            break;
    }
    //
    // List of all available registers
    m_registers = {&AMACv2RegMap::RegisterStatus,
                   &AMACv2RegMap::RegisterHxFlags,
                   &AMACv2RegMap::RegisterHyFlags,
                   &AMACv2RegMap::RegisterHyHxFlags,
                   &AMACv2RegMap::RegisterHV0Flags,
                   &AMACv2RegMap::RegisterHV2Flags,
                   &AMACv2RegMap::RegisterHV2HV0Flags,
                   &AMACv2RegMap::RegisterDCDCFlags,
                   &AMACv2RegMap::RegisterWRNHxFlags,
                   &AMACv2RegMap::RegisterWRNDCDCFlags,
                   &AMACv2RegMap::RegisterSynFlags,
                   &AMACv2RegMap::RegisterValue0,
                   &AMACv2RegMap::RegisterValue1,
                   &AMACv2RegMap::RegisterValue2,
                   &AMACv2RegMap::RegisterValue3,
                   &AMACv2RegMap::RegisterValue4,
                   &AMACv2RegMap::RegisterValue5,
                   &AMACv2RegMap::RegisterSEUstatus0,
                   &AMACv2RegMap::RegisterSEUstatus1,
                   &AMACv2RegMap::RegisterSerNum,
                   &AMACv2RegMap::RegisterFlagResets,
                   &AMACv2RegMap::RegisterLogicReset,
                   &AMACv2RegMap::RegisterHardReset,
                   &AMACv2RegMap::RegisterCntSet,
                   &AMACv2RegMap::RegisterCntSetC,
                   &AMACv2RegMap::RegisterDCDCen,
                   &AMACv2RegMap::RegisterDCDCenC,
                   &AMACv2RegMap::RegisterIlock,
                   &AMACv2RegMap::RegisterIlockC,
                   &AMACv2RegMap::RegisterRstCnt,
                   &AMACv2RegMap::RegisterRstCntC,
                   &AMACv2RegMap::RegisterAMen,
                   &AMACv2RegMap::RegisterAMenC,
                   &AMACv2RegMap::RegisterAMpwr,
                   &AMACv2RegMap::RegisterAMpwrC,
                   &AMACv2RegMap::RegisterBgCnt,
                   &AMACv2RegMap::RegisterAMCnt,
                   &AMACv2RegMap::RegisterDACs0,
                   &AMACv2RegMap::RegisterDACbias,
                   &AMACv2RegMap::RegisterAMACCnt,
                   &AMACv2RegMap::RegisterNTCRange,
                   &AMACv2RegMap::RegisterLVCurCal,
                   &AMACv2RegMap::RegisterHxICfg,
                   &AMACv2RegMap::RegisterHyICfg,
                   &AMACv2RegMap::RegisterHyHxICfg,
                   &AMACv2RegMap::RegisterHV0ICfg,
                   &AMACv2RegMap::RegisterHV2ICfg,
                   &AMACv2RegMap::RegisterHV2HV0ICfg,
                   &AMACv2RegMap::RegisterDCDCICfg,
                   &AMACv2RegMap::RegisterWRNICfg,
                   &AMACv2RegMap::RegisterWRNDCDCICfg,
                   &AMACv2RegMap::RegisterHxTLUT,
                   &AMACv2RegMap::RegisterHxModLUT1,
                   &AMACv2RegMap::RegisterHxModLUT2,
                   &AMACv2RegMap::RegisterHyTLUT,
                   &AMACv2RegMap::RegisterHyModLUT1,
                   &AMACv2RegMap::RegisterHyModLUT2,
                   &AMACv2RegMap::RegisterHV0TLUT,
                   &AMACv2RegMap::RegisterHV0ModLUT1,
                   &AMACv2RegMap::RegisterHV0ModLUT2,
                   &AMACv2RegMap::RegisterHV2TLUT,
                   &AMACv2RegMap::RegisterHV2ModLUT1,
                   &AMACv2RegMap::RegisterHV2ModLUT2,
                   &AMACv2RegMap::RegisterDCDCTLUT,
                   &AMACv2RegMap::RegisterDCDCModLUT1,
                   &AMACv2RegMap::RegisterDCDCModLUT2,
                   &AMACv2RegMap::RegisterWRNTLUT,
                   &AMACv2RegMap::RegisterWRNModLUT1,
                   &AMACv2RegMap::RegisterWRNModLUT2,
                   &AMACv2RegMap::RegisterHxFlagEn,
                   &AMACv2RegMap::RegisterHyFlagEn,
                   &AMACv2RegMap::RegisterHyHxFlagEn,
                   &AMACv2RegMap::RegisterHV0FlagEn,
                   &AMACv2RegMap::RegisterHV2FlagEn,
                   &AMACv2RegMap::RegisterHV2HV0FlagEn,
                   &AMACv2RegMap::RegisterDCDCFlagEn,
                   &AMACv2RegMap::RegisterSynFlagEn,
                   &AMACv2RegMap::RegisterWRNDCDCFlagEn,
                   &AMACv2RegMap::RegisterHxTh0,
                   &AMACv2RegMap::RegisterHxTh1,
                   &AMACv2RegMap::RegisterHxTh2,
                   &AMACv2RegMap::RegisterHxTh3,
                   &AMACv2RegMap::RegisterHyTh0,
                   &AMACv2RegMap::RegisterHyTh1,
                   &AMACv2RegMap::RegisterHyTh2,
                   &AMACv2RegMap::RegisterHyTh3,
                   &AMACv2RegMap::RegisterHV0Th0,
                   &AMACv2RegMap::RegisterHV0Th1,
                   &AMACv2RegMap::RegisterHV0Th2,
                   &AMACv2RegMap::RegisterHV0Th3,
                   &AMACv2RegMap::RegisterHV2Th0,
                   &AMACv2RegMap::RegisterHV2Th1,
                   &AMACv2RegMap::RegisterHV2Th2,
                   &AMACv2RegMap::RegisterHV2Th3,
                   &AMACv2RegMap::RegisterDCDCTh0,
                   &AMACv2RegMap::RegisterDCDCTh1,
                   &AMACv2RegMap::RegisterDCDCTh2,
                   &AMACv2RegMap::RegisterDCDCTh3,
                   &AMACv2RegMap::RegisterWRNTh0,
                   &AMACv2RegMap::RegisterWRNTh1,
                   &AMACv2RegMap::RegisterWRNTh2,
                   &AMACv2RegMap::RegisterWRNTh3,
                   &AMACv2RegMap::RegisterHxLoTh0,
                   &AMACv2RegMap::RegisterHxLoTh1,
                   &AMACv2RegMap::RegisterHxLoTh2,
                   &AMACv2RegMap::RegisterHxLoTh3,
                   &AMACv2RegMap::RegisterHxLoTh4,
                   &AMACv2RegMap::RegisterHxLoTh5,
                   &AMACv2RegMap::RegisterHxHiTh0,
                   &AMACv2RegMap::RegisterHxHiTh1,
                   &AMACv2RegMap::RegisterHxHiTh2,
                   &AMACv2RegMap::RegisterHxHiTh3,
                   &AMACv2RegMap::RegisterHxHiTh4,
                   &AMACv2RegMap::RegisterHxHiTh5,
                   &AMACv2RegMap::RegisterHyLoTh0,
                   &AMACv2RegMap::RegisterHyLoTh1,
                   &AMACv2RegMap::RegisterHyLoTh2,
                   &AMACv2RegMap::RegisterHyLoTh3,
                   &AMACv2RegMap::RegisterHyLoTh4,
                   &AMACv2RegMap::RegisterHyLoTh5,
                   &AMACv2RegMap::RegisterHyHiTh0,
                   &AMACv2RegMap::RegisterHyHiTh1,
                   &AMACv2RegMap::RegisterHyHiTh2,
                   &AMACv2RegMap::RegisterHyHiTh3,
                   &AMACv2RegMap::RegisterHyHiTh4,
                   &AMACv2RegMap::RegisterHyHiTh5,
                   &AMACv2RegMap::RegisterHV0LoTh0,
                   &AMACv2RegMap::RegisterHV0LoTh1,
                   &AMACv2RegMap::RegisterHV0LoTh2,
                   &AMACv2RegMap::RegisterHV0LoTh3,
                   &AMACv2RegMap::RegisterHV0LoTh4,
                   &AMACv2RegMap::RegisterHV0LoTh5,
                   &AMACv2RegMap::RegisterHV0HiTh0,
                   &AMACv2RegMap::RegisterHV0HiTh1,
                   &AMACv2RegMap::RegisterHV0HiTh2,
                   &AMACv2RegMap::RegisterHV0HiTh3,
                   &AMACv2RegMap::RegisterHV0HiTh4,
                   &AMACv2RegMap::RegisterHV0HiTh5,
                   &AMACv2RegMap::RegisterHV2LoTh0,
                   &AMACv2RegMap::RegisterHV2LoTh1,
                   &AMACv2RegMap::RegisterHV2LoTh2,
                   &AMACv2RegMap::RegisterHV2LoTh3,
                   &AMACv2RegMap::RegisterHV2LoTh4,
                   &AMACv2RegMap::RegisterHV2LoTh5,
                   &AMACv2RegMap::RegisterHV2HiTh0,
                   &AMACv2RegMap::RegisterHV2HiTh1,
                   &AMACv2RegMap::RegisterHV2HiTh2,
                   &AMACv2RegMap::RegisterHV2HiTh3,
                   &AMACv2RegMap::RegisterHV2HiTh4,
                   &AMACv2RegMap::RegisterHV2HiTh5,
                   &AMACv2RegMap::RegisterDCDCLoTh0,
                   &AMACv2RegMap::RegisterDCDCLoTh1,
                   &AMACv2RegMap::RegisterDCDCLoTh2,
                   &AMACv2RegMap::RegisterDCDCLoTh3,
                   &AMACv2RegMap::RegisterDCDCLoTh4,
                   &AMACv2RegMap::RegisterDCDCLoTh5,
                   &AMACv2RegMap::RegisterDCDCHiTh0,
                   &AMACv2RegMap::RegisterDCDCHiTh1,
                   &AMACv2RegMap::RegisterDCDCHiTh2,
                   &AMACv2RegMap::RegisterDCDCHiTh3,
                   &AMACv2RegMap::RegisterDCDCHiTh4,
                   &AMACv2RegMap::RegisterDCDCHiTh5,
                   &AMACv2RegMap::RegisterWRNLoTh0,
                   &AMACv2RegMap::RegisterWRNLoTh1,
                   &AMACv2RegMap::RegisterWRNLoTh2,
                   &AMACv2RegMap::RegisterWRNLoTh3,
                   &AMACv2RegMap::RegisterWRNLoTh4,
                   &AMACv2RegMap::RegisterWRNLoTh5,
                   &AMACv2RegMap::RegisterWRNHiTh0,
                   &AMACv2RegMap::RegisterWRNHiTh1,
                   &AMACv2RegMap::RegisterWRNHiTh2,
                   &AMACv2RegMap::RegisterWRNHiTh3,
                   &AMACv2RegMap::RegisterWRNHiTh4,
                   &AMACv2RegMap::RegisterWRNHiTh5};

    for (AMACv2Register AMACv2RegMap::*reg : m_registers) {
        m_regAddrMap[(this->*reg).getAddress()] = reg;
        m_regMap[(this->*reg).getName()] = reg;
    }

    //
    // List of all available fields
    m_fields = {
        // Status
        &AMACv2RegMap::StatusAM, &AMACv2RegMap::StatusWARN,
        &AMACv2RegMap::StatusDCDC, &AMACv2RegMap::StatusHV3,
        &AMACv2RegMap::StatusHV2, &AMACv2RegMap::StatusHV1,
        &AMACv2RegMap::StatusHV0, &AMACv2RegMap::StatusY2LDO,
        &AMACv2RegMap::StatusY1LDO, &AMACv2RegMap::StatusY0LDO,
        &AMACv2RegMap::StatusX2LDO, &AMACv2RegMap::StatusX1LDO,
        &AMACv2RegMap::StatusX0LDO, &AMACv2RegMap::StatusGPI,
        &AMACv2RegMap::StatusPGOOD, &AMACv2RegMap::StatusILockWARN,
        &AMACv2RegMap::StatusILockDCDC, &AMACv2RegMap::StatusILockHV2,
        &AMACv2RegMap::StatusILockHV0, &AMACv2RegMap::StatusILockYLDO,
        &AMACv2RegMap::StatusILockxLDO, &AMACv2RegMap::AMACVersion,
        // HxFlags
        &AMACv2RegMap::HxFlagsHi, &AMACv2RegMap::HxFlagsLo,
        &AMACv2RegMap::HxChFlags,
        // HyFlags
        &AMACv2RegMap::HyFlagsHi, &AMACv2RegMap::HyFlagsLo,
        &AMACv2RegMap::HyChFlags,
        // HV0Flags
        &AMACv2RegMap::HV0FlagsHi, &AMACv2RegMap::HV0FlagsLo,
        &AMACv2RegMap::HV0ChFlags,
        // HV2Flags
        &AMACv2RegMap::HV2FlagsHi, &AMACv2RegMap::HV2FlagsLo,
        &AMACv2RegMap::HV2ChFlags,
        // DCDCFlags
        &AMACv2RegMap::DCDCflagsHi, &AMACv2RegMap::DCDCflagsLo,
        &AMACv2RegMap::DCDCChFlags,
        // WRNHxFlags
        &AMACv2RegMap::WRNflagsHi, &AMACv2RegMap::WRNflagsLo,
        &AMACv2RegMap::WRNChFlags,
        // SynFlags
        &AMACv2RegMap::SynFlagsHx, &AMACv2RegMap::SynFlagsHy,
        &AMACv2RegMap::SynFlagsHV0, &AMACv2RegMap::SynFlagsHV2,
        &AMACv2RegMap::SynFlagsDCDC, &AMACv2RegMap::SynFlagsWRN,
        // Value0
        &AMACv2RegMap::Value0AMen, &AMACv2RegMap::Ch0Value,
        &AMACv2RegMap::Ch1Value, &AMACv2RegMap::Ch2Value,
        // Value1
        &AMACv2RegMap::Value1AMen, &AMACv2RegMap::Ch3Value,
        &AMACv2RegMap::Ch4Value, &AMACv2RegMap::Ch5Value,
        // Value2
        &AMACv2RegMap::Value2AMen, &AMACv2RegMap::Ch6Value,
        &AMACv2RegMap::Ch7Value, &AMACv2RegMap::Ch8Value,
        // Value3
        &AMACv2RegMap::Value3AMen, &AMACv2RegMap::Ch9Value,
        &AMACv2RegMap::Ch10Value, &AMACv2RegMap::Ch11Value,
        // Value4
        &AMACv2RegMap::Value4AMen, &AMACv2RegMap::Ch12Value,
        &AMACv2RegMap::Ch13Value, &AMACv2RegMap::Ch14Value,
        &AMACv2RegMap::Ch12S, &AMACv2RegMap::Ch13S,
        // Value5
        &AMACv2RegMap::Value5AMen, &AMACv2RegMap::Ch15Value,
        &AMACv2RegMap::Ch14S, &AMACv2RegMap::Ch15S,
        // SEUstatus
        &AMACv2RegMap::SEUStatusLo, &AMACv2RegMap::SEUStatusHi,
        // SerNum
        &AMACv2RegMap::PadID, &AMACv2RegMap::SerNum,
        // FlagResets
        &AMACv2RegMap::FlagResetOe, &AMACv2RegMap::FlagResetF,
        &AMACv2RegMap::FlagResetS, &AMACv2RegMap::FlagResetWRN,
        &AMACv2RegMap::FlagResetDCDC, &AMACv2RegMap::FlagResetHV2,
        &AMACv2RegMap::FlagResetHV0, &AMACv2RegMap::FlagResetXLDO,
        &AMACv2RegMap::FlagResetYLDO,
        // LogicReset
        &AMACv2RegMap::LogicReset,
        // HardReset
        &AMACv2RegMap::HardReset,
        // CntSet
        &AMACv2RegMap::CntSetHV3frq, &AMACv2RegMap::CntSetHV3en,
        &AMACv2RegMap::CntSetHV2frq, &AMACv2RegMap::CntSetHV2en,
        &AMACv2RegMap::CntSetHV1frq, &AMACv2RegMap::CntSetHV1en,
        &AMACv2RegMap::CntSetHV0frq, &AMACv2RegMap::CntSetHV0en,
        &AMACv2RegMap::CntSetHyLDO2en, &AMACv2RegMap::CntSetHyLDO1en,
        &AMACv2RegMap::CntSetHyLDO0en, &AMACv2RegMap::CntSetHxLDO2en,
        &AMACv2RegMap::CntSetHxLDO1en, &AMACv2RegMap::CntSetHxLDO0en,
        &AMACv2RegMap::CntSetWARN,
        // CntSetC
        &AMACv2RegMap::CntSetCHV3frq, &AMACv2RegMap::CntSetCHV3en,
        &AMACv2RegMap::CntSetCHV2frq, &AMACv2RegMap::CntSetCHV2en,
        &AMACv2RegMap::CntSetCHV1frq, &AMACv2RegMap::CntSetCHV1en,
        &AMACv2RegMap::CntSetCHV0frq, &AMACv2RegMap::CntSetCHV0en,
        &AMACv2RegMap::CntSetCHyLDO2en, &AMACv2RegMap::CntSetCHyLDO1en,
        &AMACv2RegMap::CntSetCHyLDO0en, &AMACv2RegMap::CntSetCHxLDO2en,
        &AMACv2RegMap::CntSetCHxLDO1en, &AMACv2RegMap::CntSetCHxLDO0en,
        &AMACv2RegMap::CntSetCWARN,
        // DCDCen
        &AMACv2RegMap::DCDCAdj, &AMACv2RegMap::DCDCen,
        // DCDCenC
        &AMACv2RegMap::DCDCAdjC, &AMACv2RegMap::DCDCenC,
        // Ilock
        &AMACv2RegMap::IlockHx, &AMACv2RegMap::IlockHy, &AMACv2RegMap::IlockHV0,
        &AMACv2RegMap::IlockHV2, &AMACv2RegMap::IlockDCDC,
        &AMACv2RegMap::IlockWRN,
        // IlockC
        &AMACv2RegMap::IlockCHx, &AMACv2RegMap::IlockCHy,
        &AMACv2RegMap::IlockCHV0, &AMACv2RegMap::IlockCHV2,
        &AMACv2RegMap::IlockCDCDC, &AMACv2RegMap::IlockCWRN,
        // RstCnt
        &AMACv2RegMap::RstCntHyHCCresetB, &AMACv2RegMap::RstCntHxHCCresetB,
        &AMACv2RegMap::RstCntOF,
        // RstCntC
        &AMACv2RegMap::RstCntCHyHCCresetB, &AMACv2RegMap::RstCntCHxHCCresetB,
        &AMACv2RegMap::RstCntCOF,
        // AMen
        &AMACv2RegMap::AMzeroCalib, &AMACv2RegMap::AMen,
        // AMenC
        &AMACv2RegMap::AMzeroCalibC, &AMACv2RegMap::AMenC,
        // AMpwr
        &AMACv2RegMap::ClockDisableEn, &AMACv2RegMap::ReqDCDCPGOOD,
        &AMACv2RegMap::DCDCenToPwrAMAC,
        // AMpwrC
        &AMACv2RegMap::ReqDCDCPGOODC, &AMACv2RegMap::DCDCenToPwrAMACC,
        // BgCnt
        &AMACv2RegMap::AMbgen, &AMACv2RegMap::AMbgSW, &AMACv2RegMap::AMbg,
        &AMACv2RegMap::VDDbgen, &AMACv2RegMap::VDDbgSW, &AMACv2RegMap::VDDbg,
        // AMCnt
        &AMACv2RegMap::AMCntRg, &AMACv2RegMap::AMintCalib,
        &AMACv2RegMap::Ch15Mux, &AMACv2RegMap::Ch14Mux, &AMACv2RegMap::Ch13Mux,
        &AMACv2RegMap::Ch12Mux, &AMACv2RegMap::Ch12AMux, &AMACv2RegMap::Ch5Mux,
        &AMACv2RegMap::Ch4Mux, &AMACv2RegMap::Ch3Mux,
        // DACs0
        &AMACv2RegMap::DACShunty, &AMACv2RegMap::DACShuntx,
        &AMACv2RegMap::DACCaly, &AMACv2RegMap::DACCalx,
        // DACbias
        &AMACv2RegMap::DACbias, &AMACv2RegMap::ShuntEn,
        // AMACCnt
        &AMACv2RegMap::eFusebitSelect, &AMACv2RegMap::HVcurGain,
        &AMACv2RegMap::DRcomMode, &AMACv2RegMap::DRcurr, &AMACv2RegMap::ClkDis,
        &AMACv2RegMap::RingOscFrq,
        // NTCRange
        &AMACv2RegMap::CTAToffset, &AMACv2RegMap::NTCpbCal,
        &AMACv2RegMap::NTCpbSenseRange, &AMACv2RegMap::NTCy0Cal,
        &AMACv2RegMap::NTCy0SenseRange, &AMACv2RegMap::NTCx0Cal,
        &AMACv2RegMap::NTCx0SenseRange,
        // LVCurCal
        &AMACv2RegMap::DCDCoOffset, &AMACv2RegMap::DCDCoZeroReading,
        &AMACv2RegMap::DCDCoN, &AMACv2RegMap::DCDCoP,
        &AMACv2RegMap::DCDCiZeroReading, &AMACv2RegMap::DCDCiRangeSW,
        &AMACv2RegMap::DCDCiOffset, &AMACv2RegMap::DCDCiP,
        &AMACv2RegMap::DCDCiN,
        // HxICfg
        &AMACv2RegMap::HxFlagValid, &AMACv2RegMap::HxFlagValidEn,
        &AMACv2RegMap::HxFlagsLogic, &AMACv2RegMap::HxFlagsLatch,
        &AMACv2RegMap::HxLAM,
        // HyICfg
        &AMACv2RegMap::HyFlagValid, &AMACv2RegMap::HyFlagValidEn,
        &AMACv2RegMap::HyFlagsLogic, &AMACv2RegMap::HyFlagsLatch,
        &AMACv2RegMap::HyLAM,
        // HV0ICfg
        &AMACv2RegMap::HV0FlagValid, &AMACv2RegMap::HV0FlagValidEn,
        &AMACv2RegMap::HV0FlagsLogic, &AMACv2RegMap::HV0FlagsLatch,
        &AMACv2RegMap::HV0LAM,
        // HV2ICfg
        &AMACv2RegMap::HV2FlagValid, &AMACv2RegMap::HV2FlagValidEn,
        &AMACv2RegMap::HV2FlagsLogic, &AMACv2RegMap::HV2FlagsLatch,
        &AMACv2RegMap::HV2LAM,
        // DCDCICfg
        &AMACv2RegMap::DCDCFlagValid, &AMACv2RegMap::DCDCFlagValidEn,
        &AMACv2RegMap::DCDCFlagsLogic, &AMACv2RegMap::DCDCFlagsLatch,
        &AMACv2RegMap::DCDCLAM,
        // WRNICfg
        &AMACv2RegMap::WRNFlagValid, &AMACv2RegMap::WRNFlagValidEn,
        &AMACv2RegMap::WRNFlagsLogic, &AMACv2RegMap::WRNFlagsLatch,
        &AMACv2RegMap::WRNLAM,
        // HxTLUT
        &AMACv2RegMap::HxTlut,
        // HxModLUT1
        &AMACv2RegMap::HxModlut1,
        // HxModLUT2
        &AMACv2RegMap::HxModlut2,
        // HyTLUT
        &AMACv2RegMap::HyTlut,
        // HyModLUT1
        &AMACv2RegMap::HyModlut1,
        // HyModLUT2
        &AMACv2RegMap::HyModlut2,
        // HV0TLUT
        &AMACv2RegMap::HV0Tlut,
        // HV0ModLUT1
        &AMACv2RegMap::HV0Modlut1,
        // HV0ModLUT2
        &AMACv2RegMap::HV0Modlut2,
        // HV2TLUT
        &AMACv2RegMap::HV2Tlut,
        // HV2ModLUT1
        &AMACv2RegMap::HV2Modlut1,
        // HV2ModLUT2
        &AMACv2RegMap::HV2Modlut2,
        // DCDCTLUT
        &AMACv2RegMap::DCDCTlut,
        // DCDCModLUT1
        &AMACv2RegMap::DCDCModlut1,
        // DCDCModLUT2
        &AMACv2RegMap::DCDCModlut2,
        // WRNTLUT
        &AMACv2RegMap::WRNTlut,
        // WRNModLUT1
        &AMACv2RegMap::WRNModlut1,
        // WRNModLUT2
        &AMACv2RegMap::WRNModlut2,
        // HxFlagEn
        &AMACv2RegMap::HxFlagsEnHi, &AMACv2RegMap::HxFlagsEnLo,
        &AMACv2RegMap::HxILockFlagEn,
        // HyFlagEn
        &AMACv2RegMap::HyFlagsEnHi, &AMACv2RegMap::HyFlagsEnLo,
        &AMACv2RegMap::HyILockFlagEn,
        // HV0FlagEn
        &AMACv2RegMap::HV0FlagsEnHi, &AMACv2RegMap::HV0FlagsEnLo,
        &AMACv2RegMap::HV0ILockFlagEn,
        // HV2FlagEn
        &AMACv2RegMap::HV2FlagsEnHi, &AMACv2RegMap::HV2FlagsEnLo,
        &AMACv2RegMap::HV2ILockFlagEn,
        // DCDCFlagEn
        &AMACv2RegMap::DCDCFlagsEnHi, &AMACv2RegMap::DCDCFlagsEnLo,
        &AMACv2RegMap::DCDCILockFlagEn,
        // SynFlagEn
        &AMACv2RegMap::HyFlagEnSynY, &AMACv2RegMap::HxFlagEnSynX,
        &AMACv2RegMap::HV2FlagEnSynH2, &AMACv2RegMap::HV0FlagEnSynH0,
        &AMACv2RegMap::WRNDCDCFlagEnSynW, &AMACv2RegMap::WRNDCDCFlagEnSynDc,
        &AMACv2RegMap::WRNFlagsEnHi, &AMACv2RegMap::WRNFlagsEnLo,
        &AMACv2RegMap::WARNILockFlagEn, &AMACv2RegMap::WRNsynFlagEnHi,
        &AMACv2RegMap::WRNsynFlagEnLo, &AMACv2RegMap::DCDCsynFlagEnHi,
        &AMACv2RegMap::DCDCsynFlagEnLo, &AMACv2RegMap::HV2synFlagEnHi,
        &AMACv2RegMap::HV2synFlagEnLo, &AMACv2RegMap::HV0synFlagEnHi,
        &AMACv2RegMap::HV0synFlagEnLo, &AMACv2RegMap::HysynFlagEnHi,
        &AMACv2RegMap::HysynFlagEnLo, &AMACv2RegMap::HxsynFlagEnHi,
        &AMACv2RegMap::HxsynFlagEnLo,
        // HxLoTh0
        &AMACv2RegMap::HxLoThCh0, &AMACv2RegMap::HxLoThCh1,
        &AMACv2RegMap::HxLoThCh2,
        // HxLoTh1
        &AMACv2RegMap::HxLoThCh3, &AMACv2RegMap::HxLoThCh4,
        &AMACv2RegMap::HxLoThCh5,
        // HxLoTh2
        &AMACv2RegMap::HxLoThCh6, &AMACv2RegMap::HxLoThCh7,
        &AMACv2RegMap::HxLoThCh8,
        // HxLoTh3
        &AMACv2RegMap::HxLoThCh9, &AMACv2RegMap::HxLoThCh10,
        &AMACv2RegMap::HxLoThCh11,
        // HxLoTh4
        &AMACv2RegMap::HxLoThCh12, &AMACv2RegMap::HxLoThCh13,
        &AMACv2RegMap::HxLoThCh14,
        // HxLoTh5
        &AMACv2RegMap::HxLoThCh15,
        // HxHiTh0
        &AMACv2RegMap::HxHiThCh0, &AMACv2RegMap::HxHiThCh1,
        &AMACv2RegMap::HxHiThCh2,
        // HxHiTh1
        &AMACv2RegMap::HxHiThCh3, &AMACv2RegMap::HxHiThCh4,
        &AMACv2RegMap::HxHiThCh5,
        // HxHiTh2
        &AMACv2RegMap::HxHiThCh6, &AMACv2RegMap::HxHiThCh7,
        &AMACv2RegMap::HxHiThCh8,
        // HxHiTh3
        &AMACv2RegMap::HxHiThCh9, &AMACv2RegMap::HxHiThCh10,
        &AMACv2RegMap::HxHiThCh11,
        // HxHiTh4
        &AMACv2RegMap::HxHiThCh12, &AMACv2RegMap::HxHiThCh13,
        &AMACv2RegMap::HxHiThCh14,
        // HxHiTh5
        &AMACv2RegMap::HxHiThCh15,
        // HyLoTh0
        &AMACv2RegMap::HyLoThCh0, &AMACv2RegMap::HyLoThCh1,
        &AMACv2RegMap::HyLoThCh2,
        // HyLoTh1
        &AMACv2RegMap::HyLoThCh3, &AMACv2RegMap::HyLoThCh4,
        &AMACv2RegMap::HyLoThCh5,
        // HyLoTh2
        &AMACv2RegMap::HyLoThCh6, &AMACv2RegMap::HyLoThCh7,
        &AMACv2RegMap::HyLoThCh8,
        // HyLoTh3
        &AMACv2RegMap::HyLoThCh9, &AMACv2RegMap::HyLoThCh10,
        &AMACv2RegMap::HyLoThCh11,
        // HyLoTh4
        &AMACv2RegMap::HyLoThCh12, &AMACv2RegMap::HyLoThCh13,
        &AMACv2RegMap::HyLoThCh14,
        // HyLoTh5
        &AMACv2RegMap::HyLoThCh15,
        // HyHiTh0
        &AMACv2RegMap::HyHiThCh0, &AMACv2RegMap::HyHiThCh1,
        &AMACv2RegMap::HyHiThCh2,
        // HyHiTh1
        &AMACv2RegMap::HyHiThCh3, &AMACv2RegMap::HyHiThCh4,
        &AMACv2RegMap::HyHiThCh5,
        // HyHiTh2
        &AMACv2RegMap::HyHiThCh6, &AMACv2RegMap::HyHiThCh7,
        &AMACv2RegMap::HyHiThCh8,
        // HyHiTh3
        &AMACv2RegMap::HyHiThCh9, &AMACv2RegMap::HyHiThCh10,
        &AMACv2RegMap::HyHiThCh11,
        // HyHiTh4
        &AMACv2RegMap::HyHiThCh12, &AMACv2RegMap::HyHiThCh13,
        &AMACv2RegMap::HyHiThCh14,
        // HyHiTh5
        &AMACv2RegMap::HyHiThCh15,
        // HV0LoTh0
        &AMACv2RegMap::HV0LoThCh0, &AMACv2RegMap::HV0LoThCh1,
        &AMACv2RegMap::HV0LoThCh2,
        // HV0LoTh1
        &AMACv2RegMap::HV0LoThCh3, &AMACv2RegMap::HV0LoThCh4,
        &AMACv2RegMap::HV0LoThCh5,
        // HV0LoTh2
        &AMACv2RegMap::HV0LoThCh6, &AMACv2RegMap::HV0LoThCh7,
        &AMACv2RegMap::HV0LoThCh8,
        // HV0LoTh3
        &AMACv2RegMap::HV0LoThCh9, &AMACv2RegMap::HV0LoThCh10,
        &AMACv2RegMap::HV0LoThCh11,
        // HV0LoTh4
        &AMACv2RegMap::HV0LoThCh12, &AMACv2RegMap::HV0LoThCh13,
        &AMACv2RegMap::HV0LoThCh14,
        // HV0LoTh5
        &AMACv2RegMap::HV0LoThCh15,
        // HV0HiTh0
        &AMACv2RegMap::HV0HiThCh0, &AMACv2RegMap::HV0HiThCh1,
        &AMACv2RegMap::HV0HiThCh2,
        // HV0HiTh1
        &AMACv2RegMap::HV0HiThCh3, &AMACv2RegMap::HV0HiThCh4,
        &AMACv2RegMap::HV0HiThCh5,
        // HV0HiTh2
        &AMACv2RegMap::HV0HiThCh6, &AMACv2RegMap::HV0HiThCh7,
        &AMACv2RegMap::HV0HiThCh8,
        // HV0HiTh3
        &AMACv2RegMap::HV0HiThCh9, &AMACv2RegMap::HV0HiThCh10,
        &AMACv2RegMap::HV0HiThCh11,
        // HV0HiTh4
        &AMACv2RegMap::HV0HiThCh12, &AMACv2RegMap::HV0HiThCh13,
        &AMACv2RegMap::HV0HiThCh14,
        // HV0HiTh5
        &AMACv2RegMap::HV0HiThCh15,
        // HV2HiTh0
        &AMACv2RegMap::HV2LoThCh0, &AMACv2RegMap::HV2LoThCh1,
        &AMACv2RegMap::HV2LoThCh2,
        // HV2LoTh1
        &AMACv2RegMap::HV2LoThCh3, &AMACv2RegMap::HV2LoThCh4,
        &AMACv2RegMap::HV2LoThCh5,
        // HV2LoTh2
        &AMACv2RegMap::HV2LoThCh6, &AMACv2RegMap::HV2LoThCh7,
        &AMACv2RegMap::HV2LoThCh8,
        // HV2LoTh3
        &AMACv2RegMap::HV2LoThCh9, &AMACv2RegMap::HV2LoThCh10,
        &AMACv2RegMap::HV2LoThCh11,
        // HV2LoTh4
        &AMACv2RegMap::HV2LoThCh12, &AMACv2RegMap::HV2LoThCh13,
        &AMACv2RegMap::HV2LoThCh14,
        // HV2LoTh5
        &AMACv2RegMap::HV2LoThCh15,
        // HV2HiTh0
        &AMACv2RegMap::HV2HiThCh0, &AMACv2RegMap::HV2HiThCh1,
        &AMACv2RegMap::HV2HiThCh2,
        // HV2HiTh1
        &AMACv2RegMap::HV2HiThCh3, &AMACv2RegMap::HV2HiThCh4,
        &AMACv2RegMap::HV2HiThCh5,
        // HV2HiTh2
        &AMACv2RegMap::HV2HiThCh6, &AMACv2RegMap::HV2HiThCh7,
        &AMACv2RegMap::HV2HiThCh8,
        // HV2HiTh3
        &AMACv2RegMap::HV2HiThCh9, &AMACv2RegMap::HV2HiThCh10,
        &AMACv2RegMap::HV2HiThCh11,
        // HV2HiTh4
        &AMACv2RegMap::HV2HiThCh12, &AMACv2RegMap::HV2HiThCh13,
        &AMACv2RegMap::HV2HiThCh14,
        // HV2HiTh5
        &AMACv2RegMap::HV2HiThCh15,
        // DCDCLoTh0
        &AMACv2RegMap::DCDCLoThCh0, &AMACv2RegMap::DCDCLoThCh1,
        &AMACv2RegMap::DCDCLoThCh2,
        // DCDCLoTh1
        &AMACv2RegMap::DCDCLoThCh3, &AMACv2RegMap::DCDCLoThCh4,
        &AMACv2RegMap::DCDCLoThCh5,
        // DCDCLoTh2
        &AMACv2RegMap::DCDCLoThCh6, &AMACv2RegMap::DCDCLoThCh7,
        &AMACv2RegMap::DCDCLoThCh8,
        // DCDCLoTh3
        &AMACv2RegMap::DCDCLoThCh9, &AMACv2RegMap::DCDCLoThCh10,
        &AMACv2RegMap::DCDCLoThCh11,
        // DCDCLoTh4
        &AMACv2RegMap::DCDCLoThCh12, &AMACv2RegMap::DCDCLoThCh13,
        &AMACv2RegMap::DCDCLoThCh14,
        // DCDCLoTh5
        &AMACv2RegMap::DCDCLoThCh15,
        // DCDCHiTh0
        &AMACv2RegMap::DCDCHiThCh0, &AMACv2RegMap::DCDCHiThCh1,
        &AMACv2RegMap::DCDCHiThCh2,
        // DCDCHiTh1
        &AMACv2RegMap::DCDCHiThCh3, &AMACv2RegMap::DCDCHiThCh4,
        &AMACv2RegMap::DCDCHiThCh5,
        // DCDCHiTh2
        &AMACv2RegMap::DCDCHiThCh6, &AMACv2RegMap::DCDCHiThCh7,
        &AMACv2RegMap::DCDCHiThCh8,
        // DCDCHiTh3
        &AMACv2RegMap::DCDCHiThCh9, &AMACv2RegMap::DCDCHiThCh10,
        &AMACv2RegMap::DCDCHiThCh11,
        // DCDCHiTh4
        &AMACv2RegMap::DCDCHiThCh12, &AMACv2RegMap::DCDCHiThCh13,
        &AMACv2RegMap::DCDCHiThCh14,
        // DCDCHiTh5
        &AMACv2RegMap::DCDCHiThCh15,
        // WRNLoTh0
        &AMACv2RegMap::WRNLoThCh0, &AMACv2RegMap::WRNLoThCh1,
        &AMACv2RegMap::WRNLoThCh2,
        // WRNLoTh1
        &AMACv2RegMap::WRNLoThCh3, &AMACv2RegMap::WRNLoThCh4,
        &AMACv2RegMap::WRNLoThCh5,
        // WRNLoTh2
        &AMACv2RegMap::WRNLoThCh6, &AMACv2RegMap::WRNLoThCh7,
        &AMACv2RegMap::WRNLoThCh8,
        // WRNLoTh3
        &AMACv2RegMap::WRNLoThCh9, &AMACv2RegMap::WRNLoThCh10,
        &AMACv2RegMap::WRNLoThCh11,
        // WRNLoTh4
        &AMACv2RegMap::WRNLoThCh12, &AMACv2RegMap::WRNLoThCh13,
        &AMACv2RegMap::WRNLoThCh14,
        // WRNLoTh5
        &AMACv2RegMap::WRNLoThCh15,
        // WRNHiTh0
        &AMACv2RegMap::WRNHiThCh0, &AMACv2RegMap::WRNHiThCh1,
        &AMACv2RegMap::WRNHiThCh2,
        // WRNHiTh1
        &AMACv2RegMap::WRNHiThCh3, &AMACv2RegMap::WRNHiThCh4,
        &AMACv2RegMap::WRNHiThCh5,
        // WRNHiTh2
        &AMACv2RegMap::WRNHiThCh6, &AMACv2RegMap::WRNHiThCh7,
        &AMACv2RegMap::WRNHiThCh8,
        // WRNHiTh3
        &AMACv2RegMap::WRNHiThCh9, &AMACv2RegMap::WRNHiThCh10,
        &AMACv2RegMap::WRNHiThCh11,
        // WRNHiTh4
        &AMACv2RegMap::WRNHiThCh12, &AMACv2RegMap::WRNHiThCh13,
        &AMACv2RegMap::WRNHiThCh14,
        // WRNHiTh5
        &AMACv2RegMap::WRNHiThCh15};

    for (AMACv2Field AMACv2RegMap::*field : m_fields)
        m_fieldMap[(this->*field).getName()] = field;
}

std::vector<const AMACv2Register *> AMACv2RegMap::getRegisters() const {
    std::vector<const AMACv2Register *> registers(m_registers.size(), nullptr);
    std::transform(
        m_registers.cbegin(), m_registers.cend(), registers.begin(),
        [this](AMACv2Register AMACv2RegMap::*reg) { return (&(this->*reg)); });
    return registers;
}

std::vector<const AMACv2Field *> AMACv2RegMap::getFields() const {
    std::vector<const AMACv2Field *> fields;
    fields.reserve(m_fieldMap.size());
    for (AMACv2Field AMACv2RegMap::*field : m_fields)
        fields.push_back(&(this->*field));
    return fields;
}

AMACv2Field *AMACv2RegMap::findField(AMACv2Field AMACv2RegMap::*ref) {
    return &(this->*ref);
}

AMACv2Field *AMACv2RegMap::findField(const std::string &fieldName) {
    if (m_fieldMap.find(fieldName) != m_fieldMap.end())
        return &(this->*m_fieldMap[fieldName]);

    return nullptr;
}

uint32_t AMACv2RegMap::getField(AMACv2Field AMACv2RegMap::*ref) {
    return (this->*ref).read();
}

uint32_t AMACv2RegMap::getField(const std::string &fieldName) {
    if (m_fieldMap.find(fieldName) != m_fieldMap.end())
        return (this->*m_fieldMap[fieldName]).read();
    else
        std::cerr << " --> Error: Could not find register \"" << fieldName
                  << "\"" << std::endl;
    return 0;
}

uint32_t AMACv2RegMap::getReg(uint32_t reg) {
    return (this->*m_regAddrMap[reg]).getValue();
}

uint32_t AMACv2RegMap::getReg(AMACv2Register AMACv2RegMap::*ref) {
    return (this->*ref).getValue();
}

void AMACv2RegMap::setField(AMACv2Field AMACv2RegMap::*ref, uint32_t value) {
    (this->*ref).write(value);
}

void AMACv2RegMap::setField(const std::string &fieldName, uint32_t value) {
    if (m_fieldMap.find(fieldName) != m_fieldMap.end())
        return (this->*m_fieldMap[fieldName]).write(value);
    else
        std::cerr << " --> Error: Could not find register \"" << fieldName
                  << "\"" << std::endl;
    return;
}

void AMACv2RegMap::setReg(uint32_t reg, uint32_t value) {
    (this->*(m_regAddrMap[reg])).setValue(value);
}

void AMACv2RegMap::setReg(AMACv2Register AMACv2RegMap::*ref, uint32_t value) {
    (this->*ref).setValue(value);
}

uint8_t AMACv2RegMap::getAddr(AMACv2Field AMACv2RegMap::*ref) {
    return (this->*ref).getRegister()->getAddress();
}

uint8_t AMACv2RegMap::getAddr(const std::string &fieldName) {
    if (m_fieldMap.find(fieldName) != m_fieldMap.end())
        return (this->*m_fieldMap[fieldName]).getRegister()->getAddress();
    else
        std::cerr << " --> Error: Could not find register \"" << fieldName
                  << "\"" << std::endl;
    return 0;
}

bool AMACv2RegMap::canBeWritten(AMACv2Field AMACv2RegMap::*ref) {
    return (this->*ref).canBeWrittenField();
}
