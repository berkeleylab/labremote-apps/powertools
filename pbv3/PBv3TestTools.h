#ifndef PBV3TESTTOOLS_H
#define PBV3TESTTOOLS_H

#include <chrono>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <string>
#include <thread>

#include "AMACv2.h"
#include "Bk85xx.h"
#include "Logger.h"
#include "PBv3TB.h"

using nlohmann::json;

/**
 * \brief Module defining several tests for a Powerboard
 *
 * Some notes
 *  - All tests restore the AMACv2 configuration to what
 *    it was before the test was run.
 *  - All tests return a JSON format that can be directly
 *    uploaded to the ITk Production Database
 *  - All helper functions expects all hardware to be on and initialised
 *
 * # Console Printing
 *
 * Any tests that should be interpreted by a web GUI should try to follow
 * a standard format for outputs. A few formats are available to address
 * common use cases.
 *
 * The following placeholders are used in the examples below:
 * - `TESTTYPE`: Name of the, same as testType in the returned JSON.
 * - `DATETIME`: Date+time in the following format: %Y_%m_%d-%H:%M:%SZ%z (see
 * `PBv3Utils::getTimeAsString`)
 *
 *
 * The following are common rules for all formats:
 *
 * All tests must begin with:
 * `[INFO]    : ## Test TESTTYPE ## DATETIME`
 *
 * All tests must end with (pass vs fail depending on status):
 * ```
 * [INFO]    :  TESTTYPE passed! :)
 * [ERROR]   :  TESTTYPE failed! :(
 * [INFO]    : ## End test TESTTYPE ## DATETIME
 * ```
 *
 * Any output to be skipped should be prefixed with
 * a single `->`. For example:
 * `[INFO]    : -> I am doing stuff that does not need to be recorded`
 *
 * Any content after a ` # ` is considered a comment.
 *
 * ## Status Read Tests
 *
 * This format targets tests that check whether a single value is within an
 * whether it is in allowed range. For example, the BER.
 *
 * Placeholders:
 * - `VALUE`: Value being measured, same as column in JSON file (can be multiple
 * per test)
 * - `XXX`: Measured value of `VALUE`
 * - `UNIT`: Units of `XXX`
 *
 * Any value that falls outside of bounds should be
 * printed using `logger(logERROR)`.
 *
 * ```
 * [INFO]    : ## Test TESTTYPE ## DATETIME
 * [INFO]    :  --> VALUE1: XXX1 UNIT1
 * [ERROR]   :  --> VALUE2: XXX2 UNIT2
 * [INFO]    : ## End test TESTTYPE ## DATETIME
 * ```
 *
 * ## Voltage Enable Tests
 *
 * This format targets tests that enable some voltage (ie: DC/DC) and
 * read out several values.
 *
 * Placeholders:
 * - `ITEM`: Name of item being enabled, same as column in JSON file
 * - `CONST`: Columns constant for both OFF and ON states
 * - `CONSTVAL`: Column value for `CONST
 * - `VALUE`: Value being measured, same as column in JSON file (can be multiple
 * per test)
 * - `XXX`: Measured value of `VALUE`
 * - `UNIT`: Units of `XXX`
 *
 * Any value that falls outside of bounds should be
 * printed using `logger(logERROR)`.
 *
 * ```
 * [INFO]    : ## Test TESTTYPE ## DATETIME
 * [INFO]    :  ---> CONST1: CONSTVAL1
 * [INFO]    :  --> Enabling ITEM
 * [INFO]    :  ---> VALUE1: XXX1 UNIT1
 * [ERROR]   :  ---> VALUE2: XXX2 UNIT2
 * [INFO]    :  --> Disabling ITEM
 * [INFO]    :  ---> VALUE1: XXX1 UNIT1
 * [INFO]    :  ---> VALUE2: XXX2 UNIT2
 * [INFO]    : ## End test TESTTYPE ## DATETIME
 * ```
 *
 */
namespace PBv3TestTools {

//
// LV tests

/**
 * \brief LV_ENABLE Test the LV enable functionality
 *
 * Pass: Output voltage is between 1.4V and 1.6V when on and less than 0.1V when
 * off.
 *
 * Modifies the following registers
 *  - DCDCen
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 *
 * \return json object with test results
 */
json testLvEnable(uint32_t pbNum, std::shared_ptr<PBv3TB> tb);

/** \brief Permutes through DCDC Adjust states and confirms the level of
 * adjustment of the bpol DCDC output Looks at 0%, -13.3%, -6.67%, +6.67% shifts
 * and confirms amount
 *
 * Pass: When all 3 DCDC Adjust states are within (hardcoded: +/- 1%)  bounds
 *
 * Modifies the following registers
 *  - DCDCen
 *  - DCDCenC
 *  - DCDCAdj
 *  - DCDCAdjC
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 *
 * \return json object with test results
 */
json testDCDCAdj(uint32_t pbNum, std::shared_ptr<PBv3TB> tb);

/**
 * \brief DCDCEFFICIENCY Measures powerboard efficiency at different output
 * currents by comparing output power to input power.
 *
 * Scans output current from min to max with given step size.
 *
 * At each set output current value, the following are recorded:
 *   - Input voltage (VIN)
 *   - Input current (IIN)
 *   - Output voltage (VOUT)
 *   - Measured output current (IOUT)
 *   - bPol output voltage, in counts (AMACVDCDC)
 *   - linPol output voltage, in counts (AMACVDDLR)
 *   - bPol input voltage, in counts (AMACDCDCIN)
 *   - Powerboard NTC reading, in counts (AMACNTCPB)
 *   - Output current, in counts (AMACCUR10V)
 *   - Input current, in counts (AMACCUR1V)
 *   - bPol PTAT temperature reading, in counts (AMACPTAT)
 *   - DCDC efficiency, calculated using the set ouput current (EFFICIENCY)
 *   - DCDC efficiency, calculated using the measured output current
 * (EFFICIENCYSENSE)
 *
 * Efficiency is Power out/power in -> (Vout*Iout)/(Vin*(Iin-IinOffset))
 *
 * Pass: Always
 *
 * Modifies the following registers
 *  - DCDCen
 *  - DCDCenC
 *  - Ch12Mux
 *  - Ch14Mux
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 * \param step Steps in load current
 * \param min Minimum load current
 * \param min Maximum load current
 * \param VinSet Input voltage at power supply
 * \param trails Number of measurements per set current
 *
 * \return json object with test results
 */
json measureEfficiency(uint32_t pbNum, std::shared_ptr<PBv3TB>, double step,
                       double min, double max, double VinSet = 11.0,
                       uint32_t trails = 1);

/** \brief Scans through different VIN values and measures AMAC response
 *
 * Scan of Vin from 6V to 11V (all valid for linPOL operation) is performed
 * and the AMAC reading of this value is taken.
 *
 * Pass: Always
 *
 * \param amac AMAC communication object
 * \param lv Low Voltage power supply
 *
 * \return json object with test result
 */
json calibVinResponse(std::shared_ptr<AMACv2> amac,
                      std::shared_ptr<PowerSupplyChannel> lv);

/** \brief Scans through different VIN values and measures current
 *
 * Scan of Vin from 2V to 11V is performed and current usage monitored
 * from the power supply. linPOL will not work below 6V, thus an AMAC
 * reinitialization has to be preformed after this test.
 *
 * Pass: Always
 *
 * \param lv Low Voltage power supply
 *
 * \return json object with test result
 */
json measureLvIV(std::shared_ptr<PowerSupplyChannel> lv);

/**
 * \brief toggleOutput calls toggleOutputHelper to toggle the outputs
 * of various AMAC registers to determine what is working.
 *
 * Toggles the following:
 *   - Vout
 *   - OFout
 *   - Shuntx
 *   - Shunty
 *   - CALx
 *   - CALy
 *   - LDx0EN
 *   - LDx1EN
 *   - LDx2EN
 *   - LDy0EN
 *   - LDy1EN
 *   - LDy2EN
 *
 * Pass: if all calls to toggleOutputHelper pass.
 * Acceptable ranges for the various registers are as follows:
 *   - Vout:
 *     - Off: (-0.1, 0.1)
 *     - On: (1.4, 1.6)
 *   - OFout:
 *     - Off: (-0.1, 0.1)
 *     - On: (1.0, 1.5)
 *   - Shuntx:
 *     - Off: (-0.1, 0.3)
 *     - On: (0.9, 1.3)
 *   - Shunty:
 *     - Off: (-0.1, 0.3)
 *     - On: (0.9, 1.3)
 *   - CALx:
 *     - Off: (-0.1, 0.1)
 *     - On: (0.9, 1.3)
 *   - CALy:
 *     - Off: (-0.1, 0.1)
 *     - On: (0.9, 1.3)
 *   - LDx0EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *   - LDx1EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *   - LDx2EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *   - LDy0EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *   - LDy1EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *   - LDy2EN:
 *     - Off: (-0.01, 0.01)
 *     - On: (1.0, 1.5)
 *
 * \param pbNum powerboard number on testbench
 * \param tb testbench
 * \param hbipc Hybrid burn-in test flag, if true then OFout tests will be
 * bypassed
 *
 * \return json object with test result
 */
json toggleOutput(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                  bool hbipc = false);

/**
 * \brief toggleOutputHelper is a helper function for toggleOutput that toggles
 * a single powerboard output
 *
 * Sets testSum["results"]["passed"]=false if test fails, otherwise passed is
 * not set.
 *
 * \param tb Testbench object
 * \param pbNum Powerboard number on the testboard
 * \param muxCh multiplexer channel
 * \param on AMAC register value to toggle on the output
 * \param off AMAC register value to toggle off the output
 * \param offLowThresh lower bound on acceptable range of off output value
 * \param offHighThresh upper bound on acceptable range of off output value
 * \param onLowThresh lower bound on acceptable range of on output value
 * \param onHighThresh upper bound on acceptable range of on output value
 * \param outString name output channel being toggled
 * \param amacCh AMAC register to toggle
 * \param amacChCopy second AMAC register to toggle (set to `nullptr` for non
 * copy protected registers)
 * \param maxPbNum the powerboard number on the testboard at which to read the
 * MUX
 *
 * Pass: If on value is between onLowThresh and onHighThresh and off value is
 * between offLowThresh and offHighThresh.
 *
 * \return json object with test results
 */
json toggleOutputHelper(
    uint32_t pbNum, std::shared_ptr<PBv3TB> tb, PBv3TB::CARRIER_OUTPUT muxCh,
    uint32_t on, uint32_t off, double offLowThresh, double offHighThresh,
    double onLowThresh, double onHighThresh, const std::string &outString,
    AMACv2Field AMACv2RegMap::*amacCh,
    AMACv2Field AMACv2RegMap::*amacChCopy = nullptr,
    uint32_t muxPbNum = std::numeric_limits<uint32_t>::max());

/** \name HV tests
 * @{ */

/**
 * \brief HV_ENABLE test the HV enable functionality
 *
 * Output 500V from power supply with 1mA current limit. The
 * current is then measured with HVmux in off and on states.
 *
 * Pass:
 *  - HVVIN ON and OFF states > 450 V
 *  - HVIIN ON state - OFF state > 0.4mA (HVmux toggled)
 *  - HVIOUT ON state > 0.8e-3 A
 *  - HVIOUT OFF state < 1e-7 A
 *  - AMACHVRET OFF state < 200
 *  - AMACHVRET ON state > 300
 *
 * Modifies the following registers
 *  - HVcurGain
 *  - CntSetHV0frq
 *  - CntSetCHV0frq
 *  - CntSetHV0en
 *  - CntSetCHV0en
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 * \param hvMux The HVMux to test
 * \param frequency Value of HV enable frequency register (see AMAC
 * specification for possible values)
 *
 * \return JSON object with test results
 */
json testHvEnable(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, int hvMux = 0,
                  unsigned frequency = 0x3);

/**
 * \brief HVSENSE test AMAC response to changing HV current
 *
 * Step through several voltage values and read the AMAC
 * measurement at different gain settings. This translates to
 * a current scan if a fixed load is used.
 *
 * Pass: Always
 *
 * Modifies the following registers
 *  - CntSetHV0en
 *  - CntSetCHV0en
 *  - HVcurGain
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 * \param hvMux The HVMux to test
 * \param trails Number of trails at each current values
 *
 * \return JSON object with test results
 */
json measureHvSense(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, int hvMux = 0,
                    uint32_t trails = 1);

/**
 * \brief HVCURRENT test AMAC measurement of HVret current
 *
 * Measures the sensor current using the AMAC for different gain
 * settings.
 *
 * Pass: Always
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench object
 * \param trails Number of trails at each current values
 *
 * \return JSON object with test results
 */
json measureHvCurrent(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                      uint32_t trails = 1);

/** @} */

//
// AMAC tests

/**
 * \brief BER Tests AMAC read/write reliability
 *
 * Writes random values to AMAC register 166 and reads them back to confirm that
 * the value has not changed.
 *
 * Pass: If all values are returned unchanged.
 *
 * \param amac AMAC for communication
 * \param trails Number if times to write to the register. Default: 1000.
 *
 * \return JSON object with test results
 */
json runBER(std::shared_ptr<AMACv2> amac, uint32_t trails = 1000);

/**
 * \brief STATUS .
 *
 * Reads the current value of the AMAC for a number of registers and reads low
 * voltage and high voltage currents and voltages from the connected power
 * supplies and active board load.
 *
 * Reads the status of the following AMAC registers:
 *   - AMAC output voltage, in counts (AMACVDCDC)
 *   - AMAC VddLr, in counts (AMACVDDLR)
 *   - AMAC input voltage, in counts (AMACDCDCIN)
 *   - AMAC VddReg, in counts (AMACVDDREG)
 *   - AMAC AM bandgap, 900mV, in counts (AMACAM900BG)
 *   - AMAC AM bandgap, 600mV, in counts (AMACAM600BG)
 *   - AMAC CAL, in counts, (AMACCAL)
 *   - AMAC CTAT temperature reading, in counts, (AMACCTAT)
 *   - X Hybrid NTC temperature reading, in counts (AMACNTCX)
 *   - Y Hybrid NTC temperature reading, in counts (AMACNTCY)
 *   - Powerboard NTC temperature reading, in counts (AMACNTCPB)
 *   - AMAC output current, in counts (AMACCUR10V)
 *   - AMAC input current, in counts (AMACCUR1V)
 *   - AMAC high voltage return, in counts (AMACHVRET)
 *   - FEAST PTAT temperature reading, in counts (AMACPTAT)
 *   - AMAC ADC0, in counts (ADC0)
 *   - AMAC ADC1, in counts (ADC1)
 *   - AMAC ADC2, in counts (ADC2)
 *   - AMAC ADC3, in counts (ADC3)
 *
 * Reads the following values from either the power supplies or load:
 *   - Low voltage input voltage (VIN)
 *   - Low voltage input current (IIN)
 *   - Low voltage output voltage (VOUT)
 *   - Low voltage output current (IOUT)
 *   - High voltage input voltage (HVVIN)
 *   - High voltage input current (HVIIN)
 *
 * Pass: Always
 *
 * \param amac AMAC for communication
 * \param lv low voltage power supply
 * \param load external load
 * \param hv high voltage power supply
 * \param tests number of times to perform status read. Default: 1
 *
 * \return JSON object with test results
 */
json readStatus(std::shared_ptr<AMACv2> amac,
                std::shared_ptr<PowerSupplyChannel> lv,
                std::shared_ptr<Bk85xx> load,
                std::shared_ptr<PowerSupplyChannel> hv, uint32_t tests = 1);

/**
 * \brief AMSLOPE step through CAL values to get the slope of the AM
 *
 * Modifies the following registers
 *  - AMintCalib
 *  - AMbg
 *  - Ch4Mux
 *
 * Pass: Always
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench with all control classes
 * \param step Voltage steps for the CAL
 * \param scanSettings Scan AMintCalib and AMbg if true, use current value if
 * false
 *
 * \return JSON object with test results
 */
json calibrateAMACslope(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, double step,
                        bool scanSettings = true);

/**
 * \brief AMOFFSET step through channels and gain settings to get the offset of
 * the AM per channel.
 *
 * Modifies the following registers
 *  - AMintCalib
 *  - AMzeroCalib
 *  - AMzeroCalibC
 *
 * Pass: Always
 *
 * \param amac AMAC for communication
 * \param scanSettings Scan AMintCalib if true, use current value if false
 *
 * \return JSON object with test results
 */
json calibrateAMACoffset(std::shared_ptr<AMACv2> amac,
                         bool scanSettings = true);

/**
 * \brief CUR10V calibrates the AMAC 10V current monitoring.
 *
 * Modifies the following registers
 *  - DCDCiZeroReading
 *  - DCDCiOffset
 *  - DCDCiP
 *  - DCDCiN
 *
 * Pass: Always
 *
 * \param amac AMAC for communication
 * \param tests number of times read values. Default: 10.
 *
 * \return JSON object with test results
 */
json calibrateAMACCur10V(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                         uint32_t tests = 10);

/**
 * \brief CUR1V calibrates the AMAC 1V current monitoring.
 *
 * Modifies the following registers
 *  - DCDCen
 *  - DCDCenC
 *  - DCDCoZeroReading
 *  - DCDCoOffset
 *  - DCDCoP
 *  - DCDCoN
 *
 * Pass: Always
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench with all control classes
 *
 * \return JSON object with test results
 */
json calibrateAMACCur1V(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
                        uint32_t tests = 10);

//! \brief Checks to see whether or not the linPOL on the powerboard is
//! operational.
/**
 * Toggles OF and checks that the linPOL output voltage is above 0.1V when OFF
 * and 1.3V when ON.
 *
 * The test also reads the input voltage and current at both stages. The ON
 * reading is done second with no AMAC configuration in between. Thus the
 * current is the power-up current draw of the chip.
 *
 * Pass:
 *   - when OF is disabled (linPOL is on), the linPOL voltage is greater
 * than 1.3V
 *   - when OF is enabled (linPOL is off), the linPOL voltage is less than 0.1V
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench with all control classes
 *
 * \return JSON object with test results
 */
json testOF(uint32_t pbNum, std::shared_ptr<PBv3TB> tb);

/**
 * \brief Determine the AMAC PADID and eFuse ID
 *
 * The implementation requires the AMAC ID to be set again even if it was
 * previously.
 *
 * Pass: If AMAC PADID is 0.
 *
 * \param pbNum Powerboard number on the testboard
 * \param tb Testbench with all control classes
 * \param hbipc Hybrid burn-in flag, if enabled it expands accepted AMAC PADID
 * to 0 to 15
 *
 * \return JSON object with test results
 */
json scanPADID(uint32_t pbNum, std::shared_ptr<PBv3TB> tb, bool hbipc = false);

/**
 * \brief Measure the temperature using all possible ways
 *
 * Pass: Temperature in range as defined by the following
 * table. All values are in [counts].
 *
 * | __Input__ | __Min__ | __Max__ |
 * |-----------|---------|---------|
 * | CTAT      |     300 |     400 |
 * | PTAT      |     350 |     450 |
 * | NTCpb     |     700 |    1000 |
 * | NTCx      |     600 |     700 |
 * | NTCy      |     600 |     700 |
 *
 * Special cases apply to the end-cap, where not a resistor,
 * but a 10 kOhm NTC is used to mimic the hybrid NTC. Therefore, the
 * accepted DAC is defined by the value given by the value of
 * the PB 10 kOhm NTC as follows:
 * NTCx, NTCy in [NTCpb - 125, NTCpb + 125].
 *
 * Modifies the following registers
 *  - CTAToffset
 *  - NTCx0SenseRange
 *  - NTCy0SenseRange
 *  - NTCpbSenseRange
 *
 * \param amac AMAC communication object
 * \param tb Testbench object
 *
 * \return JSON object with test results
 */
json temperature(std::shared_ptr<AMACv2> amac, std::shared_ptr<PBv3TB> tb);

/**
 * \brief Scan NTC gain settings
 *
 * Pass: Always
 *
 * Modifies the following registers
 *  - NTCx0SenseRange
 *  - NTCy0SenseRange
 *  - NTCpbSenseRange
 *
 * \param amac AMAC communication object
 *
 * \return JSON object with test results
 */
json temperatureNTC(std::shared_ptr<AMACv2> amac);

/**
 * \brief Scan CTAT offset settings
 *
 * Pass: Always
 *
 * Modifies the following registers
 *  - CTAToffset
 *
 * \param amac AMAC communication object
 *
 * \return JSON object with test results
 */
json temperatureCTAT(std::shared_ptr<AMACv2> amac);

//! Ramp an internal DAC and measure the output voltage
/**
 * Name of the test will be `RAMPdacName`.
 *
 * \param tb Testbench object
 * \param pbNum Powerboard number on the testboard
 * \param dacName field name controlling DAC output
 * \param adcCh ADC channel on test board measuring the output
 * \param am AM channel for internal measurement of DAC
 *
 * Pass: Always.
 *
 * \return json object with test results
 */
json rampDAC(uint32_t pbNum, std::shared_ptr<PBv3TB> tb,
             const std::string &dacName, PBv3TB::CARRIER_OUTPUT adcCh,
             AMACv2::AM am);

//! In the results replace all keys according to a specific "HV" pattern:
/**
 * Replace "HV0" by "HV`n`"
 * then replace "HV" by "HV`n`"
 * then replace "HV`nn`" by "HV`n`"
 * then replace "AMACGAIN`" by "AMAC_`n`_GAIN"
 *
 * \param inJson results json section test["results"]
 * \param n the resplacement to use for "HV`n`" etc
 */
void accountJSONForHVMuxN(nlohmann::json &inJson, int n);
}  // namespace PBv3TestTools

#endif
