#include <Logger.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBMassive.h"
#include "PBv3TestTools.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

//------ Channel Map
std::map<std::string, AMACv2Field AMACv2RegMap::*> channelmap = {
    {"VDCDC", &AMACv2::Ch0Value},         {"VDDLR", &AMACv2::Ch1Value},
    {"DCDCin", &AMACv2::Ch2Value},        {"VDDREG", &AMACv2::Ch3Value},
    {"sysBG", &AMACv2::Ch3Value},         {"AM900BG", &AMACv2::Ch3Value},
    {"AM600BG", &AMACv2::Ch4Value},       {"CAL", &AMACv2::Ch4Value},
    {"AMref", &AMACv2::Ch4Value},         {"CALx", &AMACv2::Ch5Value},
    {"CALy", &AMACv2::Ch5Value},          {"Shuntx", &AMACv2::Ch5Value},
    {"Shunty", &AMACv2::Ch5Value},        {"HGND", &AMACv2::Ch5Value},
    {"DIETEMP", &AMACv2::Ch6Value},       {"NTCx", &AMACv2::Ch7Value},
    {"NTCy", &AMACv2::Ch8Value},          {"NTCpb", &AMACv2::Ch9Value},
    {"Hrefx", &AMACv2::Ch10Value},        {"Hrefy", &AMACv2::Ch11Value},
    {"Cur10V", &AMACv2::Ch12Value},       {"Cur10VTPlow", &AMACv2::Ch12Value},
    {"Cur10VTPhigh", &AMACv2::Ch12Value}, {"Cur1V", &AMACv2::Ch13Value},
    {"Cur1VTPlow", &AMACv2::Ch13Value},   {"Cur1VTPhigh", &AMACv2::Ch13Value},
    {"HVret", &AMACv2::Ch14Value},        {"PTAT", &AMACv2::Ch15Value},
};

std::map<std::string, std::pair<AMACv2Field AMACv2RegMap::*, uint32_t>> muxmap =
    {{"VDDREG", {&AMACv2::Ch3Mux, 0}},
     {"sysBG", {&AMACv2::Ch3Mux, 1}},
     {"AM900BG", {&AMACv2::Ch3Mux, 2}},
     {"AM600BG", {&AMACv2::Ch4Mux, 0}},
     {"CAL", {&AMACv2::Ch4Mux, 1}},
     {"AMref", {&AMACv2::Ch4Mux, 2}},
     {"CALx", {&AMACv2::Ch5Mux, 0}},
     {"CALy", {&AMACv2::Ch5Mux, 1}},
     {"Shuntx", {&AMACv2::Ch5Mux, 2}},
     {"Shunty", {&AMACv2::Ch5Mux, 3}},
     {"HGND", {&AMACv2::Ch5Mux, 4}},
     {"Cur10V", {&AMACv2::Ch12Mux, 0}},
     {"Cur10VTPlow", {&AMACv2::Ch12Mux, 1}},
     {"Cur10VTPhigh", {&AMACv2::Ch12Mux, 2}},
     {"Cur1V", {&AMACv2::Ch13Mux, 0}},
     {"Cur1VTPlow", {&AMACv2::Ch13Mux, 1}},
     {"Cur1VTPhigh", {&AMACv2::Ch13Mux, 2}}};
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

void readAMAC(std::shared_ptr<AMACv2> amac);
void readMux(std::shared_ptr<PBv3TB> tb, int pb);

int main(int argc, char *argv[]) {
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Get amac
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);

    if (std::dynamic_pointer_cast<PBv3TBMassive>(
            tb)) {  // Massive tester can check if AMAC is powered before trying
                    // to read it
        double LP1V4;
        LP1V4 = tb->readCarrierOutput(pbNum, PBv3TB::LINPOL1V4);

        if (LP1V4 > 1.3 && LP1V4 < 1.5) {
            readMux(tb, pbNum);
            readAMAC(amac);
        } else {
            logger(logERROR)
                << "PB " << pbNum << ": linPOL 1V4 reading " << LP1V4 << ".";
            readMux(tb, pbNum);
        }
    } else
        readAMAC(amac);

    return 0;
}

void readMux(std::shared_ptr<PBv3TB> tb, int pbNum) {
    std::cout << "\nReading Multiplexer values:" << std::endl << std::endl;

    // Vout
    std::cout << "Vout: "
              << "\t\t" << tb->readCarrierOutput(pbNum, PBv3TB::VOUT)
              << std::endl;

    // OFout
    std::cout << "OFout: "
              << "\t\t" << tb->readCarrierOutput(pbNum, PBv3TB::OFout)
              << std::endl;

    // Shuntx
    std::cout << "Shuntx: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::SHUNTx)
              << std::endl;

    // Shunty
    std::cout << "Shunty: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::SHUNTy)
              << std::endl;

    // CALx
    std::cout << "CALx: "
              << "\t\t" << tb->readCarrierOutput(pbNum, PBv3TB::CALx)
              << std::endl;

    // CALy
    std::cout << "CALy: "
              << "\t\t" << tb->readCarrierOutput(pbNum, PBv3TB::CALy)
              << std::endl;

    // LDx0EN
    std::cout << "LDx0EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDx0EN)
              << std::endl;

    // LDx1EN
    std::cout << "LDx1EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDx1EN)
              << std::endl;

    // LDx2EN
    std::cout << "LDx2EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDx2EN)
              << std::endl;

    // LDy0EN
    std::cout << "LDy0EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDy0EN)
              << std::endl;

    // LDy1EN
    std::cout << "LDy1EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDy1EN)
              << std::endl;

    // LDy2EN
    std::cout << "LDy2EN: "
              << "\t" << tb->readCarrierOutput(pbNum, PBv3TB::LDy2EN)
              << std::endl;
}

void readAMAC(std::shared_ptr<AMACv2> amac) {
    std::cout << "\nReading AMAC values:" << std::endl << std::endl;

    std::cout << "VDCDC: "
              << "\t\t" << amac->rdField(&AMACv2::Ch0Value) << std::endl;

    std::cout << "VDDLR: "
              << "\t\t" << amac->rdField(&AMACv2::Ch1Value) << std::endl;

    std::cout << "DCDCin: "
              << "\t" << amac->rdField(&AMACv2::Ch2Value) << std::endl;

    amac->wrField(muxmap["VDDREG"].first, muxmap["VDDREG"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "VDDREG: "
              << "\t" << amac->rdField(&AMACv2::Ch3Value) << std::endl;

    amac->wrField(muxmap["sysBG"].first, muxmap["sysBG"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "sysBG: "
              << "\t\t" << amac->rdField(&AMACv2::Ch3Value) << std::endl;

    amac->wrField(muxmap["AM900BG"].first, muxmap["AM900BG"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "AM900BG: "
              << "\t" << amac->rdField(&AMACv2::Ch3Value) << std::endl;

    std::cout << "AM600BG: "
              << "\t" << amac->rdField(&AMACv2::Ch4Value) << std::endl;

    amac->wrField(muxmap["CAL"].first, muxmap["CAL"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "CAL: "
              << "\t\t" << amac->rdField(&AMACv2::Ch4Value) << std::endl;

    amac->wrField(muxmap["AMref"].first, muxmap["AMref"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "AMref: "
              << "\t\t" << amac->rdField(&AMACv2::Ch4Value) << std::endl;

    amac->wrField(muxmap["Shuntx"].first, muxmap["Shuntx"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Shuntx: "
              << "\t" << amac->rdField(&AMACv2::Ch5Value) << std::endl;

    amac->wrField(muxmap["Shunty"].first, muxmap["Shunty"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Shunty: "
              << "\t" << amac->rdField(&AMACv2::Ch5Value) << std::endl;

    amac->wrField(muxmap["CALx"].first, muxmap["CALx"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "CALx: "
              << "\t\t" << amac->rdField(&AMACv2::Ch5Value) << std::endl;

    amac->wrField(muxmap["CALy"].first, muxmap["CALy"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "CALy: "
              << "\t\t" << amac->rdField(&AMACv2::Ch5Value) << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    amac->wrField(muxmap["HGND"].first, muxmap["HGND"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "HGND: "
              << "\t\t" << amac->rdField(&AMACv2::Ch5Value) << std::endl;

    std::cout << "DIETEMP: "
              << "\t" << amac->rdField(&AMACv2::Ch6Value) << std::endl;

    std::cout << "NTCx: "
              << "\t\t" << amac->rdField(&AMACv2::Ch7Value) << std::endl;

    std::cout << "NTCy: "
              << "\t\t" << amac->rdField(&AMACv2::Ch8Value) << std::endl;

    std::cout << "NTCpb: "
              << "\t\t" << amac->rdField(&AMACv2::Ch9Value) << std::endl;

    std::cout << "Hrefx: "
              << "\t\t" << amac->rdField(&AMACv2::Ch10Value) << std::endl;

    std::cout << "Hrefy: "
              << "\t\t" << amac->rdField(&AMACv2::Ch11Value) << std::endl;

    amac->wrField(muxmap["Cur10V"].first, muxmap["Cur10V"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur10V: "
              << "\t" << amac->rdField(&AMACv2::Ch12Value) << std::endl;

    amac->wrField(muxmap["Cur10VTPlow"].first, muxmap["Cur10VTPlow"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur10VTPlow: "
              << "\t" << amac->rdField(&AMACv2::Ch12Value) << std::endl;

    amac->wrField(muxmap["Cur10VTPhigh"].first, muxmap["Cur10VTPhigh"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur10VTPhigh: "
              << "\t" << amac->rdField(&AMACv2::Ch12Value) << std::endl;

    amac->wrField(muxmap["Cur1V"].first, muxmap["Cur1V"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur1V: "
              << "\t\t" << amac->rdField(&AMACv2::Ch13Value) << std::endl;

    amac->wrField(muxmap["Cur1VTPlow"].first, muxmap["Cur1VTPlow"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur1VTPlow: "
              << "\t" << amac->rdField(&AMACv2::Ch13Value) << std::endl;

    amac->wrField(muxmap["Cur1VTPhigh"].first, muxmap["Cur1VTPhigh"].second);
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    std::cout << "Cur1VTPhigh: "
              << "\t" << amac->rdField(&AMACv2::Ch13Value) << std::endl;

    std::cout << "HVret: "
              << "\t\t" << amac->rdField(&AMACv2::Ch14Value) << std::endl;

    std::cout << "PTAT: "
              << "\t\t" << amac->rdField(&AMACv2::Ch15Value) << std::endl;
}
