#include <getopt.h>
#include <unistd.h>

#include <iomanip>
#include <iostream>
#include <thread>

#include "AMACv2.h"
#include "EndeavourCom.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "UIOCom.h"

//------ SETTINGS
loglevel_e loglevel = logINFO;
uint8_t pbNum;
std::string equipConfigFile = "config/equip_testbench.json";

void usage(char *argv[]) {
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board       Powerboard number to tune (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr << " -e, --equip       config.json Equipment configuration file "
                 "(default: "
              << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug       Enable more verbose printout" << std::endl;
    std::cerr << "" << std::endl;
    std::cerr << "" << std::endl;
}

int main(int argc, char *argv[]) {
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                loglevel = logDEBUG;
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;
    logger(logDEBUG) << " Hardware configuration: " << equipConfigFile;

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    tb->getPB(pbNum)->init();

    //
    // Get eFuse ID
    uint32_t eFuse = tb->getPB(pbNum)->rdField(&AMACv2RegMap::SerNum);
    std::cout << "0x" << std::hex << std::setw(8) << std::setfill('0') << eFuse
              << std::endl;

    return 0;
}
