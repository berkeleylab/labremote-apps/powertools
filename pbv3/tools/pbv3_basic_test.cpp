#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBTypeDef.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBSingle.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"

//------ SETTINGS
uint8_t pbNum = 0;
PBTypeDef::PBType type = PBTypeDef::PBType::BARREL;
std::string configfile = "config.json";
std::string runNumber = "0-0";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] datadir" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr << " -t, --type              Powerboard type (default: "
              << (int)type << ")" << std::endl
              << "                             Which is:" << std::endl
              << "                               Barrel =  "
              << (int)PBTypeDef::PBType::BARREL << std::endl
              << "                               R0     =  "
              << (int)PBTypeDef::PBType::EC_R0 << std::endl
              << "                               R1     =  "
              << (int)PBTypeDef::PBType::EC_R1 << std::endl
              << "                               R2     =  "
              << (int)PBTypeDef::PBType::EC_R2 << std::endl
              << "                               R3     =  "
              << (int)PBTypeDef::PBType::EC_R3 << std::endl
              << "                               R4     =  "
              << (int)PBTypeDef::PBType::EC_R4 << std::endl
              << "                               R5     =  "
              << (int)PBTypeDef::PBType::EC_R5 << std::endl;
    std::cerr << " -c, --config            Config for initializing the AMAC. "
                 "(default: "
              << configfile << ")" << std::endl;
    std::cerr
        << " -r, --runnumber         Run number to associate with the test "
           "results. (default: "
        << runNumber << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"type", required_argument, 0, 't'},
            {"config", required_argument, 0, 'c'},
            {"runnumber", required_argument, 0, 'r'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:t:c:r:e:dh", long_options,
                        &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 't':
                type = static_cast<PBTypeDef::PBType>(atoi(optarg));
                break;
            case 'c':
                configfile = optarg;
                break;
            case 'r':
                runNumber = optarg;
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Required paths missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    std::string outDir = argv[optind++];

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;
    logger(logDEBUG) << " Type: " << (int)type
                     << " (=" << PBTypeDef::mapTypeToString.at(type) << ")";
    logger(logDEBUG) << " outDir: " << outDir;

    //
    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Set PB panel type
    tb->setPBPanelType(type);

    // Power up the testbench
    tb->powerTBOn();

    // Turn on power
    logger(logINFO) << "Turn on PS fully";
    tb->powerLVOn();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // Init com
    json config;
    if (!configfile.empty()) {
        std::ifstream fh_in(configfile);
        if (fh_in.is_open()) fh_in >> config;
    }

    logger(logINFO) << "Init AMAC";
    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    try {
        int padid = tb->getPBTypePADID(pbNum);
        logger(logDEBUG) << "Using padID " << padid << " for type "
                         << (int)tb->getPBPanelType();
        amac->setPADID(padid);
        amac->init();
        try {
            auto id = amac->readEFuse();
            logger(logINFO) << "Fuse ID is " << id;
        } catch (const EndeavourComException &e) {
            logger(logINFO) << "Could not read fuseID";
        }
        PBv3ConfigTools::configAMAC(amac, config, false);
        amac->initRegisters();
        PBv3ConfigTools::saveConfigAMAC(amac, config);
    } catch (EndeavourComException &e) {
        logger(logERROR) << "Unable to initialize AMACv2";
        logger(logERROR) << e.what();
        return 1;
    }

    //
    // Start testing

    std::shared_ptr<PowerSupplyChannel> lv = tb->getLVPS();
    std::shared_ptr<PowerSupplyChannel> hv = tb->getHVPS();

    json testSum;
    testSum["program"] = argv[0];
    testSum["runNumber"] = runNumber;
    testSum["config"] = config;
    testSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    uint32_t test = 0;
    testSum["tests"][test++] = PBv3TestTools::runBER(amac);
    if (std::dynamic_pointer_cast<PBv3TBSingle>(tb))
        testSum["tests"][test++] = PBv3TestTools::readStatus(
            amac, lv, std::dynamic_pointer_cast<PBv3TBSingle>(tb)->getLoadPtr(),
            hv);
    else
        testSum["tests"][test++] =
            PBv3TestTools::readStatus(amac, lv, nullptr, hv);
    testSum["tests"][test++] = PBv3TestTools::testLvEnable(pbNum, tb);
    testSum["tests"][test++] = PBv3TestTools::testDCDCAdj(pbNum, tb);
    testSum["tests"][test++] = PBv3TestTools::testHvEnable(pbNum, tb);
    testSum["tests"][test++] = PBv3TestTools::calibVinResponse(amac, lv);
    testSum["tests"][test++] = PBv3TestTools::measureHvSense(pbNum, tb);
    testSum["tests"][test++] =
        PBv3TestTools::measureEfficiency(pbNum, tb, 0.1, 0, 3.6);
    testSum["tests"][test++] = PBv3TestTools::calibrateAMACoffset(amac, false);
    testSum["tests"][test++] =
        PBv3TestTools::calibrateAMACslope(pbNum, tb, 0.01, false);
    testSum["tests"][test++] =
        PBv3TestTools::calibrateAMACCur10V(pbNum, tb, 10);
    testSum["tests"][test++] = PBv3TestTools::calibrateAMACCur1V(pbNum, tb, 10);
    testSum["tests"][test++] = PBv3TestTools::measureLvIV(lv);

    testSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Power off
    logger(logINFO) << "Power off";

    tb->powerHVOff();
    tb->powerLVOff();
    tb->powerTBOff();

    // Output file
    std::string fileName =
        outDir + "/" +
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) +
        "_pbv3-test.json";
    logger(logINFO) << "Results stored in " << fileName;
    std::fstream outfile(fileName, std::ios::out);
    if (!outfile.is_open()) {
        logger(logERROR) << "Unable to create results file " << fileName;
        return 2;
    }

    outfile << std::setw(4) << testSum << std::endl;

    outfile.close();

    return 0;
}
