#include <Logger.h>
#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "EndeavourComException.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string configfile = "config.json";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] config.json" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 1) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:e:dh", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (optind == argc) {
        std::cerr << "Required config file argument missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }
    configfile = argv[optind++];

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    //
    // Open configuration
    json config;
    std::ifstream fh_in(configfile);
    if (fh_in.is_open())
        fh_in >> config;
    else {
        logger(logERROR) << "Unable to open config file: " << configfile;
        return 1;
    }
    PBv3ConfigTools::decorateConfig(config);

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    amac->init();

    // Write the configuration
    try {
        PBv3ConfigTools::configAMAC(amac, config, false);
        amac->initRegisters();
    } catch (EndeavourComException &e) {
        logger(logERROR) << "Unable to initialize AMACv2";
        logger(logERROR) << e.what();
        return 1;
    }

    return 0;
}
