#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBSingle.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string configfile = "config.json";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

bool quit = false;
void cleanup(int signum) { quit = true; }

#define PERIOD_LONG 60  // number of seconds of monitoring between calibrations
#define PERIOD_MONITOR 1  // number of seconds to wait between readings
void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] datadir" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -c, --config            Config with AMAC calibrations. (default: "
        << configfile << ")" << std::endl;
    std::cerr << " -b, --board             Powerboard number (default: "
              << (uint32_t)pbNum << ")" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"config", required_argument, 0, 'c'},
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "c:b:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'c':
                configfile = optarg;
                break;
            case 'b':
                pbNum = atoi(optarg);
                break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Required output path missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    std::string outDir = argv[optind++];

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " outDir: " << outDir;
    logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;

    // Register interrupt for cleanup
    signal(SIGINT, cleanup);
    signal(SIGTERM, cleanup);

    // Get default config
    json defconfig;
    if (!configfile.empty()) {
        std::ifstream fh_in(configfile);
        if (fh_in.is_open()) fh_in >> defconfig;
    }

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    std::shared_ptr<AMACv2> amac = tb->getPB(pbNum);
    amac->init();

    //
    // Start running tests in a loop forever!
    //
    while (!quit) {
        //
        // Rerun the calibration

        // Run all calibrations
        json config = defconfig;
        config.merge_patch(PBv3ConfigTools::tuneNTC(amac));
        config.merge_patch(
            PBv3ConfigTools::calibrateSlope(amac, tb->getCalDAC()));
        config.merge_patch(PBv3ConfigTools::calibrateOffset(amac));
        config.merge_patch(PBv3ConfigTools::calibrateNTC(amac));
        PBv3ConfigTools::saveConfigAMAC(amac, config);

        // Prepare the output structure
        json testSum;
        testSum["program"] = argv[0];
        testSum["config"] = config;
        testSum["time"]["start"] =
            PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

        uint32_t test = 0;

        // Start monitoring in a loop
        for (uint32_t monIdx = 0; monIdx < PERIOD_LONG / PERIOD_MONITOR;
             monIdx++) {
            if (std::dynamic_pointer_cast<PBv3TBSingle>(tb))
                testSum["tests"][test++] = PBv3TestTools::readStatus(
                    amac, tb->getLVPS(),
                    std::dynamic_pointer_cast<PBv3TBSingle>(tb)->getLoadPtr(),
                    tb->getHVPS());
            else
                testSum["tests"][test++] = PBv3TestTools::readStatus(
                    amac, tb->getLVPS(), nullptr, tb->getHVPS());
            std::this_thread::sleep_for(std::chrono::seconds(PERIOD_MONITOR));
        }

        // cleanup the file
        testSum["time"]["end"] =
            PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

        // Output file
        std::string fileName =
            outDir + "/" +
            PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) +
            "_pbv3-monitor.json";
        std::fstream outfile(fileName, std::ios::out);
        outfile << std::setw(4) << testSum << std::endl;
        outfile.close();
        logger(logINFO) << "Results stored in " << fileName;
    }

    return 0;
}
