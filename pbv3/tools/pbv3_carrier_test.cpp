#include <getopt.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "Logger.h"
#include "PBv3CarrierTools.h"
#include "PBv3TBMassive.h"
#include "PBv3Utils.h"

using namespace nlohmann;

//------ SETTINGS
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] datadir" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "b:c:r:e:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Required paths missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    std::string outDir = argv[optind++];

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " outDir: " << outDir;

    // Output file
    std::string fileName =
        outDir + "/" +
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) +
        "_pbv3-carrier.json";
    logger(logINFO) << "Results stored in " << fileName;
    std::fstream outfile(fileName, std::ios::out);
    if (!outfile.is_open()) {
        logger(logERROR) << "Unable to create results file " << fileName;
        return 2;
    }

    //
    // Create and initialize the testbench
    std::shared_ptr<PBv3TBMassive> tb = std::make_shared<PBv3TBMassive>();
    tb->init();

    std::shared_ptr<EquipConf> equipConfig =
        std::make_shared<EquipConf>(equipConfigFile);
    tb->setLVPS(equipConfig->getPowerSupplyChannel("Vin"));

    //
    // Start testing

    json testSum;
    testSum["program"] = argv[0];
    testSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    uint32_t test = 0;
    testSum["tests"][test++] = PBv3CarrierTools::testLvEnable(tb);
    testSum["tests"][test++] = PBv3CarrierTools::testMuxes(tb);

    testSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    outfile << std::setw(4) << testSum << std::endl;

    outfile.close();

    return 0;
}
