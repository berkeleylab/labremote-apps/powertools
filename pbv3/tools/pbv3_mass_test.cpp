#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <sstream>
#include <thread>

#include "AMACv2.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBTypeDef.h"
#include "PBv3ConfigTools.h"
#include "PBv3QCFlow.h"
#include "PBv3TBConf.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"
#include "PowerSupplyChannel.h"

//------ SETTINGS
std::string testTag = "";
std::string runNumber = "0-0";
std::string version = "0";
std::string batch = "0";
#ifdef FTDI
std::vector<uint32_t> pbNum = {0};
std::vector<std::string> serial = {};
#else
std::vector<uint32_t> pbNum = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
std::vector<std::string> serial = {};
#endif  // FTDI
PBTypeDef::PBType type = PBTypeDef::PBType::BARREL;

std::string equipConfigFile = "config/equip_testbench.json";
std::string institution = "";
bool skipHV = false;
bool skipSmoke = false;
bool advanced = false;
bool hbipc = false;
//---------------

void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options] panelNum" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -r, --runnumber         Run number to associate with the test "
           "results. (default: "
        << runNumber << ")" << std::endl;
    std::cerr
        << " -g, --testtag         A tag or label to associate with the test "
           "results. (default: "
        << testTag << ")" << std::endl;
    std::cerr
        << " -b, --board             Powerboard numbers to test, separated "
           "by commas. (default: 0 if single testbench, all 0,..,9 if mass "
           "testbench)"
        << std::endl;
    std::cerr << " -t, --type              Powerboard type (default: "
              << (int)type << ")" << std::endl
              << "                             Which is:" << std::endl
              << "                               Barrel =  "
              << (int)PBTypeDef::PBType::BARREL << std::endl
              << "                               R0     =  "
              << (int)PBTypeDef::PBType::EC_R0 << std::endl
              << "                               R1     =  "
              << (int)PBTypeDef::PBType::EC_R1 << std::endl
              << "                               R2     =  "
              << (int)PBTypeDef::PBType::EC_R2 << std::endl
              << "                               R3     =  "
              << (int)PBTypeDef::PBType::EC_R3 << std::endl
              << "                               R4     =  "
              << (int)PBTypeDef::PBType::EC_R4 << std::endl
              << "                               R5     =  "
              << (int)PBTypeDef::PBType::EC_R5 << std::endl;
    std::cerr << " -v, --version           Powerboard version (default: "
              << version << ")" << std::endl;
    std::cerr << " -a, --batch             Powerboard batch (default: " << batch
              << ")" << std::endl;
    std::cerr
        << " -s, --serial            Powerboard serial numbers, separated "
           "by commas. (default: same as -b)"
        << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -i, --institution instition code of where the test is "
                 "performed (default: "
              << institution << ")" << std::endl;
    std::cerr
        << " -f, --advanced          Run all elecrical characterization tests"
        << std::endl;
    std::cerr << "     --skiphv            Skip running HV tests" << std::endl;
    std::cerr << "     --skipsmoke         Skip inital power-up current check"
              << std::endl;
    std::cerr
        << "     --hbipc             Specialized hybrid burn-in powerboard "
           "carrierboard tests"
        << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << " -h, --help              Print this help text" << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

int main(int argc, char *argv[]) {
    //
    // Parse input options to configure test
    //
    if (argc < 2) {
        usage(argv);
        return 1;
    }

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"runnumber", required_argument, 0, 'r'},
            {"testtag", required_argument, 0, 'g'},
            {"board", required_argument, 0, 'b'},
            {"type", required_argument, 0, 't'},
            {"version", required_argument, 0, 'v'},
            {"batch", required_argument, 0, 'a'},
            {"serial", required_argument, 0, 's'},
            {"equip", required_argument, 0, 'e'},
            {"institution", required_argument, 0, 'i'},
            {"advanced", no_argument, 0, 'f'},
            {"skiphv  ", no_argument, 0, 1},
            {"skipsmoke", no_argument, 0, 2},
            {"hbipc", no_argument, 0, 3},
            {"debug", no_argument, 0, 'd'},
            {"help", no_argument, 0, 'h'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "c:r:g:b:t:v:a:s:e:i:fmdh", long_options,
                        &option_index);
        if (c == -1) break;

        switch (c) {
            case 1:
                skipHV = true;
                break;
            case 2:
                skipSmoke = true;
                break;
            case 3:
                skipHV = true;
                hbipc = true;
                break;
            case 'r':
                runNumber = optarg;
                break;
            case 'g':
                testTag = optarg;
                break;
            case 'b': {
                pbNum.clear();
                std::stringstream pb_stream(optarg);
                std::string pb_substr;
                while (getline(pb_stream, pb_substr, ',')) {
                    pbNum.push_back(stoi(pb_substr));
                }
            } break;
            case 't':
                type = static_cast<PBTypeDef::PBType>(atoi(optarg));
                break;
            case 'v':
                version = optarg;
                break;
            case 'a':
                batch = optarg;
                break;
            case 's': {
                serial.clear();
                std::stringstream s_stream(optarg);
                std::string s_substr;
                while (getline(s_stream, s_substr, ',')) {
                    serial.push_back(s_substr);
                }
            } break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 'i':
                institution = optarg;
                break;
            case 'f':
                advanced = true;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (argc - optind < 1) {
        std::cerr << "Required paths missing." << std::endl;
        std::cerr << std::endl;
        usage(argv);
        return 1;
    }

    //
    // Serial number formatting
    bool serialGiven = !serial.empty();
    if (serialGiven)  // transorm to full serial number
    {
        std::transform(serial.begin(), serial.end(), serial.begin(),
                       [](const std::string &num) -> std::string {
                           std::stringstream ss;
                           if (type == PBTypeDef::PBType::BARREL) {  // barrel
                               ss << "20USBP0" << version;
                           } else {  // endcap
                               ss << "20USEP" << (int)type;
                           }
                           ss << std::setw(2) << std::setfill('0') << batch;
                           ss << std::setw(4) << std::setfill('0') << num;
                           return ss.str();
                       });
    } else  // user powerboard numbers
    {
        std::transform(
            pbNum.begin(), pbNum.end(), std::back_inserter(serial),
            [](uint32_t num) -> std::string { return std::to_string(num); });
    }

    if (pbNum.size() != serial.size()) {
        std::cerr << "Number of serial numbers must match number of boards "
                     "specified by optional argument -b."
                  << std::endl;
        usage(argv);
        return 1;
    }

    std::string panelDir = "panel" + std::string(argv[optind++]);

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " Hardware configuration: " << equipConfigFile;
    logger(logDEBUG) << " Institution: " << institution;
    logger(logDEBUG) << " Panel directory: " << panelDir;
    logger(logDEBUG) << " Panel type: " << (int)type
                     << " (=" << PBTypeDef::mapTypeToString.at(type) << ")";
    if (logIt::loglevel >= logDEBUG) {
        logger(logDEBUG) << " (PB number, serial number): ";
        for (int i = 0; i < pbNum.size(); i++) {
            std::cout << "(" << pbNum[i] << ", " << serial[i] << ")"
                      << std::endl;
        }
    }

    // R3/R4/R5 speciality: only even slots are populated with AMACs, do not use
    // uneven slots
    if (type == PBTypeDef::PBType::EC_R3 || type == PBTypeDef::PBType::EC_R4 ||
        type == PBTypeDef::PBType::EC_R5) {
        for (int i = 0; i < pbNum.size(); i++) {
            if (pbNum[i] % 2 == 1) {
                logger(logERROR)
                    << "Can only test even powerboard slots for R3/R4/R5 "
                       "powerboards! Please remove odd ones!";
                if (type == PBTypeDef::PBType::EC_R3) {
                    logger(logERROR) << "You are running R3 powerboards: the "
                                        "odd AMACs are tested "
                                        "automatically with the even ones";
                }
                return 1;
            }
        }
    }

    //
    // Create output directories
    //

    std::string startTime =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    // Create panel subfolder
    PBv3Utils::createDirectory(panelDir);

    // Create date subfolder under panel subfolder
    PBv3Utils::createDirectory(panelDir + "/" + startTime);

    // Create powerboard subfolders
    for (uint8_t i = 0; i < serial.size(); i++)
        PBv3Utils::createDirectory(panelDir + "/" + startTime + "/pb" +
                                   serial[i]);

    //
    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    // Check if institution is provided
    if (institution == "") {
        logger(logERROR) << "Please specify institution!";
        return 1;
    }

    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;
    /*
    if (tb->getLVPS()->getPowerSupply()->identify() == "SY5527") {
        srand(time(NULL));
        int sleep_time = 10 + rand() % 100;
        logger(logINFO) << "sleep " << sleep_time
                        << " seconds to avoid traffic conflicts";
        std::this_thread::sleep_for(std::chrono::seconds(sleep_time));
    }
    */
    bool doHV = (tb->getHVPS() != nullptr and !skipHV);

    // Power up the testbench
    tb->powerTBOn();

    // Make sure to start in off state
    logger(logINFO) << "Turn off PS";
    if (doHV) tb->powerHVOff();
    tb->powerLVOff();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // Turn on power
    logger(logINFO) << "Turn on LV fully";
    tb->powerLVOn();
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    float poweroncurrent = tb->getVinCurrent();
    logger(logINFO) << "LV current: " << poweroncurrent << "A";
    if (!skipSmoke && poweroncurrent > 0.7) {
        tb->powerLVOff();
        logger(logERROR) << "Power-up current too high. Stopping test..";
        return 1;
    }

    // Create test tracking structure
    std::vector<PBv3QCFlow> pbs;
    std::map<int, PBv3QCFlow> pbs_secondary_map;
    for (uint32_t i = 0; i < pbNum.size(); i++) {
        // get PBType, this type specification is needed for R3
        tb->setPBPanelType(type);

        PBv3QCFlow pb(pbNum[i], doHV, hbipc);
        pb.setSerial(serial[i]);
        pbs.push_back(pb);

        // R3 speciality: run on second AMAC if first successful (the second
        // AMAC is always in an uneven slot)
        if (type == PBTypeDef::PBType::EC_R3) {
            int secondaryPbNum = pbNum[i] + 1;

            PBv3QCFlow pb_secondary(secondaryPbNum, doHV, hbipc);
            pb_secondary.setSerial(serial[i]);
            pbs_secondary_map[pbNum[i]] = pb_secondary;
        }
    }

    //
    // Perform diagnostic tests on mass test panel and powerboards.
    std::string pbDir;

    //
    // Basic functionality tests
    //

    // Is Alive version
    {
        for (PBv3QCFlow &pb : pbs) {
            // Output file
            pbDir = panelDir + "/" + startTime + "/pb" + pb.serial();
            std::string fileName = pbDir + "/" + startTime + "_pbv3-alive.json";
            logger(logINFO) << "Results stored in " << fileName;
            std::fstream outfile(fileName, std::ios::out);
            if (!outfile.is_open()) {
                logger(logERROR)
                    << "Unable to create results file " << fileName;
                return 2;
            }

            json diagSum;

            diagSum["program"] = argv[0];
            diagSum["runNumber"] = runNumber;
            diagSum["testTag"] = testTag;

            // run test on first Pb
            json testResultPb1 = pb.runAliveTests(tb);

            // if there is a secondary AMAC associated...
            if (pbs_secondary_map.find(pb.num()) != pbs_secondary_map.end()) {
                // ... perform alive test and further tests if alive test passed
                // for primary Pb
                if (pb.didAliveTestPass()) {
                    // ... perform test on secondary Pb
                    json testResultPb2 =
                        pbs_secondary_map[pb.num()].runAliveTests(tb);

                    // merge results
                    for (int i = 0; i < testResultPb2["tests"].size(); i++) {
                        PBv3Utils::renameKeySuffix(
                            testResultPb2["tests"][i]["results"], "_AMAC1");
                        PBv3Utils::mergeResult(testResultPb1["tests"][i],
                                               testResultPb2["tests"][i], true);
                    }
                }
            }

            diagSum.merge_patch(testResultPb1);
            diagSum["config"]["institution"] = institution;

            outfile << std::setw(4) << diagSum << std::endl;
            outfile.close();
        }
    }

    // Functionality checks
    {
        for (PBv3QCFlow &pb : pbs) {
            // Output file
            pbDir = panelDir + "/" + startTime + "/pb" + pb.serial();
            std::string fileName =
                pbDir + "/" + startTime + "_pbv3-functionality.json";
            logger(logINFO) << "Results stored in " << fileName;
            std::fstream outfile(fileName, std::ios::out);
            if (!outfile.is_open()) {
                logger(logERROR)
                    << "Unable to create results file " << fileName;
                return 2;
            }

            // Output structure
            json diagSum;

            diagSum["program"] = argv[0];
            diagSum["runNumber"] = runNumber;
            diagSum["testTag"] = testTag;

            // run test on first Pb
            json testResultPb1 = pb.runBasicTests(tb);

            // if there is a secondary AMAC associated...
            if (pbs_secondary_map.find(pb.num()) != pbs_secondary_map.end()) {
                // then perform test on secondary Pb
                json testResultPb2 =
                    pbs_secondary_map[pb.num()].runBasicTests(tb);

                // merge results
                PBv3Utils::renameKeySuffix(testResultPb2["config"]["results"],
                                           "_AMAC1");
                PBv3Utils::mergeResult(testResultPb1["config"],
                                       testResultPb2["config"]);
                for (int i = 0; i < testResultPb2["tests"].size(); i++) {
                    PBv3Utils::renameKeySuffix(
                        testResultPb2["tests"][i]["results"], "_AMAC1");
                    PBv3Utils::mergeResult(testResultPb1["tests"][i],
                                           testResultPb2["tests"][i], true);
                }
            }

            diagSum.merge_patch(testResultPb1);
            diagSum["config"]["institution"] = institution;

            outfile << std::setw(4) << diagSum << std::endl;
            outfile.close();
        }
    }

    //
    // Run advanced characterization tests
    //
    if (advanced) {
        for (PBv3QCFlow &pb : pbs) {
            // Output file
            pbDir = panelDir + "/" + startTime + "/pb" + pb.serial();
            std::string fileName =
                pbDir + "/" + startTime + "_pbv3-advanced.json";
            logger(logINFO) << "Results stored in " << fileName;
            std::fstream outfile(fileName, std::ios::out);
            if (!outfile.is_open()) {
                logger(logERROR)
                    << "Unable to create results file " << fileName;
                return 2;
            }

            // Output structure
            json testSum;
            testSum["program"] = argv[0];
            testSum["runNumber"] = runNumber;
            testSum["testTag"] = testTag;

            // run test on first Pb
            json testResultPb1 = pb.runAdvancedTests(tb);

            // if there is a secondary AMAC associated...
            if (pbs_secondary_map.find(pb.num()) != pbs_secondary_map.end()) {
                // then perform test on secondary Pb
                json testResultPb2 =
                    pbs_secondary_map[pb.num()].runAdvancedTests(tb);

                // merge results
                PBv3Utils::renameKeySuffix(testResultPb2["config"]["results"],
                                           "_AMAC1");
                PBv3Utils::mergeResult(testResultPb1["config"],
                                       testResultPb2["config"]);
                for (int i = 0; i < testResultPb2["tests"].size(); i++) {
                    PBv3Utils::renameKeySuffix(
                        testResultPb2["tests"][i]["results"], "_AMAC1");
                    PBv3Utils::mergeResult(testResultPb1["tests"][i],
                                           testResultPb2["tests"][i], true);
                }
            }

            testSum.merge_patch(testResultPb1);
            testSum["config"]["institution"] = institution;

            // save
            outfile << std::setw(4) << testSum << std::endl;
            outfile.close();
        }
    }

    logger(logINFO) << "Power off";

    tb->powerLVOff();
    tb->loadOff(0);
    if (doHV) tb->powerHVOff();
    tb->powerTBOff();

    return 0;
}
