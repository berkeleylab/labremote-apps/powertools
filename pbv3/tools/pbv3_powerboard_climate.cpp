#include <DataSinkConf.h>
#include <IDataSink.h>
#include <getopt.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <nlohmann/json.hpp>
#include <thread>

#include "AMACv2.h"
#include "EndeavourComException.h"
#include "Logger.h"
#include "PBv3ConfigTools.h"
#include "PBv3TBConf.h"
#include "PBv3TBSingle.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"

//------ SETTINGS
std::vector<uint32_t> pbNum = {0};
std::string configfile = "config.json";
std::string equipConfigFile = "config/equip_testbench.json";
std::string streamName = "";
std::string outDir = "";
std::string DB_MeasurementName = "";
std::string DB_ChannelName = "";
//---------------

bool quit = false;
void cleanup(int signum) { quit = true; }

#define PERIOD_LONG 10  // number of seconds of monitoring between calibrations
#define PERIOD_MONITOR 1  // number of seconds to wait between readings
void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
    std::cerr << std::endl;
    std::cerr << "List of options:" << std::endl;
    std::cerr
        << " -c, --config            Config with AMAC calibrations. (default: "
        << configfile << ")" << std::endl;
    std::cerr
        << " -b, --board             Powerboard numbers to test, separated "
           "by commas. (default: 0 if single testbench, all 0,..,9 if mass "
           "testbench)"
        << std::endl;
    std::cerr
        << " -e, --equip config.json Equipment configuration file (default: "
        << equipConfigFile << ")" << std::endl;
    std::cerr << " -s, --streamName Stream  Name of the stream from equipment "
                 "configuration"
              << std::endl;
    std::cerr << " -o, --outDir output directory to save the csv file to "
              << std::endl;
    std::cerr << " -d, --debug             Enable more verbose printout"
              << std::endl;
    std::cerr << std::endl;
    std::cerr << std::endl;
}

void getStreamConfig() {
    std::ifstream i(equipConfigFile);
    nlohmann::json config;
    i >> config;
    json stream_config;
    for (const auto &datastream : config["streamChannels"]) {
        if (datastream["name"] == streamName) stream_config = datastream;
    }
    for (const auto &kv : stream_config.items()) {
        if (kv.key() == "DB_MeasurementName") DB_MeasurementName = kv.value();
        if (kv.key() == "DB_ChannelName") DB_ChannelName = kv.value();
    }
}

int main(int argc, char *argv[]) {
    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"config", required_argument, 0, 'c'},
            {"board", required_argument, 0, 'b'},
            {"equip", required_argument, 0, 'e'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

        c = getopt_long(argc, argv, "c:b:e:s:o:d", long_options, &option_index);
        if (c == -1) break;

        switch (c) {
            case 'c':
                configfile = optarg;
                break;
            case 'b': {
                pbNum.clear();
                std::stringstream pb_stream(optarg);
                std::string pb_substr;
                while (getline(pb_stream, pb_substr, ',')) {
                    pbNum.push_back(stoi(pb_substr));
                }
            } break;
            case 'e':
                equipConfigFile = optarg;
                break;
            case 's':
                streamName = optarg;
                break;
            case 'o':
                outDir = optarg;
                break;
            case 'd':
                logIt::incrDebug();
                break;
            default:
                std::cerr << "Invalid option supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
        }
    }

    logger(logDEBUG) << "Settings";
    logger(logDEBUG) << " PB number: ";
    if (logIt::loglevel >= logDEBUG) {
        for (int i = 0; i < pbNum.size(); i++) {
            std::cout << pbNum[i] << ",";
        }
        std::cout << std::endl;
    }

    // Register interrupt for cleanup
    signal(SIGINT, cleanup);
    signal(SIGTERM, cleanup);

    DataSinkConf ds;
    ds.setHardwareConfig(equipConfigFile);
    std::shared_ptr<IDataSink> stream = ds.getDataStream(streamName);

    getStreamConfig();

    // Get default config
    json defconfig;
    if (!configfile.empty()) {
        std::ifstream fh_in(configfile);
        if (fh_in.is_open()) fh_in >> defconfig;
    }

    // Create and initialize the testbench
    PBv3TBConf factory_pbv3tb(equipConfigFile);
    std::shared_ptr<PBv3TB> tb = factory_pbv3tb.getPBv3TB("default");
    if (tb == nullptr) return 1;

    // Power up the testbench
    tb->powerTBOn();

    // Make sure to start in off state
    logger(logINFO) << "Turn off PS";
    tb->powerHVOff();
    tb->powerLVOff();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // Turn on power
    logger(logINFO) << "Turn on LV fully";
    tb->powerLVOn();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    float poweroncurrent = tb->getVinCurrent();
    logger(logINFO) << "LV current: " << poweroncurrent << "A";
    if (poweroncurrent > 0.7) {
        tb->powerLVOff();
        logger(logERROR) << "Power-up current too high. Stopping test..";
        return 1;
    }

    std::vector<std::shared_ptr<AMACv2>> amacs;

    for (uint32_t i = 0; i < pbNum.size(); i++) {
        logger(logINFO) << "Init AMAC on PB " << pbNum[i];
        std::shared_ptr<AMACv2> amac = tb->getPB(pbNum[i]);
        try {
            amac->init();
            PBv3ConfigTools::configAMAC(amac, defconfig, false);
            amac->initRegisters();
            PBv3ConfigTools::saveConfigAMAC(amac, defconfig);
        } catch (EndeavourComException &e) {
            logger(logERROR) << "Unable to initialize AMACv2 on " << pbNum[i];
            logger(logERROR) << e.what();
            continue;
        }

        amacs.push_back(amac);
    }

    if (amacs.size() < 1) {
        logger(logERROR) << "No working AMAC ...";
        return 1;
    }

    //
    // Start running tests in a loop forever!
    //
    std::string fileName =
        outDir + "/" + DB_ChannelName + "_" +
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) + ".csv";

    FILE *outputFile1;
    if (outDir != "") {
        system(("mkdir -p " + outDir).c_str());
        logger(logINFO) << "Saving monitoring result to " << fileName;
        outputFile1 = fopen(fileName.c_str(), "a");
        for (uint32_t i = 0; i < amacs.size(); i++) {
            fprintf(outputFile1,
                    "PB%d_NTCx, PB%d_NTCy, PB%d_NTCpb, PB%d_PTAT, PB%d_CTAT, ",
                    pbNum[i], pbNum[i], pbNum[i], pbNum[i], pbNum[i], pbNum[i]);
            ;
        }
        fprintf(
            outputFile1,
            "active_temp, active_hum, active_dew, active_deltaT, active_ntc0, "
            "active_ntc1, active_ntc2, active_ntc3, active_ntcave \n");
        fclose(outputFile1);
    }

    while (!quit) {
        FILE *outputFile;
        if (outDir != "") {
            outputFile = fopen(fileName.c_str(), "a");
        }

        //
        // Rerun the calibration
        // Run all calibrations
        for (uint32_t i = 0; i < amacs.size(); i++) {
            logger(logINFO) << "Calibrating PB" << pbNum[i];
            json config = defconfig;
            config.merge_patch(
                PBv3ConfigTools::calibrateSlope(amacs[i], tb->getCalDAC()));
            config.merge_patch(PBv3ConfigTools::calibrateOffset(amacs[i]));
            config.merge_patch(PBv3ConfigTools::tuneNTC(amacs[i]));
            config.merge_patch(PBv3ConfigTools::calibrateNTC(amacs[i]));
            config.merge_patch(PBv3ConfigTools::calibrateTemperature(amacs[i]));
            PBv3ConfigTools::saveConfigAMAC(amacs[i], config);
            PBv3ConfigTools::configAMAC(amacs[i], config, true);
            amacs[i]->initRegisters();
        }

        uint32_t test = 0;

        // Start monitoring in a loop
        for (uint32_t monIdx = 0; monIdx < PERIOD_LONG / PERIOD_MONITOR;
             monIdx++) {
            std::vector<double> temp_NTCx, temp_NTCy, temp_NTCpb, temp_PTAT,
                temp_CTAT;
            for (uint32_t i = 0; i < amacs.size(); i++) {
                temp_NTCx.push_back(amacs[i]->temperatureX());
                temp_NTCy.push_back(amacs[i]->temperatureY());
                temp_NTCpb.push_back(amacs[i]->temperaturePB());
                temp_PTAT.push_back(amacs[i]->temperaturePTAT());
                temp_CTAT.push_back(amacs[i]->temperatureCTAT());
            }

            double active_temp = tb->getActiveTemp();
            double active_hum = tb->getActiveHum();
            double active_dew = tb->getActiveDew();

            double active_ntc0 = tb->getNTC(0);
            double active_ntc1 = tb->getNTC(1);
            double active_ntc2 = tb->getNTC(2);
            double active_ntc3 = tb->getNTC(3);
            double active_ntcave =
                (active_ntc0 + active_ntc1 + active_ntc2 + active_ntc3) / 4.0;

            std::time_t time_now = std::time(0);
            std::tm *now = std::localtime(&time_now);
            std::cout << "MONITOR_TIME:---> " << now->tm_year + 1900 << "-"
                      << std::setfill('0') << std::setw(2) << now->tm_mon + 1
                      << "-" << std::setfill('0') << std::setw(2)
                      << now->tm_mday << " " << std::setfill('0')
                      << std::setw(2) << now->tm_hour << ":"
                      << std::setfill('0') << std::setw(2) << now->tm_min << ":"
                      << std::setfill('0') << std::setw(2) << now->tm_sec
                      << " <---:MONITOR_TIME" << std::endl;
            std::cout << "MONITOR_TIME_SECONDS:---> " << time_now
                      << " <---:MONITOR_TIME_SECONDS" << std::endl;
            std::cout << "ActiveBoard:---> " << active_temp << ", "
                      << active_hum << ", " << active_dew << ", "
                      << active_temp - active_dew << ", " << active_ntc0 << ", "
                      << active_ntc1 << ", " << active_ntc2 << ", "
                      << active_ntc3 << ", " << active_ntcave
                      << " <---:ActiveBoard" << std::endl;
            for (uint32_t i = 0; i < amacs.size(); i++) {
                std::cout << "Powerboard" << pbNum[i] << ":---> "
                          << temp_NTCx[i] << ", " << temp_NTCy[i] << ", "
                          << temp_NTCpb[i] << ", " << temp_PTAT[i] << ", "
                          << temp_CTAT[i] << " <---:Powerboard" << pbNum[i]
                          << std::endl;
            }

            if (outDir != "") {
                for (uint32_t i = 0; i < amacs.size(); i++) {
                    fprintf(outputFile, "%6.3f, %6.3f, %6.3f, %6.3f, %6.3f, ",
                            temp_NTCx[i], temp_NTCy[i], temp_NTCpb[i],
                            temp_PTAT[i], temp_CTAT[i]);
                }
                fprintf(outputFile,
                        "%6.3f, %6.3f, %6.3f, %6.3f, %6.3f, %6.3f, %6.3f, "
                        "%6.3f, %6.3f \n",
                        active_temp, active_hum, active_dew,
                        active_temp - active_dew, active_ntc0, active_ntc1,
                        active_ntc2, active_ntc3, active_ntcave);
            }

            // push data to influx db
            if (streamName != "" && streamName != "DUMMY") {
                stream->setTag("Channel", DB_ChannelName);
                stream->startMeasurement(DB_MeasurementName,
                                         std::chrono::system_clock::now());

                for (uint32_t i = 0; i < amacs.size(); i++) {
                    stream->setField("AMACNTCpb_P" + std::to_string(pbNum[i]),
                                     temp_NTCpb[i]);
                    stream->setField("AMACNTCx_P" + std::to_string(pbNum[i]),
                                     temp_NTCx[i]);
                    stream->setField("AMACNTCy_P" + std::to_string(pbNum[i]),
                                     temp_NTCy[i]);
                    stream->setField("AMACPTAT_P" + std::to_string(pbNum[i]),
                                     temp_PTAT[i]);
                    stream->setField("AMACCTAT_P" + std::to_string(pbNum[i]),
                                     temp_CTAT[i]);
                }
                stream->setField("active_ntc0", active_ntc0);
                stream->setField("active_ntc1", active_ntc1);
                stream->setField("active_ntc2", active_ntc2);
                stream->setField("active_ntc3", active_ntc3);
                stream->setField("active_ntcave", active_ntcave);
                stream->setField("active_temp", active_temp);
                stream->setField("active_hum", active_hum);
                stream->setField("active_dew", active_dew);
                stream->setField("active_deltaT", active_temp - active_dew);

                stream->recordPoint();
                stream->endMeasurement();
            }

            std::this_thread::sleep_for(std::chrono::seconds(PERIOD_MONITOR));
        }
        if (outDir != "") fclose(outputFile);
    }

    if (outDir != "")
        logger(logINFO) << "Monitoring result saved to " << fileName;
    logger(logINFO) << "Turn off LV";
    tb->powerLVOff();

    return 0;
}
