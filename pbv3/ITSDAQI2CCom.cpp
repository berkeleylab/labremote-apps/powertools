#include "ITSDAQI2CCom.h"

#include <unistd.h>

#include <iomanip>
#include <iostream>

#include "ComIOException.h"

ITSDAQI2CCom::ITSDAQI2CCom(uint8_t deviceAddr, std::shared_ptr<ITSDAQCom> com)
    : I2CCom(deviceAddr), m_com(com) {}

ITSDAQI2CCom::~ITSDAQI2CCom() {}

void ITSDAQI2CCom::write_reg32(uint32_t address, uint32_t data) {
    std::vector<uint16_t> command(7);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | (address & 0xFF);
    command[3] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 24) & 0xFF);
    command[4] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 16) & 0xFF);
    command[5] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[6] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg32 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg24(uint32_t address, uint32_t data) {
    std::vector<uint16_t> command(6);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | (address & 0xFF);
    command[3] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 16) & 0xFF);
    command[4] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[5] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg16 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg16(uint32_t address, uint16_t data) {
    std::vector<uint16_t> command(5);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | (address & 0xFF);
    command[3] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[4] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg16 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg8(uint32_t address, uint8_t data) {
    std::vector<uint16_t> command(4);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | (address & 0xFF);
    command[3] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom write_reg8 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg32(uint32_t data) {
    std::vector<uint16_t> command(6);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 24) & 0xFF);
    command[3] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 16) & 0xFF);
    command[4] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[5] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg32 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg24(uint32_t data) {
    std::vector<uint16_t> command(5);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 16) & 0xFF);
    command[3] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[4] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg32 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg16(uint16_t data) {
    std::vector<uint16_t> command(4);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | ((data >> 8) & 0xFF);
    command[3] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_reg16 failed: no opcode reply");
}

void ITSDAQI2CCom::write_reg8(uint8_t data) {
    std::vector<uint16_t> command(3);

    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] =
        ITSDAQ_I2C_CMD_SEND1 | ((data >> 0) & 0xFF) | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom write_reg8 failed: no opcode reply");
}

void ITSDAQI2CCom::write_block(uint32_t address,
                               const std::vector<uint8_t> &data) {
    std::vector<uint16_t> command(3 + data.size());

    uint32_t cmdidx = 0;
    command[cmdidx++] = 0x0000 | 15;
    command[cmdidx++] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[cmdidx++] = ITSDAQ_I2C_CMD_SEND1 | (address & 0xFF);
    for (uint8_t byte : data) command[cmdidx++] = ITSDAQ_I2C_CMD_SEND1 | byte;
    command[cmdidx - 1] |= ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_block failed: no opcode reply");
}

void ITSDAQI2CCom::write_block(const std::vector<uint8_t> &data) {
    std::vector<uint16_t> command(2 + data.size());

    uint32_t cmdidx = 0;
    command[cmdidx++] = 0x0000 | 15;
    command[cmdidx++] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    for (uint8_t byte : data) command[cmdidx++] = ITSDAQ_I2C_CMD_SEND1 | byte;
    command[cmdidx - 1] |= ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException(
            "ITSDAQI2CCom write_block failed: no opcode reply");
}

uint32_t ITSDAQI2CCom::read_reg32(uint32_t address) {
    std::vector<uint16_t> command(6);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | address & 0xFF;
    command[3] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[4] = ITSDAQ_I2C_CMD_READ2;
    command[5] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg32 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return (retopcode.payload()[2] << 16) | (retopcode.payload()[3] << 0);
}

uint32_t ITSDAQI2CCom::read_reg24(uint32_t address) {
    std::vector<uint16_t> command(5);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | address & 0xFF;
    command[3] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[4] = ITSDAQ_I2C_CMD_READ1;
    command[5] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg32 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return (retopcode.payload()[2] << 16) | (retopcode.payload()[3] << 0);
}

uint16_t ITSDAQI2CCom::read_reg16(uint32_t address) {
    std::vector<uint16_t> command(5);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | address & 0xFF;
    command[3] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[4] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg16 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return retopcode.payload()[2];
}

uint8_t ITSDAQI2CCom::read_reg8(uint32_t address) {
    std::vector<uint16_t> command(5);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[2] = ITSDAQ_I2C_CMD_SEND1 | address & 0xFF;
    command[3] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[4] = ITSDAQ_I2C_CMD_READ1 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg8 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return retopcode.payload()[2] & 0xFF;
}

uint32_t ITSDAQI2CCom::read_reg32() {
    std::vector<uint16_t> command(4);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[2] = ITSDAQ_I2C_CMD_READ2;
    command[3] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg32 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return (retopcode.payload()[2] << 16) | (retopcode.payload()[3] << 0);
}

uint32_t ITSDAQI2CCom::read_reg24() {
    std::vector<uint16_t> command(4);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[2] = ITSDAQ_I2C_CMD_READ1;
    command[3] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg32 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return (retopcode.payload()[2] << 16) | (retopcode.payload()[3] << 0);
}

uint16_t ITSDAQI2CCom::read_reg16() {
    std::vector<uint16_t> command(3);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[2] = ITSDAQ_I2C_CMD_READ2 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg16 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return retopcode.payload()[2];
}

uint8_t ITSDAQI2CCom::read_reg8() {
    std::vector<uint16_t> command(3);
    command[0] = 0x0000 | 15;
    command[1] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    command[2] = ITSDAQ_I2C_CMD_READ1 | ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_reg8 failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);
    return (retopcode.payload()[2] & 0xFF);
}

void ITSDAQI2CCom::read_block(uint32_t address, std::vector<uint8_t> &data) {
    uint32_t n2reads = data.size() / 2;
    uint32_t n1reads = data.size() % 2;

    std::vector<uint16_t> command(4 + n2reads + n1reads);

    uint32_t cmdidx = 0;
    command[cmdidx++] = 0x0000 | 15;
    command[cmdidx++] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 0;
    command[cmdidx++] = ITSDAQ_I2C_CMD_SEND1 | address & 0xFF;
    command[cmdidx++] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    for (uint32_t i = 0; i < n2reads; i++)
        command[cmdidx++] = ITSDAQ_I2C_CMD_READ2;
    for (uint32_t i = 0; i < n1reads; i++)
        command[cmdidx++] = ITSDAQ_I2C_CMD_READ1;
    command[cmdidx - 1] |= ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_block failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);

    cmdidx = 0;
    uint32_t pidx = 2;
    for (uint32_t i = 0; i < n2reads; i++) {
        data[cmdidx++] = (retopcode.payload()[pidx] >> 8) & 0xFF;
        data[cmdidx++] = (retopcode.payload()[pidx++] >> 0) & 0xFF;
    }
    for (uint32_t i = 0; i < n1reads; i++) {
        data[cmdidx++] = (retopcode.payload()[pidx++] >> 0) & 0xFF;
    }
}

void ITSDAQI2CCom::read_block(std::vector<uint8_t> &data) {
    uint32_t n2reads = data.size() / 2;
    uint32_t n1reads = data.size() % 2;

    std::vector<uint16_t> command(2 + n2reads + n1reads);

    uint32_t cmdidx = 0;
    command[cmdidx++] = 0x0000 | 15;
    command[cmdidx++] =
        ITSDAQ_I2C_CMD_START | ITSDAQ_I2C_CMD_SEND1 | (deviceAddr() << 1) | 1;
    for (uint32_t i = 0; i < n2reads; i++)
        command[cmdidx++] = ITSDAQ_I2C_CMD_READ2;
    for (uint32_t i = 0; i < n1reads; i++)
        command[cmdidx++] = ITSDAQ_I2C_CMD_READ1;
    command[cmdidx - 1] |= ITSDAQ_I2C_CMD_STOP;

    ITSDAQOpCode opcode(0, ITSDAQ_OPCODE_TWOWIRE, command);
    ITSDAQPacket packet(0, {opcode});
    m_com->send(packet);
    packet = m_com->receive();
    if (packet.nOpcodes() != 1)
        throw ComIOException("ITSDAQI2CCom read_block failed: no opcode reply");

    ITSDAQOpCode retopcode = packet.opcode(0);

    cmdidx = 0;
    uint32_t pidx = 2;
    for (uint32_t i = 0; i < n2reads; i++) {
        data[cmdidx++] = (retopcode.payload()[pidx] >> 8) & 0xFF;
        data[cmdidx++] = (retopcode.payload()[pidx++] >> 0) & 0xFF;
    }
    for (uint32_t i = 0; i < n1reads; i++) {
        data[cmdidx++] = (retopcode.payload()[pidx++] >> 0) & 0xFF;
    }
}

void ITSDAQI2CCom::lock() {}

void ITSDAQI2CCom::unlock() {}
