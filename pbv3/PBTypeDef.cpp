#include "PBTypeDef.h"

namespace PBTypeDef {

const std::unordered_map<PBType, std::string, PBTypeHash> mapTypeToString = {
    {PBType::BARREL, "Barrel"}, {PBType::EC_R0, "R0"}, {PBType::EC_R1, "R1"},
    {PBType::EC_R2, "R2"},      {PBType::EC_R3, "R3"}, {PBType::EC_R4, "R4"},
    {PBType::EC_R5, "R5"}};

bool isPrimaryR3(PBType type, int pbNum) {
    // check if we are actually dealing with an R3 type
    if (type != PBType::EC_R3) {
        throw std::runtime_error(
            "Type is not R3, but " + mapTypeToString.at(type) +
            ". Determination of primary/secondary AMAC makes no "
            "sense in that case.");
    }

    // yes we are dealing with an R3 PB
    // destinguish types according to even/odd pbNum
    // EC_R3_0
    if (pbNum % 2 == 0) return true;
    // EC_R3_1
    return false;
}

int getPBTypePADID(PBType type, int pbNum) {
    static const std::unordered_map<PBType, int, PBTypeHash> mapTypeToPADID = {
        {PBType::BARREL, 0}, {PBType::EC_R0, 2}, {PBType::EC_R1, 3},
        {PBType::EC_R2, 4},  {PBType::EC_R4, 7}, {PBType::EC_R5, 9}};

    if (type == PBType::EC_R3) {
        // R3_0
        if (isPrimaryR3(type, pbNum)) {
            return 5;
        } else {
            // R3_1
            return 6;
        }
    }

    return mapTypeToPADID.at(type);
}

}  // namespace PBTypeDef