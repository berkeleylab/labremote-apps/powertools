#include "PBv3Utils.h"

#include "Logger.h"

using nlohmann::json;

#include <sys/stat.h>
#include <sys/types.h>

#include <set>

std::string PBv3Utils::getTimeAsString(
    std::chrono::system_clock::time_point t) {
    auto as_time_t = std::chrono::system_clock::to_time_t(t);
    struct tm tm;
    char some_buffer[128];
    if (::gmtime_r(&as_time_t, &tm))
        if (std::strftime(some_buffer, sizeof(some_buffer),
                          "%Y_%m_%d-%H:%M:%SZ%z", &tm))
            return std::string(some_buffer);
    throw std::runtime_error("Failed to get current date as string");
}

void PBv3Utils::printResults(const json &results) {
    // Ignore the following common results
    std::set<std::string> ignorekeys = {"TIMESTART", "TIMEEND"};

    // Determine widths of each column
    std::unordered_map<std::string, uint32_t> colw;
    for (const auto &kv : results.items()) {
        uint32_t mycolw = std::max<uint32_t>(
            kv.key().size() + 2, 10);  // key size +2 pad with minimum of 8
        if (kv.value().is_string())
            mycolw = std::max<uint32_t>(
                mycolw, kv.value().get<std::string>().length() + 2);

        colw[kv.key()] = mycolw;
    }

    // Print header
    uint32_t maxlen = 1;
    for (const auto &kv : results.items()) {
        if (ignorekeys.count(kv.key()) == 1) continue;

        std::cout << std::setw(colw[kv.key()]) << kv.key();

        if (kv.value().is_array()) maxlen = kv.value().size();
    }
    std::cout << std::endl;

    // Print values
    for (uint32_t i = 0; i < maxlen; i++) {
        for (const auto &kv : results.items()) {
            if (ignorekeys.count(kv.key()) == 1) continue;

            uint32_t pad = colw[kv.key()];

            const json &value =
                (kv.value().is_array()) ? kv.value()[i] : kv.value();

            std::cout << std::setw(pad);
            if (value.is_string())
                std::cout << value.get<std::string>();
            else if (value.is_number_float())
                std::cout << std::scientific << std::setprecision(2)
                          << value.get<float>();
            else if (value.is_number_integer())
                std::cout << value.get<int32_t>();
            else
                std::cout << value.get<uint32_t>();
        }
        std::cout << std::endl;
    }
}

void PBv3Utils::createDirectory(const std::string &path) {
    if (mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1) {
        if (errno == EEXIST) { /* Path already exists, ok */
        }

        else {
            logger(logERROR) << "Cannot create folder " << path
                             << ". Error: " << strerror(errno);
            throw std::runtime_error(strerror(errno));
        }
    }
}

void PBv3Utils::mergeResult(nlohmann::json &baseResult,
                            const nlohmann::json &addResult,
                            bool mergeTimestart) {
    bool passed = false;
    bool bothContainPassed = false;
    if (baseResult.find("passed") != baseResult.end() &&
        addResult.find("passed") != addResult.end()) {
        passed = baseResult["passed"] && addResult["passed"];
        bothContainPassed = true;
    }

    // use earlier timestart when merging results
    std::string timestart = "";
    if (baseResult["results"].find("TIMESTART") !=
            baseResult["results"].end() &&
        mergeTimestart) {
        timestart = baseResult["results"]["TIMESTART"];
    }

    baseResult.merge_patch(addResult);

    // merge passed
    if (bothContainPassed) {
        baseResult["passed"] = passed;
    }

    // merge timestart
    if (timestart != "" && mergeTimestart) {
        baseResult["results"]["TIMESTART"] = timestart;
    }
}

std::string PBv3Utils::findAndReplace(const std::string &in,
                                      const std::string &what,
                                      const std::string &by) {
    std::string out = in;
    size_t pos = out.find(what);
    while (pos != std::string::npos) {
        out.replace(pos, what.size(), by);
        pos = out.find(what, pos + by.size());
    }
    return out;
}

void PBv3Utils::renameKeyCompContaining(nlohmann::json &inJson,
                                        const std::string &search,
                                        const std::string &by) {
    // make a copy
    nlohmann::json outJson = inJson;
    // list of keys to delete
    std::vector<std::string> toDelete;

    // first create new keys and move the values across in copied json
    for (const auto &element : inJson.items()) {
        const std::string key = element.key();

        if (key.find(search) != std::string::npos) {
            // replace old key
            std::string newKey = PBv3Utils::findAndReplace(key, search, by);
            // update new json
            std::swap(outJson[newKey], element.value());
            toDelete.push_back(key);
        }
    }
    // then delete old keys
    for (const std::string &key : toDelete) {
        outJson.erase(key);
    }

    // call by reference -> replace old json by new json
    inJson = outJson;
}

void PBv3Utils::renameKeySuffix(nlohmann::json &inJson,
                                const std::string &addToEnd) {
    // make a copy
    nlohmann::json outJson = inJson;
    // list of keys to delete
    std::vector<std::string> toDelete;

    // first create new keys and move the values across in copied json
    for (const auto &element : inJson.items()) {
        const std::string key = element.key();

        // replace old key
        std::string newKey = key + addToEnd;
        // update new json
        std::swap(outJson[newKey], element.value());
        toDelete.push_back(key);
    }
    // then delete old keys
    for (const std::string &key : toDelete) {
        outJson.erase(key);
    }

    // call by reference -> replace old json by new json
    inJson = outJson;
}