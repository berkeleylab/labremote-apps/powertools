#include "PBv3TBMassive20190718.h"

// AMAC
#include "EndeavourRawUIO.h"
#include "EndeavourRawUIOMux.h"

// I2C
#include "I2CDevCom.h"
#include "PCA9548ACom.h"

// ADCs
#include "AD799X.h"
#include "MCP3425.h"

// IO expanders
#include "MCP23008.h"

// Register test bench
#include "PBv3TBRegistry.h"
REGISTER_PBV3TB(PBv3TBMassive20190718)

void PBv3TBMassive20190718::initDevices() {
    // Common
    PBv3TBMassive::initDevices();

    // Specific
    m_adc_pwr = std::make_shared<AD799X>(
        3.3, AD799X::AD7993,
        std::make_shared<PCA9548ACom>(0x21, 7, m_i2c_root));

    m_adc_common = std::make_shared<MCP3425>(
        std::make_shared<PCA9548ACom>(0x68, 1, m_i2c_root));

    //
    // PBs
    if (m_pbdev.empty()) {
        for (uint32_t pbNum = 0; pbNum < 10; pbNum++)
            m_pbs[pbNum] = std::make_shared<AMACv2>(
                0, std::unique_ptr<EndeavourRaw>(
                       new EndeavourRawUIO(std::make_shared<UIOCom>(
                           "/dev/uio" + std::to_string(pbNum), 0x10000))));

        // Invert polarities
        for (uint32_t pbNum = 0; pbNum < 5; pbNum++) {
            dynamic_cast<EndeavourRawUIO *>(m_pbs[pbNum]->raw().get())
                ->invertCMDin(true);
            dynamic_cast<EndeavourRawUIO *>(m_pbs[pbNum]->raw().get())
                ->invertCMDout(true);
        }
    } else {
        m_pbsel = std::make_shared<MCP23008>(
            std::make_shared<I2CDevCom>(0x27, m_i2cdev));
        m_pbsel->setIO(0x0);

        std::shared_ptr<UIOCom> pbuio =
            std::make_shared<UIOCom>(m_pbdev, 0x10000);

        // Mapping for the CMDmux patch panel
        m_pbs[3] = std::make_shared<AMACv2>(
            0, std::unique_ptr<EndeavourRaw>(
                   new EndeavourRawUIOMux(0x8, true, true, m_pbsel, pbuio)));
        m_pbs[5] = std::make_shared<AMACv2>(
            0, std::unique_ptr<EndeavourRaw>(
                   new EndeavourRawUIOMux(0xF, true, true, m_pbsel, pbuio)));
        m_pbs[7] = std::make_shared<AMACv2>(
            0, std::unique_ptr<EndeavourRaw>(
                   new EndeavourRawUIOMux(0x5, true, true, m_pbsel, pbuio)));
    }
}
