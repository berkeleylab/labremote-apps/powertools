#include "PBv3TB.h"

#include <stdexcept>

#include "PowerSupplyChannel.h"

void PBv3TB::setConfiguration(const nlohmann::json &config) {}

void PBv3TB::powerTBOn() {}

void PBv3TB::powerTBOff() {}

void PBv3TB::setCalDAC(std::shared_ptr<DACDevice> CalDAC) { m_CalDAC = CalDAC; }

std::shared_ptr<DACDevice> PBv3TB::getCalDAC() { return m_CalDAC; }

void PBv3TB::setLVPS(std::shared_ptr<PowerSupplyChannel> ps) { m_lv = ps; }

std::shared_ptr<PowerSupplyChannel> PBv3TB::getLVPS() { return m_lv; }

void PBv3TB::setHVPS(std::shared_ptr<PowerSupplyChannel> ps) { m_hv = ps; }

std::shared_ptr<PowerSupplyChannel> PBv3TB::getHVPS() { return m_hv; }

void PBv3TB::powerLVOn(float Imax) {
    m_lv->setVoltageLevel(11.0);
    m_lv->setCurrentProtect(Imax);
    m_lv->turnOn();
}

void PBv3TB::powerLVOff() { m_lv->turnOff(); }

void PBv3TB::setVin(double Vout) { m_lv->setVoltageLevel(Vout); }

double PBv3TB::getVinCurrent() { return m_lv->measureCurrent(); }

void PBv3TB::powerHVOn() { m_hv->turnOn(); }

void PBv3TB::powerHVOff() { m_hv->turnOff(); }

void PBv3TB::setPBPanelType(PBTypeDef::PBType type) { m_type = type; }

PBTypeDef::PBType PBv3TB::getPBPanelType() { return m_type; }

int PBv3TB::getPBTypePADID(int pbNum) {
    return PBTypeDef::getPBTypePADID(m_type, pbNum);
}
