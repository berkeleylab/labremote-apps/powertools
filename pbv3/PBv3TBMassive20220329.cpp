#include "PBv3TBMassive20220329.h"

// AMAC
#include "EndeavourComException.h"
#include "EndeavourRawUIO.h"
#include "EndeavourRawUIOMux.h"

// I2C
#include "I2CDevCom.h"
#include "PCA9547Com.h"
#include "SPIDevCom.h"

// ADCs
#include "AD799X.h"
#include "ADS7828.h"
#include "LTC2451.h"
#include "MCP3425.h"
#include "MCP3428.h"

// DACs
#include "DAC5571.h"
#include "DAC5574.h"

// IO expanders
#include "MCP23017.h"
#include "TCA9534.h"

// Climate sensors
#include "NTCSensor.h"
#include "SHT85.h"

// Multiplexers
#include "ComIOException.h"
#include "PGA11x.h"

// Register test bench
#include "PBv3TBRegistry.h"
REGISTER_PBV3TB(PBv3TBMassive20220329)

void PBv3TBMassive20220329::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "zturn_adapter") {
            m_zturn_adapter = kv.value();
        }
        if (kv.key() == "invertCMDin") {
            m_invertCMDin = int(kv.value());
        }
        if (kv.key() == "invertCMDout") {
            m_invertCMDout = int(kv.value());
        }
    }

    PBv3TBMassive::setConfiguration(config);
}

void PBv3TBMassive20220329::initDevices() {
    // Multiplexers
    m_i2c_root = std::make_shared<I2CDevCom>(0x70, m_i2cdev);

    m_i2c_analogue = std::make_shared<PCA9547Com>(0x4c, 1, m_i2c_root);
    m_mux_hv =
        std::make_shared<PGA11x>(std::make_shared<SPIDevCom>(m_hvcurrmuxdev));

    //
    // ADCs
    m_adc_lv0 = std::make_shared<ADS7828>(
        3.3, std::make_shared<PCA9547Com>(0x48, 5, m_i2c_root));
    m_adc_lv1 = std::make_shared<ADS7828>(
        3.3, std::make_shared<PCA9547Com>(0x49, 5, m_i2c_root));

    m_adc_lvset0 = std::make_shared<MCP3428>(
        std::make_shared<PCA9547Com>(0x68, 5, m_i2c_root));
    m_adc_lvset1 = std::make_shared<MCP3428>(
        std::make_shared<PCA9547Com>(0x6C, 5, m_i2c_root));
    m_adc_lvset2 = std::make_shared<MCP3428>(
        std::make_shared<PCA9547Com>(0x6A, 5, m_i2c_root));

    m_adc_hv = std::make_shared<LTC2451>(
        3.25 / 3.24e3, std::make_shared<PCA9547Com>(0x14, 0, m_i2c_root));

    m_adc_pwr = std::make_shared<AD799X>(
        3.3, AD799X::AD7993, std::make_shared<PCA9547Com>(0x23, 7, m_i2c_root));

    m_adc_common = std::make_shared<MCP3425>(
        std::make_shared<PCA9547Com>(0x6A, 1, m_i2c_root));

    // Carrier card
    m_sel_out = std::make_shared<MCP23017>(
        std::make_shared<PCA9547Com>(0x20, 2, m_i2c_root));
    m_sel_of = std::make_shared<MCP23017>(
        std::make_shared<PCA9547Com>(0x21, 2, m_i2c_root));

    //
    // DACs
    double maxcurr = 3.25 * 16.9 / (100 + 16.9) / 100e-3;
    m_dac_0 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9547Com>(0x4C, 5, m_i2c_root));
    m_dac_1 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9547Com>(0x4D, 5, m_i2c_root));
    m_dac_2 = std::make_shared<DAC5574>(
        maxcurr, std::make_shared<PCA9547Com>(0x4E, 5, m_i2c_root));

    //
    // NTC's
    m_ntc0 =
        std::make_shared<NTCSensor>(ADC_NTC_CH0, m_adc_lv0, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc1 =
        std::make_shared<NTCSensor>(ADC_NTC_CH1, m_adc_lv0, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc2 =
        std::make_shared<NTCSensor>(ADC_NTC_CH2, m_adc_lv1, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);
    m_ntc3 =
        std::make_shared<NTCSensor>(ADC_NTC_CH3, m_adc_lv1, false, 298.15, 10e3,
                                    (3601 + 3650) / 2, false, 10e3, 3.25);

    //
    // SHT35 on active board
    m_sht = std::make_shared<SHT85>(
        std::make_shared<PCA9547Com>(0x44, 4, m_i2c_root));

    //
    // CAL DAC
    setCalDAC(std::make_shared<DAC5571>(1.085, m_i2c_analogue));

    //
    // PBs
    m_pbsel = std::make_shared<TCA9534>(
        std::make_shared<PCA9547Com>(0x20, 3, m_i2c_root));
    m_pbsel->setIO(0x0);

    std::shared_ptr<UIOCom> pbuio = std::make_shared<UIOCom>(m_pbdev, 0x10000);

    // Mapping for the CMDmux patch panel
    bool invertCMDin = m_zturn_adapter;
    bool invertCMDout = !m_zturn_adapter;

    if (m_invertCMDin > -1) invertCMDin = m_invertCMDin;
    if (m_invertCMDout > -1) invertCMDout = m_invertCMDout;

    m_pbs[0] = std::make_shared<AMACv2>(
        0, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x0, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[1] = std::make_shared<AMACv2>(
        1, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x8, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[2] = std::make_shared<AMACv2>(
        2, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x1, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[3] = std::make_shared<AMACv2>(
        3, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x9, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[4] = std::make_shared<AMACv2>(
        4, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x2, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[5] = std::make_shared<AMACv2>(
        5, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x3, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[6] = std::make_shared<AMACv2>(
        6, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x4, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[7] = std::make_shared<AMACv2>(
        7, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x5, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[8] = std::make_shared<AMACv2>(
        8, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x6, invertCMDin, invertCMDout, m_pbsel, pbuio)));
    m_pbs[9] = std::make_shared<AMACv2>(
        9, std::unique_ptr<EndeavourRaw>(new EndeavourRawUIOMux(
               0x7, invertCMDin, invertCMDout, m_pbsel, pbuio)));
}

void PBv3TBMassive20220329::powerTBOn() {
    uint32_t curr_val = m_pbsel->read();
    m_pbsel->write((curr_val & 0x0F) | 0x80);

    PBv3TBMassive::powerTBOn();
}

void PBv3TBMassive20220329::powerTBOff() {
    PBv3TBMassive::powerTBOff();

    uint32_t curr_val = m_pbsel->read();
    m_pbsel->write((curr_val & 0x0F) | 0x00);
}

bool PBv3TBMassive20220329::checkCarrierCard() {
    try {
        std::shared_ptr<I2CCom> testdevice =
            std::make_shared<PCA9547Com>(0x20, 2, m_i2c_root);
        testdevice->read_reg8(0x00);
    } catch (const ComIOException &e) {
        return false;
    }
    return true;
}
