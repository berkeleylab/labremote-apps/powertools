#ifndef PBV3TBMASSIVE20210504_EC_H
#define PBV3TBMASSIVE20210504_EC_H

#include "PBv3TBMassive.h"

class PBv3TBMassive20210504_EC : public PBv3TBMassive {
 public:
    PBv3TBMassive20210504_EC(std::shared_ptr<PowerSupplyChannel> lv,
                             std::shared_ptr<PowerSupplyChannel> hv);
    PBv3TBMassive20210504_EC(std::shared_ptr<EquipConf> hw);
    PBv3TBMassive20210504_EC() = default;
    ~PBv3TBMassive20210504_EC() = default;

    //! Configure on JSON object
    /**
     * Valid keys:
     *  - `zturn_adapter`: True when using the ZTurn adapter
     *  - `invertCMDout`: overwrite invertCMDout, set to true for worker 3 when
     * using the Zturn adapter
     *  - `invertCMDin`: overwrite invertCMDin, normally not needed
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json &config);

    /**
     * In addtion to PBV3TBMassive powerOn:
     *  - enable carrier card power
     */
    virtual void powerTBOn();

    /**
     * In addtion to PBV3TBMassive powerOff:
     *  - disable carrier card power
     */
    virtual void powerTBOff();

 protected:
    //! Initialize devices on the active board
    virtual void initDevices();

 private:
    //! ZTurn adapter is in use
    bool m_zturn_adapter = false;
    //! overwrite invertCMDout
    int m_invertCMDout = -1;
    //! overwrite invertCMDin
    int m_invertCMDin = -1;
};

#endif  // PBV3TBMASSIVE20210504_EC_H
