#include "AMACv2.h"

#include <math.h>
#include <unistd.h>

#include <chrono>
#include <thread>

#include "EndeavourComException.h"
#include "Logger.h"
#include "NotSupportedException.h"

const std::map<AMACv2::AM, std::string> AMACv2::Map_AMStr = {
    {AM::VDCDC, "VDCDC"},         {AM::VDDLR, "VDDLR"},
    {AM::VDDLRLo, "VDDLRLo"},     {AM::VDDLRHi, "VDDLRHi"},
    {AM::DCDCIN, "DCDCIN"},       {AM::VDDREG, "VDDREG"},
    {AM::VDDBG, "VDDBG"},         {AM::AM900BG, "AM900BG"},
    {AM::AM600BG, "AM600BG"},     {AM::CAL, "CAL"},
    {AM::AMREF, "AMREF"},         {AM::CALX, "CALX"},
    {AM::CALY, "CALY"},           {AM::SHUNTX, "SHUNTX"},
    {AM::SHUNTY, "SHUNTY"},       {AM::DCDCADJ, "DCDCADJ"},
    {AM::CTAT, "CTAT"},           {AM::NTCX, "NTCX"},
    {AM::NTCY, "NTCY"},           {AM::NTCPB, "NTCPB"},
    {AM::HREFX, "HREFX"},         {AM::HREFY, "HREFY"},
    {AM::CUR10V, "CUR10V"},       {AM::CUR10VTPL, "CUR10VTPL"},
    {AM::CUR10VTPH, "CUR10VTPH"}, {AM::CUR1V, "CUR1V"},
    {AM::CUR1VTPL, "CUR1VTPL"},   {AM::CUR1VTPH, "CUR1VTPH"},
    {AM::HVRET, "HVRET"},         {AM::PTAT, "PTAT"},
};

AMACv2::AMACv2(unsigned short amacid, std::unique_ptr<EndeavourRaw> raw)
    : EndeavourCom(amacid, std::move(raw)), AMACv2RegMap() {}

AMACv2::Version AMACv2::getVersion() { return amac_version; }

void AMACv2::init() {
    // Set AMAC ID
    if (!isCommIDSet()) {
        EndeavourCom::setid(EndeavourCom::REFMODE::IDPads, m_padid);
        usleep(10);
    }

    // Get the AMAC version
    uint32_t reg_status =
        EndeavourCom::read_reg(AMACv2RegMap::RegisterStatus.getAddress());

    uint32_t version_id = ((reg_status & 0b11000000) >> 6);

    switch (version_id) {
        case 0:
            amac_version = Version::v2;
            logger(logDEBUG) << "AMAC version: v2";
            ch_VDCDC = 0;
            ch_VDDLR = 1;
            ch_DCDCIN = 2;
            ch_VDDREG = 3;
            ch_VDDBG = 3;
            ch_AM900BG = 3;
            ch_AM600BG = 4;
            ch_CAL = 4;
            ch_AMREF = 4;
            ch_CALX = 5;
            ch_CALY = 5;
            ch_SHUNTX = 5;
            ch_SHUNTY = 5;
            ch_DCDCADJ = 5;
            ch_CTAT = 6;
            ch_NTCX = 7;
            ch_NTCY = 8;
            ch_NTCPB = 9;
            ch_HREFX = 10;
            ch_HREFY = 11;
            ch_CUR10V = 12;
            ch_CUR10VTPL = 12;
            ch_CUR10VTPH = 12;
            ch_CUR1V = 13;
            ch_CUR1VTPL = 13;
            ch_CUR1VTPH = 13;
            ch_HVRET = 14;
            ch_PTAT = 15;
            break;
        case 1:
            amac_version = Version::star;
            logger(logDEBUG) << "AMAC version: star";
            ch_VDCDC = 0;
            ch_VDDLR = 12;
            ch_VDDLRLo = 12;
            ch_VDDLRHi = 12;
            ch_DCDCIN = 1;
            ch_VDDREG = 13;
            ch_VDDBG = 15;
            ch_AM900BG = 12;
            ch_AM600BG = 12;
            ch_CAL = 11;
            ch_AMREF = 13;
            ch_CALX = 15;
            ch_CALY = 15;
            ch_SHUNTX = 15;
            ch_SHUNTY = 15;
            ch_DCDCADJ = 13;
            ch_CTAT = 5;
            ch_NTCX = 2;
            ch_NTCY = 3;
            ch_NTCPB = 4;
            ch_HREFX = 9;
            ch_HREFY = 10;
            ch_CUR10V = 13;
            ch_CUR10VTPL = 14;
            ch_CUR10VTPH = 14;
            ch_CUR1V = 6;
            ch_CUR1VTPL = 14;
            ch_CUR1VTPH = 14;
            ch_HVRET = 7;
            ch_PTAT = 8;
            break;
        default:
            amac_version = Version::unknown;
            throw NotSupportedException("unknown AMAC version");
    }

    // Initiallize AMACv2RegMap
    init_regmap(amac_version);

    // Load current register values
    loadRegisters();
}

void AMACv2::initRegisters() {
    // Initialize registers with default values
    for (AMACv2Field AMACv2RegMap::*field : m_fields) {
        if ((this->*field).canBeWrittenField())
            (this->*field).writeDefaultVal();
    }

    // Write AMAC registers into the chip
    for (const AMACv2Register *reg : getRegisters()) {
        if (reg->isRW() != RW) continue;

        EndeavourCom::write_reg(reg->getAddress(), getReg(reg->getAddress()));
    }
}

void AMACv2::loadRegisters() {
    // Load register map with current values
    for (const AMACv2Register *reg : getRegisters()) {
        if (reg->isRW() != RW && reg->isRW() != RO) continue;

        uint32_t value = EndeavourCom::read_reg(reg->getAddress());
        setReg(reg->getAddress(), value);
    }
}

bool AMACv2::isCommIDSet() {
    try {
        read_reg(0);
    } catch (const EndeavourComException &e) {
        return false;
    }
    return true;
}

void AMACv2::syncReg(AMACv2Field AMACv2RegMap::*ref) {
    AMACv2Register *reg = (this->*ref).getRegister();
    EndeavourCom::write_reg(reg->getAddress(), reg->getValue());
}

void AMACv2::wrField(AMACv2Field AMACv2RegMap::*ref, uint32_t data) {
    if (!(this->*ref).isValid()) return;
    setField(ref, data);
    AMACv2Register *reg = (this->*ref).getRegister();
    EndeavourCom::write_reg(reg->getAddress(), reg->getValue());
    usleep(1e4);
}

void AMACv2::wrField(const std::string &fieldName, uint32_t data) {
    wrField(m_fieldMap[fieldName], data);
}

uint8_t AMACv2::getFieldWidth(AMACv2Field AMACv2RegMap::*ref) {
    if (!(this->*ref).isValid()) return 0;
    return (this->*ref).getWidth();
}

uint32_t AMACv2::rdField(AMACv2Field AMACv2RegMap::*ref) {
    if (!(this->*ref).isValid()) return 0;
    uint32_t ret = EndeavourCom::read_reg(getAddr(ref));
    AMACv2Register *reg = (this->*ref).getRegister();
    reg->setValue(ret);
    return getField(ref);
}

uint32_t AMACv2::rdField(const std::string &fieldName) {
    return rdField(m_fieldMap[fieldName]);
}

void AMACv2::write_reg(unsigned int address, unsigned int data) {
    setReg(address, data);
    EndeavourCom::write_reg(address, data);
    usleep(1e4);
}

unsigned int AMACv2::read_reg(unsigned int address) {
    uint32_t value = EndeavourCom::read_reg(address);
    setReg(address, value);
    return value;
}

void AMACv2::setPADID(uint8_t padid) { m_padid = padid; }

void AMACv2::setADCslope(double ADCslope) { m_ADCslope = ADCslope; }

void AMACv2::setADCoffset(uint8_t ch, uint32_t counts) {
    if (ch >= 16) return;
    m_ADCoffset[ch] = counts;
}

void AMACv2::enableNTCCalibration() {
    uint32_t bit_set = 0;
    if (amac_version == Version::star) bit_set = 1;
    wrField(&AMACv2RegMap::NTCpbCal, bit_set);
    wrField(&AMACv2RegMap::NTCx0Cal, bit_set);
    wrField(&AMACv2RegMap::NTCy0Cal, bit_set);
}

void AMACv2::disableNTCCalibration() {
    uint32_t bit_set = 1;
    if (amac_version == Version::star) bit_set = 0;
    wrField(&AMACv2RegMap::NTCpbCal, bit_set);
    wrField(&AMACv2RegMap::NTCx0Cal, bit_set);
    wrField(&AMACv2RegMap::NTCy0Cal, bit_set);
}

void AMACv2::setNTCxCal(double ntccal) { m_NTCxCal = ntccal; }

void AMACv2::setNTCyCal(double ntccal) { m_NTCyCal = ntccal; }

void AMACv2::setNTCpbCal(double ntccal) { m_NTCpbCal = ntccal; }

void AMACv2::setPTAT0(double ptat0) { m_PTAT0 = ptat0; }

void AMACv2::setCTAT0(uint8_t offset, double ctat0) {
    if (offset >= 16) return;
    m_CTAT0[offset] = ctat0;
}

void AMACv2::setCur10VOffset(double offset) { m_Cur10VOffset = offset; }

double AMACv2::getCur10VOffset() { return m_Cur10VOffset; }

void AMACv2::setCur1VOffset(double offset) { m_Cur1VOffset = offset; }

double AMACv2::getCur1VOffset() { return m_Cur1VOffset; }

double AMACv2::calibrateCounts(uint8_t ch, uint32_t counts) {
    if (ch >= 16) return 0.;

    return m_ADCslope * (counts - m_ADCoffset[ch]);
}

double AMACv2::convertNTC(double NTCvolt, double NTCcal, int SenseRange,
                          double B) {
    // convert AMAC measurement of NTC temperature from mV to C
    double NTCS0 = (SenseRange >> 0) & 1;
    double NTCS1 = (SenseRange >> 1) & 1;
    double NTCS2 = (SenseRange >> 2) & 1;

    double R = 200.0e3;
    double R0 = 133.0e3;
    double R1 = 50.0e3;
    double R2 = 22.22e3;

    double Rgain = 1 / (1 / R + NTCS0 / R0 + NTCS1 / R1 + NTCS2 / R2);

    // NTC resistance
    double Rntc = Rgain * NTCcal / (NTCvolt - NTCcal) - 280 - 250;

    // And finally calculate temperature
    double T0 = 273.15;
    double R25 = 10e3;
    double T25 = T0 + 25;

    double temp = 1. / (log(Rntc / R25) / B + 1 / T25) - T0;

    logger(logDEBUG2) << "conv_ntc: " << NTCvolt << ", " << NTCcal << ", "
                      << SenseRange << ", " << B << " => " << temp;

    return temp;
}

uint8_t AMACv2::getPADID() { return m_padid; }

uint32_t AMACv2::readAM(AM am, uint32_t reads) {
    switch (amac_version) {
        case Version::v2:
            return readAM_v2(am, reads);
        case Version::star:
            return readAM_star(am, reads);
        default:
            return 0;
    }
}

uint32_t AMACv2::readAM_v2(AM am, uint32_t reads) {
    AMACv2Field AMACv2RegMap::*ch;
    switch (am) {
        case AM::VDCDC:
            ch = &AMACv2::Ch0Value;
            break;

        case AM::VDDLR:
            ch = &AMACv2::Ch1Value;
            break;

        case AM::DCDCIN:
            ch = &AMACv2::Ch2Value;
            break;

        case AM::VDDREG:
            wrField(&AMACv2::Ch3Mux, 0);
            ch = &AMACv2::Ch3Value;
            break;

        case AM::VDDBG:
            wrField(&AMACv2::Ch3Mux, 1);
            ch = &AMACv2::Ch3Value;
            break;

        case AM::AM900BG:
            wrField(&AMACv2::Ch3Mux, 2);
            ch = &AMACv2::Ch3Value;
            break;

        case AM::AM600BG:
            wrField(&AMACv2::Ch4Mux, 0);
            ch = &AMACv2::Ch4Value;
            break;

        case AM::CAL:
            wrField(&AMACv2::Ch4Mux, 1);
            ch = &AMACv2::Ch4Value;
            break;

        case AM::AMREF:
            wrField(&AMACv2::Ch4Mux, 2);
            ch = &AMACv2::Ch4Value;
            break;

        case AM::CALX:
            wrField(&AMACv2::Ch5Mux, 0);
            ch = &AMACv2::Ch5Value;
            break;

        case AM::CALY:
            wrField(&AMACv2::Ch5Mux, 1);
            ch = &AMACv2::Ch5Value;
            break;

        case AM::SHUNTX:
            wrField(&AMACv2::Ch5Mux, 2);
            ch = &AMACv2::Ch5Value;
            break;

        case AM::SHUNTY:
            wrField(&AMACv2::Ch5Mux, 3);
            ch = &AMACv2::Ch5Value;
            break;

        case AM::DCDCADJ:
            wrField(&AMACv2::Ch5Mux, 4);
            ch = &AMACv2::Ch5Value;
            break;

        case AM::CTAT:
            ch = &AMACv2::Ch6Value;
            break;

        case AM::NTCX:
            ch = &AMACv2::Ch7Value;
            break;

        case AM::NTCY:
            ch = &AMACv2::Ch8Value;
            break;

        case AM::NTCPB:
            ch = &AMACv2::Ch9Value;
            break;

        case AM::HREFX:
            ch = &AMACv2::Ch10Value;
            break;

        case AM::HREFY:
            ch = &AMACv2::Ch11Value;
            break;

        case AM::CUR10V:
            wrField(&AMACv2::Ch12Mux, 0);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::CUR10VTPL:
            wrField(&AMACv2::Ch12Mux, 1);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::CUR10VTPH:
            wrField(&AMACv2::Ch12Mux, 2);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::CUR1V:
            wrField(&AMACv2::Ch13Mux, 0);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::CUR1VTPL:
            wrField(&AMACv2::Ch13Mux, 1);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::CUR1VTPH:
            wrField(&AMACv2::Ch13Mux, 2);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::HVRET:
            ch = &AMACv2::Ch14Value;
            break;

        case AM::PTAT:
            ch = &AMACv2::Ch15Value;
            break;

        default:
            return 0;
    }

    uint32_t val = 0;
    for (uint32_t i = 0; i < reads; i++) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
        val += rdField(ch);
    }

    return val / reads;
}

uint32_t AMACv2::readAM_star(AM am, uint32_t reads) {
    AMACv2Field AMACv2RegMap::*ch;

    switch (am) {
        case AM::VDCDC:
            ch = &AMACv2::Ch0Value;
            break;

        case AM::VDDLR:
            wrField(&AMACv2::Ch12Mux, 0);
            wrField(&AMACv2::Ch12AMux, 1);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::VDDLRLo:
            wrField(&AMACv2::Ch12Mux, 0);
            wrField(&AMACv2::Ch12AMux, 0);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::VDDLRHi:
            wrField(&AMACv2::Ch12Mux, 0);
            wrField(&AMACv2::Ch12AMux, 1);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::DCDCIN:
            ch = &AMACv2::Ch1Value;
            break;

        case AM::VDDREG:
            wrField(&AMACv2::Ch13Mux, 1);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::VDDBG:
            wrField(&AMACv2::Ch15Mux, 0);
            ch = &AMACv2::Ch15Value;
            break;

        case AM::AM900BG:
            wrField(&AMACv2::Ch12Mux, 1);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::AM600BG:
            wrField(&AMACv2::Ch12Mux, 2);
            ch = &AMACv2::Ch12Value;
            break;

        case AM::CAL:
            ch = &AMACv2::Ch11Value;
            break;

        case AM::AMREF:
            wrField(&AMACv2::Ch13Mux, 2);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::CALX:
            wrField(&AMACv2::Ch15Mux, 1);
            ch = &AMACv2::Ch15Value;
            break;

        case AM::CALY:
            wrField(&AMACv2::Ch15Mux, 2);
            ch = &AMACv2::Ch15Value;
            break;

        case AM::SHUNTX:
            wrField(&AMACv2::Ch15Mux, 3);
            ch = &AMACv2::Ch15Value;
            break;

        case AM::SHUNTY:
            wrField(&AMACv2::Ch15Mux, 4);
            ch = &AMACv2::Ch15Value;
            break;

        case AM::DCDCADJ:
            wrField(&AMACv2::Ch13Mux, 4);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::CTAT:
            ch = &AMACv2::Ch5Value;
            break;

        case AM::NTCX:
            ch = &AMACv2::Ch2Value;
            break;

        case AM::NTCY:
            ch = &AMACv2::Ch3Value;
            break;

        case AM::NTCPB:
            ch = &AMACv2::Ch4Value;
            break;

        case AM::HREFX:
            ch = &AMACv2::Ch9Value;
            break;

        case AM::HREFY:
            ch = &AMACv2::Ch10Value;
            break;

        case AM::CUR10V:
            wrField(&AMACv2::Ch13Mux, 0);
            ch = &AMACv2::Ch13Value;
            break;

        case AM::CUR10VTPL:
            wrField(&AMACv2::Ch14Mux, 1);
            ch = &AMACv2::Ch14Value;
            break;

        case AM::CUR10VTPH:
            wrField(&AMACv2::Ch14Mux, 2);
            ch = &AMACv2::Ch14Value;
            break;

        case AM::CUR1V:
            ch = &AMACv2::Ch6Value;
            break;

        case AM::CUR1VTPL:
            wrField(&AMACv2::Ch14Mux, 3);
            ch = &AMACv2::Ch14Value;
            break;

        case AM::CUR1VTPH:
            wrField(&AMACv2::Ch14Mux, 4);
            ch = &AMACv2::Ch14Value;
            break;

        case AM::HVRET:
            ch = &AMACv2::Ch7Value;
            break;

        case AM::PTAT:
            ch = &AMACv2::Ch8Value;
            break;

        default:
            return 0;
    }

    uint32_t val = 0;
    for (uint32_t i = 0; i < reads; i++) {
        std::this_thread::sleep_for(std::chrono::milliseconds(2));
        val += rdField(ch);
    }

    return val / reads;
}

double AMACv2::getLinPOLCur() {
    if (amac_version == Version::v2) return 0.0;
    uint32_t counts_hi = readAM(AM::VDDLRHi);
    uint32_t counts_lo = readAM(AM::VDDLRLo);

    static const double sf_VDDLR = (14 + 8.7) / 14;
    return sf_VDDLR * fabs(calibrateCounts(ch_VDDLRHi, counts_hi) -
                           calibrateCounts(ch_VDDLRLo, counts_lo));
}

double AMACv2::getVDDREG() {
    uint32_t counts = readAM(AM::VDDREG);
    return calibrateCounts(ch_VDDREG, counts) * 3 / 2;
}

double AMACv2::getAM600() {
    uint32_t counts = readAM(AM::AM600BG);
    return calibrateCounts(ch_AM600BG, counts);
}

double AMACv2::getAM900() {
    uint32_t counts = readAM(AM::AM900BG);
    return calibrateCounts(ch_AM900BG, counts);
}

double AMACv2::getNTCx() {
    uint32_t counts = readAM(AM::NTCX);
    return calibrateCounts(ch_NTCX, counts);
}

double AMACv2::getNTCy() {
    uint32_t counts = readAM(AM::NTCY);
    return calibrateCounts(ch_NTCY, counts);
}

double AMACv2::getNTCpb() {
    uint32_t counts = readAM(AM::NTCPB);
    return calibrateCounts(ch_NTCPB, counts);
}

double AMACv2::getPTAT() {
    uint32_t counts = readAM(AM::PTAT);
    return calibrateCounts(ch_PTAT, counts);
}

double AMACv2::getCTAT() {
    uint32_t counts = readAM(AM::CTAT);
    return calibrateCounts(ch_CTAT, counts);
}

double AMACv2::getCUR10V(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR10V, reads);
    return calibrateCounts(ch_CUR10V, counts);
}

double AMACv2::getCUR10VTPL(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR10VTPL, reads);
    return calibrateCounts(ch_CUR10VTPL, counts);
}

double AMACv2::getCUR10VTPH(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR10VTPH, reads);
    return calibrateCounts(ch_CUR10VTPH, counts);
}

double AMACv2::getCUR1V(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR1V, reads);
    return calibrateCounts(ch_CUR1V, counts);
}

double AMACv2::getCUR1VTPL(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR1VTPL);
    return calibrateCounts(ch_CUR1VTPL, counts);
}

double AMACv2::getCUR1VTPH(uint32_t reads) {
    uint32_t counts = readAM(AM::CUR1VTPH);
    return calibrateCounts(ch_CUR1VTPH, counts);
}

double AMACv2::temperatureX() {
    double NTCv = getNTCx();
    uint32_t SR = rdField(&AMACv2RegMap::NTCx0SenseRange);
    return convertNTC(NTCv, m_NTCxCal, SR, 3350);
}

double AMACv2::temperatureY() {
    double NTCv = getNTCy();
    uint32_t SR = rdField(&AMACv2RegMap::NTCy0SenseRange);
    return convertNTC(NTCv, m_NTCyCal, SR, 3350);
}

double AMACv2::temperaturePB() {
    double NTCv = getNTCpb();
    uint32_t SR = rdField(&AMACv2RegMap::NTCpbSenseRange);
    return convertNTC(NTCv, m_NTCpbCal, SR, 3900);
}

double AMACv2::temperaturePTAT() { return (getPTAT() - m_PTAT0) / 4.85; }

double AMACv2::temperatureCTAT() {
    uint32_t CTAToffset = rdField(&AMACv2RegMap::CTAToffset);
    return (getCTAT() - m_CTAT0[CTAToffset]) / (-1.5);
}

double AMACv2::convertCur10V(uint32_t Cur10V) {
    return (calibrateCounts(ch_CUR10V, Cur10V) - m_Cur10VOffset) / 10.4 / 33e-3;
}

double AMACv2::convertCur1V(uint32_t Cur1V) {
    return (calibrateCounts(ch_CUR1V, Cur1V) - m_Cur1VOffset) / 30 / 7.6e-3;
}

double AMACv2::convertVDCDC(uint32_t VDCDC) {
    return 2 * calibrateCounts(0, VDCDC) / 1e3;
}

uint32_t AMACv2::readEFuse() {
    wrField(&AMACv2RegMap::FlagResetF, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    return rdField(&AMACv2RegMap::SerNum);
}
