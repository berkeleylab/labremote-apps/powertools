#ifndef AMACREGMAP_H
#define AMACREGMAP_H

#include <stdint.h>

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "AMACv2Field.h"

//! AMACv2 register map
/**
 * Contains definition of all fields and registers inside AMAC
 */
class AMACv2RegMap {
 public:
    //! Possible AMAC versions
    enum class Version { v2, star, unknown };

 private:
    //! Initialize register map for AMACv2
    void init_v2();

    //! Initialize register map for AMACstar
    void init_star();

 protected:
    //! Initialize helper containers for looking up fields and registers
    void init_regmap(Version version);

 public:
    AMACv2RegMap();

    std::vector<const AMACv2Register *> getRegisters() const;
    std::vector<const AMACv2Field *> getFields() const;

    AMACv2Field *findField(AMACv2Field AMACv2RegMap::*ref);
    AMACv2Field *findField(const std::string &fieldName);

    uint32_t getField(AMACv2Field AMACv2RegMap::*ref);
    uint32_t getField(const std::string &fieldName);

    uint32_t getReg(uint32_t reg);
    uint32_t getReg(AMACv2Register AMACv2RegMap::*ref);

    void setField(AMACv2Field AMACv2RegMap::*ref, uint32_t value);
    void setField(const std::string &fieldName, uint32_t value);

    void setReg(uint32_t reg, uint32_t value);
    void setReg(AMACv2Register AMACv2RegMap::*ref, uint32_t value);

    uint8_t getAddr(AMACv2Field AMACv2RegMap::*ref);
    uint8_t getAddr(const std::string &fieldName);

    bool canBeWritten(AMACv2Field AMACv2RegMap::*ref);

    /** \name Registers
     * @{ */
    AMACv2Register RegisterStatus;
    AMACv2Register RegisterHxFlags;
    AMACv2Register RegisterHyFlags;
    AMACv2Register RegisterHyHxFlags;
    AMACv2Register RegisterHV0Flags;
    AMACv2Register RegisterHV2Flags;
    AMACv2Register RegisterHV2HV0Flags;
    AMACv2Register RegisterDCDCFlags;
    AMACv2Register RegisterWRNHxFlags;
    AMACv2Register RegisterWRNDCDCFlags;
    AMACv2Register RegisterSynFlags;

    AMACv2Register RegisterValue0;
    AMACv2Register RegisterValue1;
    AMACv2Register RegisterValue2;
    AMACv2Register RegisterValue3;
    AMACv2Register RegisterValue4;
    AMACv2Register RegisterValue5;

    AMACv2Register RegisterSEUstatus0;
    AMACv2Register RegisterSEUstatus1;

    AMACv2Register RegisterSerNum;

    AMACv2Register RegisterFlagResets;
    AMACv2Register RegisterLogicReset;
    AMACv2Register RegisterHardReset;

    AMACv2Register RegisterCntSet;
    AMACv2Register RegisterCntSetC;
    AMACv2Register RegisterDCDCen;
    AMACv2Register RegisterDCDCenC;
    AMACv2Register RegisterIlock;
    AMACv2Register RegisterIlockC;
    AMACv2Register RegisterRstCnt;
    AMACv2Register RegisterRstCntC;
    AMACv2Register RegisterAMen;
    AMACv2Register RegisterAMenC;
    AMACv2Register RegisterAMpwr;
    AMACv2Register RegisterAMpwrC;
    AMACv2Register RegisterBgCnt;
    AMACv2Register RegisterAMCnt;
    AMACv2Register RegisterDACs0;
    AMACv2Register RegisterDACbias;
    AMACv2Register RegisterAMACCnt;
    AMACv2Register RegisterNTCRange;
    AMACv2Register RegisterLVCurCal;

    AMACv2Register RegisterHxICfg;
    AMACv2Register RegisterHyICfg;
    AMACv2Register RegisterHyHxICfg;
    AMACv2Register RegisterHV0ICfg;
    AMACv2Register RegisterHV2ICfg;
    AMACv2Register RegisterHV2HV0ICfg;
    AMACv2Register RegisterDCDCICfg;
    AMACv2Register RegisterWRNICfg;
    AMACv2Register RegisterWRNDCDCICfg;

    AMACv2Register RegisterHxTLUT;
    AMACv2Register RegisterHxModLUT1;
    AMACv2Register RegisterHxModLUT2;

    AMACv2Register RegisterHyTLUT;
    AMACv2Register RegisterHyModLUT1;
    AMACv2Register RegisterHyModLUT2;

    AMACv2Register RegisterHV0TLUT;
    AMACv2Register RegisterHV0ModLUT1;
    AMACv2Register RegisterHV0ModLUT2;

    AMACv2Register RegisterHV2TLUT;
    AMACv2Register RegisterHV2ModLUT1;
    AMACv2Register RegisterHV2ModLUT2;

    AMACv2Register RegisterDCDCTLUT;
    AMACv2Register RegisterDCDCModLUT1;
    AMACv2Register RegisterDCDCModLUT2;

    AMACv2Register RegisterWRNTLUT;
    AMACv2Register RegisterWRNModLUT1;
    AMACv2Register RegisterWRNModLUT2;

    AMACv2Register RegisterHxFlagEn;
    AMACv2Register RegisterHyFlagEn;
    AMACv2Register RegisterHyHxFlagEn;
    AMACv2Register RegisterHV0FlagEn;
    AMACv2Register RegisterHV2FlagEn;
    AMACv2Register RegisterHV2HV0FlagEn;
    AMACv2Register RegisterDCDCFlagEn;
    AMACv2Register RegisterSynFlagEn;
    AMACv2Register RegisterWRNDCDCFlagEn;

    AMACv2Register RegisterHxTh0;
    AMACv2Register RegisterHxTh1;
    AMACv2Register RegisterHxTh2;
    AMACv2Register RegisterHxTh3;

    AMACv2Register RegisterHyTh0;
    AMACv2Register RegisterHyTh1;
    AMACv2Register RegisterHyTh2;
    AMACv2Register RegisterHyTh3;

    AMACv2Register RegisterHV0Th0;
    AMACv2Register RegisterHV0Th1;
    AMACv2Register RegisterHV0Th2;
    AMACv2Register RegisterHV0Th3;

    AMACv2Register RegisterHV2Th0;
    AMACv2Register RegisterHV2Th1;
    AMACv2Register RegisterHV2Th2;
    AMACv2Register RegisterHV2Th3;

    AMACv2Register RegisterDCDCTh0;
    AMACv2Register RegisterDCDCTh1;
    AMACv2Register RegisterDCDCTh2;
    AMACv2Register RegisterDCDCTh3;

    AMACv2Register RegisterWRNTh0;
    AMACv2Register RegisterWRNTh1;
    AMACv2Register RegisterWRNTh2;
    AMACv2Register RegisterWRNTh3;

    AMACv2Register RegisterHxLoTh0;
    AMACv2Register RegisterHxLoTh1;
    AMACv2Register RegisterHxLoTh2;
    AMACv2Register RegisterHxLoTh3;
    AMACv2Register RegisterHxLoTh4;
    AMACv2Register RegisterHxLoTh5;

    AMACv2Register RegisterHxHiTh0;
    AMACv2Register RegisterHxHiTh1;
    AMACv2Register RegisterHxHiTh2;
    AMACv2Register RegisterHxHiTh3;
    AMACv2Register RegisterHxHiTh4;
    AMACv2Register RegisterHxHiTh5;

    AMACv2Register RegisterHyLoTh0;
    AMACv2Register RegisterHyLoTh1;
    AMACv2Register RegisterHyLoTh2;
    AMACv2Register RegisterHyLoTh3;
    AMACv2Register RegisterHyLoTh4;
    AMACv2Register RegisterHyLoTh5;

    AMACv2Register RegisterHyHiTh0;
    AMACv2Register RegisterHyHiTh1;
    AMACv2Register RegisterHyHiTh2;
    AMACv2Register RegisterHyHiTh3;
    AMACv2Register RegisterHyHiTh4;
    AMACv2Register RegisterHyHiTh5;

    AMACv2Register RegisterHV0LoTh0;
    AMACv2Register RegisterHV0LoTh1;
    AMACv2Register RegisterHV0LoTh2;
    AMACv2Register RegisterHV0LoTh3;
    AMACv2Register RegisterHV0LoTh4;
    AMACv2Register RegisterHV0LoTh5;

    AMACv2Register RegisterHV0HiTh0;
    AMACv2Register RegisterHV0HiTh1;
    AMACv2Register RegisterHV0HiTh2;
    AMACv2Register RegisterHV0HiTh3;
    AMACv2Register RegisterHV0HiTh4;
    AMACv2Register RegisterHV0HiTh5;

    AMACv2Register RegisterHV2LoTh0;
    AMACv2Register RegisterHV2LoTh1;
    AMACv2Register RegisterHV2LoTh2;
    AMACv2Register RegisterHV2LoTh3;
    AMACv2Register RegisterHV2LoTh4;
    AMACv2Register RegisterHV2LoTh5;

    AMACv2Register RegisterHV2HiTh0;
    AMACv2Register RegisterHV2HiTh1;
    AMACv2Register RegisterHV2HiTh2;
    AMACv2Register RegisterHV2HiTh3;
    AMACv2Register RegisterHV2HiTh4;
    AMACv2Register RegisterHV2HiTh5;

    AMACv2Register RegisterDCDCLoTh0;
    AMACv2Register RegisterDCDCLoTh1;
    AMACv2Register RegisterDCDCLoTh2;
    AMACv2Register RegisterDCDCLoTh3;
    AMACv2Register RegisterDCDCLoTh4;
    AMACv2Register RegisterDCDCLoTh5;

    AMACv2Register RegisterDCDCHiTh0;
    AMACv2Register RegisterDCDCHiTh1;
    AMACv2Register RegisterDCDCHiTh2;
    AMACv2Register RegisterDCDCHiTh3;
    AMACv2Register RegisterDCDCHiTh4;
    AMACv2Register RegisterDCDCHiTh5;

    AMACv2Register RegisterWRNLoTh0;
    AMACv2Register RegisterWRNLoTh1;
    AMACv2Register RegisterWRNLoTh2;
    AMACv2Register RegisterWRNLoTh3;
    AMACv2Register RegisterWRNLoTh4;
    AMACv2Register RegisterWRNLoTh5;

    AMACv2Register RegisterWRNHiTh0;
    AMACv2Register RegisterWRNHiTh1;
    AMACv2Register RegisterWRNHiTh2;
    AMACv2Register RegisterWRNHiTh3;
    AMACv2Register RegisterWRNHiTh4;
    AMACv2Register RegisterWRNHiTh5;

    /** @} */

    /** \name Fields
     * @{ */
    // Status registers
    AMACv2Field StatusAM;
    AMACv2Field StatusWARN;
    AMACv2Field StatusDCDC;
    AMACv2Field StatusHV3;
    AMACv2Field StatusHV2;
    AMACv2Field StatusHV1;
    AMACv2Field StatusHV0;
    AMACv2Field StatusY2LDO;
    AMACv2Field StatusY1LDO;
    AMACv2Field StatusY0LDO;
    AMACv2Field StatusX2LDO;
    AMACv2Field StatusX1LDO;
    AMACv2Field StatusX0LDO;
    AMACv2Field StatusGPI;
    AMACv2Field StatusPGOOD;
    AMACv2Field StatusILockWARN;
    AMACv2Field StatusILockDCDC;
    AMACv2Field StatusILockHV2;
    AMACv2Field StatusILockHV0;
    AMACv2Field StatusILockYLDO;
    AMACv2Field StatusILockxLDO;
    AMACv2Field AMACVersion;
    // HxFlags
    AMACv2Field HxFlagsHi;
    AMACv2Field HxFlagsLo;
    AMACv2Field HxChFlags;
    // HyFlags
    AMACv2Field HyFlagsHi;
    AMACv2Field HyFlagsLo;
    AMACv2Field HyChFlags;
    // HV0Flags
    AMACv2Field HV0FlagsHi;
    AMACv2Field HV0FlagsLo;
    AMACv2Field HV0ChFlags;
    // HV2Flags
    AMACv2Field HV2FlagsHi;
    AMACv2Field HV2FlagsLo;
    AMACv2Field HV2ChFlags;
    // DCDCFlags
    AMACv2Field DCDCflagsHi;
    AMACv2Field DCDCflagsLo;
    AMACv2Field DCDCChFlags;
    // WRNHxFlags
    AMACv2Field WRNflagsHi;
    AMACv2Field WRNflagsLo;
    AMACv2Field WRNChFlags;
    // SynFlags
    AMACv2Field SynFlagsHx;
    AMACv2Field SynFlagsHy;
    AMACv2Field SynFlagsHV0;
    AMACv2Field SynFlagsHV2;
    AMACv2Field SynFlagsDCDC;
    AMACv2Field SynFlagsWRN;
    // Value0
    AMACv2Field Value0AMen;
    AMACv2Field Ch0Value;
    AMACv2Field Ch1Value;
    AMACv2Field Ch2Value;
    // Value1
    AMACv2Field Value1AMen;
    AMACv2Field Ch3Value;
    AMACv2Field Ch4Value;
    AMACv2Field Ch5Value;
    // Value2
    AMACv2Field Value2AMen;
    AMACv2Field Ch6Value;
    AMACv2Field Ch7Value;
    AMACv2Field Ch8Value;
    // Value3
    AMACv2Field Value3AMen;
    AMACv2Field Ch9Value;
    AMACv2Field Ch10Value;
    AMACv2Field Ch11Value;
    // Value4
    AMACv2Field Value4AMen;
    AMACv2Field Ch12Value;
    AMACv2Field Ch13Value;
    AMACv2Field Ch14Value;
    AMACv2Field Ch12S;
    AMACv2Field Ch13S;
    // Value5
    AMACv2Field Value5AMen;
    AMACv2Field Ch15Value;
    AMACv2Field Ch14S;
    AMACv2Field Ch15S;
    // SEUstatus
    AMACv2Field SEUStatusLo;
    AMACv2Field SEUStatusHi;
    // SerNum
    AMACv2Field PadID;
    AMACv2Field SerNum;
    // FlagResets
    AMACv2Field FlagResetOe;
    AMACv2Field FlagResetF;
    AMACv2Field FlagResetS;
    AMACv2Field FlagResetWRN;
    AMACv2Field FlagResetDCDC;
    AMACv2Field FlagResetHV2;
    AMACv2Field FlagResetHV0;
    AMACv2Field FlagResetXLDO;
    AMACv2Field FlagResetYLDO;
    // LogicReset
    AMACv2Field LogicReset;
    // HardReset
    AMACv2Field HardReset;
    // CntSet
    AMACv2Field CntSetHV3frq;
    AMACv2Field CntSetHV3en;
    AMACv2Field CntSetHV2frq;
    AMACv2Field CntSetHV2en;
    AMACv2Field CntSetHV1frq;
    AMACv2Field CntSetHV1en;
    AMACv2Field CntSetHV0frq;
    AMACv2Field CntSetHV0en;
    AMACv2Field CntSetHyLDO2en;
    AMACv2Field CntSetHyLDO1en;
    AMACv2Field CntSetHyLDO0en;
    AMACv2Field CntSetHxLDO2en;
    AMACv2Field CntSetHxLDO1en;
    AMACv2Field CntSetHxLDO0en;
    AMACv2Field CntSetWARN;
    // CntSetC
    AMACv2Field CntSetCHV3frq;
    AMACv2Field CntSetCHV3en;
    AMACv2Field CntSetCHV2frq;
    AMACv2Field CntSetCHV2en;
    AMACv2Field CntSetCHV1frq;
    AMACv2Field CntSetCHV1en;
    AMACv2Field CntSetCHV0frq;
    AMACv2Field CntSetCHV0en;
    AMACv2Field CntSetCHyLDO2en;
    AMACv2Field CntSetCHyLDO1en;
    AMACv2Field CntSetCHyLDO0en;
    AMACv2Field CntSetCHxLDO2en;
    AMACv2Field CntSetCHxLDO1en;
    AMACv2Field CntSetCHxLDO0en;
    AMACv2Field CntSetCWARN;
    // DCDCen
    AMACv2Field DCDCAdj;
    AMACv2Field DCDCen;
    // DCDCenC
    AMACv2Field DCDCAdjC;
    AMACv2Field DCDCenC;
    // Ilock
    AMACv2Field IlockHx;
    AMACv2Field IlockHy;
    AMACv2Field IlockHV0;
    AMACv2Field IlockHV2;
    AMACv2Field IlockDCDC;
    AMACv2Field IlockWRN;
    // IlockC
    AMACv2Field IlockCHx;
    AMACv2Field IlockCHy;
    AMACv2Field IlockCHV0;
    AMACv2Field IlockCHV2;
    AMACv2Field IlockCDCDC;
    AMACv2Field IlockCWRN;
    // RstCnt
    AMACv2Field RstCntHyHCCresetB;
    AMACv2Field RstCntHxHCCresetB;
    AMACv2Field RstCntOF;
    // RstCntC
    AMACv2Field RstCntCHyHCCresetB;
    AMACv2Field RstCntCHxHCCresetB;
    AMACv2Field RstCntCOF;
    // AMen
    AMACv2Field AMzeroCalib;
    AMACv2Field AMen;
    // AMenC
    AMACv2Field AMzeroCalibC;
    AMACv2Field AMenC;
    // AMpwr
    AMACv2Field ClockDisableEn;
    AMACv2Field ReqDCDCPGOOD;
    AMACv2Field DCDCenToPwrAMAC;
    // AMpwrC
    AMACv2Field ReqDCDCPGOODC;
    AMACv2Field DCDCenToPwrAMACC;
    // BgCnt
    AMACv2Field AMbgen;
    AMACv2Field AMbgSW;
    AMACv2Field AMbg;
    AMACv2Field VDDbgen;
    AMACv2Field VDDbgSW;
    AMACv2Field VDDbg;
    // AMCnt
    AMACv2Field AMCntRg;
    AMACv2Field AMintCalib;
    AMACv2Field Ch15Mux;
    AMACv2Field Ch14Mux;
    AMACv2Field Ch13Mux;
    AMACv2Field Ch12Mux;
    AMACv2Field Ch12AMux;
    AMACv2Field Ch5Mux;
    AMACv2Field Ch4Mux;
    AMACv2Field Ch3Mux;
    // DACs0
    AMACv2Field DACShunty;
    AMACv2Field DACShuntx;
    AMACv2Field DACCaly;
    AMACv2Field DACCalx;
    // DACbias
    AMACv2Field DACbias;
    AMACv2Field ShuntEn;
    // AMACCnt
    AMACv2Field eFusebitSelect;
    AMACv2Field HVcurGain;
    AMACv2Field DRcomMode;
    AMACv2Field DRcurr;
    AMACv2Field ClkDis;
    AMACv2Field RingOscFrq;
    // NTCRange
    AMACv2Field CTAToffset;
    AMACv2Field NTCpbCal;
    AMACv2Field NTCpbSenseRange;
    AMACv2Field NTCy0Cal;
    AMACv2Field NTCy0SenseRange;
    AMACv2Field NTCx0Cal;
    AMACv2Field NTCx0SenseRange;
    // LVCurCal
    AMACv2Field DCDCoOffset;
    AMACv2Field DCDCoZeroReading;
    AMACv2Field DCDCoN;
    AMACv2Field DCDCoP;
    AMACv2Field DCDCiZeroReading;
    AMACv2Field DCDCiRangeSW;
    AMACv2Field DCDCiOffset;
    AMACv2Field DCDCiP;
    AMACv2Field DCDCiN;
    // HxICfg
    AMACv2Field HxFlagValid;
    AMACv2Field HxFlagValidEn;
    AMACv2Field HxFlagsLogic;
    AMACv2Field HxFlagsLatch;
    AMACv2Field HxLAM;
    // HyICfg
    AMACv2Field HyFlagValid;
    AMACv2Field HyFlagValidEn;
    AMACv2Field HyFlagsLogic;
    AMACv2Field HyFlagsLatch;
    AMACv2Field HyLAM;
    // HV0ICfg
    AMACv2Field HV0FlagValid;
    AMACv2Field HV0FlagValidEn;
    AMACv2Field HV0FlagsLogic;
    AMACv2Field HV0FlagsLatch;
    AMACv2Field HV0LAM;
    // HV2ICfg
    AMACv2Field HV2FlagValid;
    AMACv2Field HV2FlagValidEn;
    AMACv2Field HV2FlagsLogic;
    AMACv2Field HV2FlagsLatch;
    AMACv2Field HV2LAM;
    // DCDCICfg
    AMACv2Field DCDCFlagValid;
    AMACv2Field DCDCFlagValidEn;
    AMACv2Field DCDCFlagsLogic;
    AMACv2Field DCDCFlagsLatch;
    AMACv2Field DCDCLAM;
    // WRNICfg
    AMACv2Field WRNFlagValid;
    AMACv2Field WRNFlagValidEn;
    AMACv2Field WRNFlagsLogic;
    AMACv2Field WRNFlagsLatch;
    AMACv2Field WRNLAM;
    // HxTLUT
    AMACv2Field HxTlut;
    // HxModLUT1
    AMACv2Field HxModlut1;
    // HxModLUT2
    AMACv2Field HxModlut2;
    // HyTLUT
    AMACv2Field HyTlut;
    // HyModLUT1
    AMACv2Field HyModlut1;
    // HyModLUT2
    AMACv2Field HyModlut2;
    // HV0TLUT
    AMACv2Field HV0Tlut;
    // HV0ModLUT1
    AMACv2Field HV0Modlut1;
    // HV0ModLUT2
    AMACv2Field HV0Modlut2;
    // HV2TLUT
    AMACv2Field HV2Tlut;
    // HV2ModLUT1
    AMACv2Field HV2Modlut1;
    // HV2ModLUT2
    AMACv2Field HV2Modlut2;
    // DCDCTLUT
    AMACv2Field DCDCTlut;
    // DCDCModLUT1
    AMACv2Field DCDCModlut1;
    // DCDCModLUT2
    AMACv2Field DCDCModlut2;
    // WRNTLUT
    AMACv2Field WRNTlut;
    // WRNModLUT1
    AMACv2Field WRNModlut1;
    // WRNModLUT2
    AMACv2Field WRNModlut2;
    // HxFlagEn
    AMACv2Field HxFlagsEnHi;
    AMACv2Field HxFlagsEnLo;
    AMACv2Field HxILockFlagEn;
    // HyFlagEn
    AMACv2Field HyFlagsEnHi;
    AMACv2Field HyFlagsEnLo;
    AMACv2Field HyILockFlagEn;
    // HV0FlagEn
    AMACv2Field HV0FlagsEnHi;
    AMACv2Field HV0FlagsEnLo;
    AMACv2Field HV0ILockFlagEn;
    // HV2FlagEn
    AMACv2Field HV2FlagsEnHi;
    AMACv2Field HV2FlagsEnLo;
    AMACv2Field HV2ILockFlagEn;
    // DCDCFlagEn
    AMACv2Field DCDCFlagsEnHi;
    AMACv2Field DCDCFlagsEnLo;
    AMACv2Field DCDCILockFlagEn;
    // SynFlagEn
    AMACv2Field HyFlagEnSynY;
    AMACv2Field HxFlagEnSynX;
    AMACv2Field HV2FlagEnSynH2;
    AMACv2Field HV0FlagEnSynH0;
    AMACv2Field WRNDCDCFlagEnSynW;
    AMACv2Field WRNDCDCFlagEnSynDc;
    AMACv2Field WRNFlagsEnHi;
    AMACv2Field WRNFlagsEnLo;
    AMACv2Field WARNILockFlagEn;
    AMACv2Field WRNsynFlagEnHi;
    AMACv2Field WRNsynFlagEnLo;
    AMACv2Field DCDCsynFlagEnHi;
    AMACv2Field DCDCsynFlagEnLo;
    AMACv2Field HV2synFlagEnHi;
    AMACv2Field HV2synFlagEnLo;
    AMACv2Field HV0synFlagEnHi;
    AMACv2Field HV0synFlagEnLo;
    AMACv2Field HysynFlagEnHi;
    AMACv2Field HysynFlagEnLo;
    AMACv2Field HxsynFlagEnHi;
    AMACv2Field HxsynFlagEnLo;
    // HxLoTh0
    AMACv2Field HxLoThCh0;
    AMACv2Field HxLoThCh1;
    AMACv2Field HxLoThCh2;
    // HxLoTh1
    AMACv2Field HxLoThCh3;
    AMACv2Field HxLoThCh4;
    AMACv2Field HxLoThCh5;
    // HxLoTh2
    AMACv2Field HxLoThCh6;
    AMACv2Field HxLoThCh7;
    AMACv2Field HxLoThCh8;
    // HxLoTh3
    AMACv2Field HxLoThCh9;
    AMACv2Field HxLoThCh10;
    AMACv2Field HxLoThCh11;
    // HxLoTh4
    AMACv2Field HxLoThCh12;
    AMACv2Field HxLoThCh13;
    AMACv2Field HxLoThCh14;
    // HxLoTh5
    AMACv2Field HxLoThCh15;
    // HxHiTh0
    AMACv2Field HxHiThCh0;
    AMACv2Field HxHiThCh1;
    AMACv2Field HxHiThCh2;
    // HxHiTh1
    AMACv2Field HxHiThCh3;
    AMACv2Field HxHiThCh4;
    AMACv2Field HxHiThCh5;
    // HxHiTh2
    AMACv2Field HxHiThCh6;
    AMACv2Field HxHiThCh7;
    AMACv2Field HxHiThCh8;
    // HxHiTh3
    AMACv2Field HxHiThCh9;
    AMACv2Field HxHiThCh10;
    AMACv2Field HxHiThCh11;
    // HxHiTh4
    AMACv2Field HxHiThCh12;
    AMACv2Field HxHiThCh13;
    AMACv2Field HxHiThCh14;
    // HxHiTh5
    AMACv2Field HxHiThCh15;
    // HyLoTh0
    AMACv2Field HyLoThCh0;
    AMACv2Field HyLoThCh1;
    AMACv2Field HyLoThCh2;
    // HyLoTh1
    AMACv2Field HyLoThCh3;
    AMACv2Field HyLoThCh4;
    AMACv2Field HyLoThCh5;
    // HyLoTh2
    AMACv2Field HyLoThCh6;
    AMACv2Field HyLoThCh7;
    AMACv2Field HyLoThCh8;
    // HyLoTh3
    AMACv2Field HyLoThCh9;
    AMACv2Field HyLoThCh10;
    AMACv2Field HyLoThCh11;
    // HyLoTh4
    AMACv2Field HyLoThCh12;
    AMACv2Field HyLoThCh13;
    AMACv2Field HyLoThCh14;
    // HyLoTh5
    AMACv2Field HyLoThCh15;
    // HyHiTh0
    AMACv2Field HyHiThCh0;
    AMACv2Field HyHiThCh1;
    AMACv2Field HyHiThCh2;
    // HyHiTh1
    AMACv2Field HyHiThCh3;
    AMACv2Field HyHiThCh4;
    AMACv2Field HyHiThCh5;
    // HyHiTh2
    AMACv2Field HyHiThCh6;
    AMACv2Field HyHiThCh7;
    AMACv2Field HyHiThCh8;
    // HyHiTh3
    AMACv2Field HyHiThCh9;
    AMACv2Field HyHiThCh10;
    AMACv2Field HyHiThCh11;
    // HyHiTh4
    AMACv2Field HyHiThCh12;
    AMACv2Field HyHiThCh13;
    AMACv2Field HyHiThCh14;
    // HyHiTh5
    AMACv2Field HyHiThCh15;
    // HV0LoTh0
    AMACv2Field HV0LoThCh0;
    AMACv2Field HV0LoThCh1;
    AMACv2Field HV0LoThCh2;
    // HV0LoTh1
    AMACv2Field HV0LoThCh3;
    AMACv2Field HV0LoThCh4;
    AMACv2Field HV0LoThCh5;
    // HV0LoTh2
    AMACv2Field HV0LoThCh6;
    AMACv2Field HV0LoThCh7;
    AMACv2Field HV0LoThCh8;
    // HV0LoTh3
    AMACv2Field HV0LoThCh9;
    AMACv2Field HV0LoThCh10;
    AMACv2Field HV0LoThCh11;
    // HV0LoTh4
    AMACv2Field HV0LoThCh12;
    AMACv2Field HV0LoThCh13;
    AMACv2Field HV0LoThCh14;
    // HV0LoTh5
    AMACv2Field HV0LoThCh15;
    // HV0HiTh0
    AMACv2Field HV0HiThCh0;
    AMACv2Field HV0HiThCh1;
    AMACv2Field HV0HiThCh2;
    // HV0HiTh1
    AMACv2Field HV0HiThCh3;
    AMACv2Field HV0HiThCh4;
    AMACv2Field HV0HiThCh5;
    // HV0HiTh2
    AMACv2Field HV0HiThCh6;
    AMACv2Field HV0HiThCh7;
    AMACv2Field HV0HiThCh8;
    // HV0HiTh3
    AMACv2Field HV0HiThCh9;
    AMACv2Field HV0HiThCh10;
    AMACv2Field HV0HiThCh11;
    // HV0HiTh4
    AMACv2Field HV0HiThCh12;
    AMACv2Field HV0HiThCh13;
    AMACv2Field HV0HiThCh14;
    // HV0HiTh5
    AMACv2Field HV0HiThCh15;
    // HV2HiTh0
    AMACv2Field HV2LoThCh0;
    AMACv2Field HV2LoThCh1;
    AMACv2Field HV2LoThCh2;
    // HV2LoTh1
    AMACv2Field HV2LoThCh3;
    AMACv2Field HV2LoThCh4;
    AMACv2Field HV2LoThCh5;
    // HV2LoTh2
    AMACv2Field HV2LoThCh6;
    AMACv2Field HV2LoThCh7;
    AMACv2Field HV2LoThCh8;
    // HV2LoTh3
    AMACv2Field HV2LoThCh9;
    AMACv2Field HV2LoThCh10;
    AMACv2Field HV2LoThCh11;
    // HV2LoTh4
    AMACv2Field HV2LoThCh12;
    AMACv2Field HV2LoThCh13;
    AMACv2Field HV2LoThCh14;
    // HV2LoTh5
    AMACv2Field HV2LoThCh15;
    // HV2HiTh0
    AMACv2Field HV2HiThCh0;
    AMACv2Field HV2HiThCh1;
    AMACv2Field HV2HiThCh2;
    // HV2HiTh1
    AMACv2Field HV2HiThCh3;
    AMACv2Field HV2HiThCh4;
    AMACv2Field HV2HiThCh5;
    // HV2HiTh2
    AMACv2Field HV2HiThCh6;
    AMACv2Field HV2HiThCh7;
    AMACv2Field HV2HiThCh8;
    // HV2HiTh3
    AMACv2Field HV2HiThCh9;
    AMACv2Field HV2HiThCh10;
    AMACv2Field HV2HiThCh11;
    // HV2HiTh4
    AMACv2Field HV2HiThCh12;
    AMACv2Field HV2HiThCh13;
    AMACv2Field HV2HiThCh14;
    // HV2HiTh5
    AMACv2Field HV2HiThCh15;
    // DCDCLoTh0
    AMACv2Field DCDCLoThCh0;
    AMACv2Field DCDCLoThCh1;
    AMACv2Field DCDCLoThCh2;
    // DCDCLoTh1
    AMACv2Field DCDCLoThCh3;
    AMACv2Field DCDCLoThCh4;
    AMACv2Field DCDCLoThCh5;
    // DCDCLoTh2
    AMACv2Field DCDCLoThCh6;
    AMACv2Field DCDCLoThCh7;
    AMACv2Field DCDCLoThCh8;
    // DCDCLoTh3
    AMACv2Field DCDCLoThCh9;
    AMACv2Field DCDCLoThCh10;
    AMACv2Field DCDCLoThCh11;
    // DCDCLoTh4
    AMACv2Field DCDCLoThCh12;
    AMACv2Field DCDCLoThCh13;
    AMACv2Field DCDCLoThCh14;
    // DCDCLoTh5
    AMACv2Field DCDCLoThCh15;
    // DCDCHiTh0
    AMACv2Field DCDCHiThCh0;
    AMACv2Field DCDCHiThCh1;
    AMACv2Field DCDCHiThCh2;
    // DCDCHiTh1
    AMACv2Field DCDCHiThCh3;
    AMACv2Field DCDCHiThCh4;
    AMACv2Field DCDCHiThCh5;
    // DCDCHiTh2
    AMACv2Field DCDCHiThCh6;
    AMACv2Field DCDCHiThCh7;
    AMACv2Field DCDCHiThCh8;
    // DCDCHiTh3
    AMACv2Field DCDCHiThCh9;
    AMACv2Field DCDCHiThCh10;
    AMACv2Field DCDCHiThCh11;
    // DCDCHiTh4
    AMACv2Field DCDCHiThCh12;
    AMACv2Field DCDCHiThCh13;
    AMACv2Field DCDCHiThCh14;
    // DCDCHiTh5
    AMACv2Field DCDCHiThCh15;
    // WRNLoTh0
    AMACv2Field WRNLoThCh0;
    AMACv2Field WRNLoThCh1;
    AMACv2Field WRNLoThCh2;
    // WRNLoTh1
    AMACv2Field WRNLoThCh3;
    AMACv2Field WRNLoThCh4;
    AMACv2Field WRNLoThCh5;
    // WRNLoTh2
    AMACv2Field WRNLoThCh6;
    AMACv2Field WRNLoThCh7;
    AMACv2Field WRNLoThCh8;
    // WRNLoTh3
    AMACv2Field WRNLoThCh9;
    AMACv2Field WRNLoThCh10;
    AMACv2Field WRNLoThCh11;
    // WRNLoTh4
    AMACv2Field WRNLoThCh12;
    AMACv2Field WRNLoThCh13;
    AMACv2Field WRNLoThCh14;
    // WRNLoTh5
    AMACv2Field WRNLoThCh15;
    // WRNHiTh0
    AMACv2Field WRNHiThCh0;
    AMACv2Field WRNHiThCh1;
    AMACv2Field WRNHiThCh2;
    // WRNHiTh1
    AMACv2Field WRNHiThCh3;
    AMACv2Field WRNHiThCh4;
    AMACv2Field WRNHiThCh5;
    // WRNHiTh2
    AMACv2Field WRNHiThCh6;
    AMACv2Field WRNHiThCh7;
    AMACv2Field WRNHiThCh8;
    // WRNHiTh3
    AMACv2Field WRNHiThCh9;
    AMACv2Field WRNHiThCh10;
    AMACv2Field WRNHiThCh11;
    // WRNHiTh4
    AMACv2Field WRNHiThCh12;
    AMACv2Field WRNHiThCh13;
    AMACv2Field WRNHiThCh14;
    // WRNHiTh5
    AMACv2Field WRNHiThCh15;

    /** @} */
 protected:
    //! List of all registers
    std::vector<AMACv2Register AMACv2RegMap::*> m_registers;
    //! List of all fields
    std::vector<AMACv2Field AMACv2RegMap::*> m_fields;

    //
    // Name to attribute maps
    std::unordered_map<uint8_t, AMACv2Register AMACv2RegMap::*> m_regAddrMap;
    std::unordered_map<std::string, AMACv2Register AMACv2RegMap::*> m_regMap;
    std::unordered_map<std::string, AMACv2Field AMACv2RegMap::*> m_fieldMap;
};
#endif  // AMACREGMAP_H
