#ifndef FTDIHELP_H
#define FTDIHELP_H

#include <cstdint>
#include <vector>

//! read all requested data
/**
 * Reads from the FTDI device until the numbers of
 * requested bytes has been reached or a timeout
 * occurs.
 *
 * A timeout occurs after 10 read attempts.
 *
 * \param ftdi FTDI context corresponding to the connection
 * \param data Resulting vector that will be filled with read data
 * \param requested Number of bytes to read.
 *
 * \return Number of bytes read. Equals to `requested` if no timeout occured.
 */
int32_t ftdi_read_alldata(struct ftdi_context *ftdi, std::vector<uint8_t> &data,
                          uint32_t requested);

#endif  // FTDIHELP_H
