#ifndef AMACV2_FIELD_H
#define AMACV2_FIELD_H

#include <memory>
#include <string>

#include "AMACv2Register.h"

//! \brief Definition and manipulation of an AMACv2 register field
/**
 * A register field is a set of bits inside a register that
 * control a specific setting.
 */
class AMACv2Field {
 private:
    //! The whole register
    AMACv2Register *m_register = nullptr;

    //! Default field value
    uint32_t m_defaultVal = 0;

    //! Number of bits
    uint8_t m_width = 0;

    //! Position of the LSB
    uint8_t m_offset = 0;

    //! Mask for updating register values
    uint32_t m_mask = 0;

    //! Name of field
    std::string m_fieldName = "";

 public:
    //! Empty (invalid) field definition
    AMACv2Field() = default;

    //! Define a bit field inside register `reg`.
    AMACv2Field(const std::string &fieldName, AMACv2Register *reg,
                uint8_t offset, uint8_t width, uint32_t defaultVal);

    //! Valid field definition
    bool isValid() const;

    bool canBeWrittenField() const;
    bool canBeReadField() const;
    bool isReadWrite() const;

    //! Get field name
    /**
     * \return name of the field
     */
    std::string getName() const;

    //! Get underlaying register
    AMACv2Register *getRegister() const;

    //! Get field width
    /**
     * \return width of field in bytes
     */
    uint8_t getWidth() const;

    void setDefaultVal(uint32_t defaultVal);
    void writeDefaultVal();

    void write(const uint32_t &cfgBits);
    uint32_t read() const;
};

#endif  // AMACV2_FIELD_H
