#include "ITSDAQComException.h"

ITSDAQComException::ITSDAQComException(const std::string &msg)
    : m_msg("ITSDAQCom: " + msg) {}

ITSDAQComException::ITSDAQComException(const char *format, ...) {
    char buffer[256];

    va_list args;
    va_start(args, format);
    vsnprintf(buffer, 256, format, args);
    va_end(args);

    m_msg = std::string("ITSDAQCom: ") + buffer;
}

const char *ITSDAQComException::what() const throw() { return m_msg.c_str(); }
