#include "PBv3ActiveTools.h"

#include <memory>
#include <thread>

#include "ComIOException.h"
#include "NotSupportedException.h"
#include "PBv3TBMassive20190718.h"
#include "PBv3TBMassive20210504.h"
#include "PBv3TBMassive20210504_EC.h"
#include "PBv3TBMassive20220329.h"
#include "PBv3Utils.h"
#include "PCA9547Com.h"
#include "PCA9548ACom.h"
#include "PowerSupplyChannel.h"

using namespace nlohmann;

namespace PBv3ActiveTools {
json testLvEnable(std::shared_ptr<PBv3TBMassive> tb) {
    logger(logINFO) << "## Test LV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "LV_ENABLE";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = false;

    tb->powerLVOn();

    // Wait to stabilize
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    // read interesting values
    double Iin = tb->getLVPS()->measureCurrent();
    double Vin = tb->getLVPS()->measureVoltage();

    testSum["passed"] = (Iin < 5e-3);

    if (testSum["passed"])
        logger(logINFO) << " --> Reading " << Iin << "A with " << Vin
                        << "V supplied";
    else
        logger(logERROR) << " --> Reading " << Iin << "A with " << Vin
                         << "V supplied";

    testSum["results"]["VIN"] = Vin;
    testSum["results"]["IIN"] = Iin;

    tb->powerLVOff();

    logger(logINFO) << "## End test LV_ENABLE ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json testI2C(std::shared_ptr<PBv3TBMassive> tb) {
    logger(logINFO) << "## Test I2CDEV ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    json testSum;
    testSum["testType"] = "I2CDEV";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;

    std::shared_ptr<I2CCom> i2c = tb->getI2CRoot();

    // common chips
    std::map<std::string, std::shared_ptr<I2CCom>> devices = {};

    if (std::dynamic_pointer_cast<PBv3TBMassive20190718>(tb) != nullptr ||
        std::dynamic_pointer_cast<PBv3TBMassive20210504>(tb) != nullptr ||
        std::dynamic_pointer_cast<PBv3TBMassive20210504_EC>(tb) != nullptr) {
        devices["0_0x14"] = std::make_shared<PCA9548ACom>(0x14, 0, i2c);
        devices["1_0x4C"] = std::make_shared<PCA9548ACom>(0x4C, 1, i2c);
        devices["5_0x4C"] = std::make_shared<PCA9548ACom>(0x4C, 5, i2c);
        devices["5_0x4D"] = std::make_shared<PCA9548ACom>(0x4D, 5, i2c);
        devices["5_0x4E"] = std::make_shared<PCA9548ACom>(0x4E, 5, i2c);
        devices["5_0x21"] = std::make_shared<PCA9548ACom>(0x21, 5, i2c);
        devices["5_0x22"] = std::make_shared<PCA9548ACom>(0x22, 5, i2c);
        devices["5_0x68"] = std::make_shared<PCA9548ACom>(0x68, 5, i2c);
        devices["5_0x6C"] = std::make_shared<PCA9548ACom>(0x6C, 5, i2c);
        devices["5_0x6A"] = std::make_shared<PCA9548ACom>(0x6A, 5, i2c);
    }

    if (std::dynamic_pointer_cast<PBv3TBMassive20210504>(tb) != nullptr ||
        std::dynamic_pointer_cast<PBv3TBMassive20210504_EC>(tb) != nullptr) {
        devices["1_0x6A"] = std::make_shared<PCA9548ACom>(0x6A, 1, i2c);
        devices["4_0x44"] = std::make_shared<PCA9548ACom>(0x44, 4, i2c);
        devices["7_0x23"] = std::make_shared<PCA9548ACom>(0x23, 7, i2c);
    }

    // determine revision to determine what chips to test
    if (std::dynamic_pointer_cast<PBv3TBMassive20190718>(tb) != nullptr) {
        devices["1_0x68"] = std::make_shared<PCA9548ACom>(0x68, 1, i2c);

        devices["3_0x20"] = std::make_shared<PCA9548ACom>(0x20, 3, i2c);

        devices["4_0x40"] = std::make_shared<PCA9548ACom>(0x40, 4, i2c);

        devices["7_0x21"] = std::make_shared<PCA9548ACom>(0x6A, 5, i2c);
    } else if (std::dynamic_pointer_cast<PBv3TBMassive20210504>(tb) !=
               nullptr) {
        devices["3_0x38"] = std::make_shared<PCA9548ACom>(0x38, 3, i2c);
        devices["3_0x3C"] = std::make_shared<PCA9548ACom>(0x3C, 3, i2c);
    } else if (std::dynamic_pointer_cast<PBv3TBMassive20210504_EC>(tb) !=
               nullptr) {
        // EC Active baord variant uses different chips
        devices["3_0x20"] = std::make_shared<PCA9548ACom>(0x20, 3, i2c);
        devices["3_0x24"] = std::make_shared<PCA9548ACom>(0x24, 3, i2c);
    } else if (std::dynamic_pointer_cast<PBv3TBMassive20220329>(tb) !=
               nullptr) {
        devices["0_0x14"] = std::make_shared<PCA9547Com>(0x14, 0, i2c);
        devices["1_0x4C"] = std::make_shared<PCA9547Com>(0x4C, 1, i2c);
        devices["5_0x4C"] = std::make_shared<PCA9547Com>(0x4C, 5, i2c);
        devices["5_0x4D"] = std::make_shared<PCA9547Com>(0x4D, 5, i2c);
        devices["5_0x4E"] = std::make_shared<PCA9547Com>(0x4E, 5, i2c);
        devices["5_0x48"] = std::make_shared<PCA9547Com>(0x48, 5, i2c);
        devices["5_0x49"] = std::make_shared<PCA9547Com>(0x49, 5, i2c);
        devices["5_0x68"] = std::make_shared<PCA9547Com>(0x68, 5, i2c);
        devices["5_0x6C"] = std::make_shared<PCA9547Com>(0x6C, 5, i2c);
        devices["5_0x6A"] = std::make_shared<PCA9547Com>(0x6A, 5, i2c);
        devices["1_0x6A"] = std::make_shared<PCA9547Com>(0x6A, 1, i2c);
        devices["3_0x20"] = std::make_shared<PCA9547Com>(0x20, 3, i2c);
        // devices["3_0x21"] = std::make_shared<PCA9547Com>(0x21, 3, i2c);
        devices["4_0x44"] = std::make_shared<PCA9547Com>(0x44, 4, i2c);
        devices["7_0x23"] = std::make_shared<PCA9547Com>(0x23, 7, i2c);
    } else {
        throw NotSupportedException("Unknown active board revision");
    }

    for (const std::pair<std::string, std::shared_ptr<I2CCom>> &device :
         devices) {
        try {
            device.second->write_block({});
            logger(logINFO) << device.first << " OK";
            testSum["results"][device.first] = true;
        } catch (const ComIOException &e) {
            logger(logERROR) << device.first << " ERROR";
            testSum["results"][device.first] = false;
            testSum["passed"] = false;
        }
    }
    if (testSum["passed"])
        logger(logINFO) << " I2CDEV passed! :) ";
    else
        logger(logERROR) << " I2CDEV failed! :(";

    logger(logINFO) << "## End test I2CDEV ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}

json testPSINIT(std::shared_ptr<PBv3TBMassive> tb, bool skiphv) {
    logger(logINFO) << "## Test PSINIT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());
    json testSum;
    testSum["testType"] = "PSINIT";
    testSum["results"]["TIMESTART"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
    testSum["passed"] = true;
    if (!skiphv) tb->powerHVOff();
    tb->powerLVOff();
    std::shared_ptr<PowerSupplyChannel> hv = skiphv ? nullptr : tb->getHVPS();
    std::shared_ptr<PowerSupplyChannel> lv = tb->getLVPS();

    if (!lv->getPowerSupply()->ping()) {
        logger(logERROR) << " Can not ping LV!";
        logger(logERROR) << " PSINIT failed! :(";
        testSum["passed"] = false;
        logger(logINFO) << "## End test PSINIT ## "
                        << PBv3Utils::getTimeAsString(
                               std::chrono::system_clock::now());
        return testSum;
    }

    if ((!skiphv) && (!hv->getPowerSupply()->ping())) {
        logger(logERROR) << " Can not ping HV!";
        logger(logERROR) << " PSINIT failed! :(";
        testSum["passed"] = false;
        logger(logINFO) << "## End test PSINIT ## "
                        << PBv3Utils::getTimeAsString(
                               std::chrono::system_clock::now());
        return testSum;
    }

    double hv_v_off = 0.0;
    if (!skiphv) hv_v_off = hv->measureVoltage();
    testSum["results"]["HV_V_OFF"] = hv_v_off;
    if (fabs(hv_v_off) < 10.0)
        logger(logINFO) << " --> HV_V_OFF: " << hv_v_off << " V";
    else {
        logger(logERROR) << " --> HV_V_OFF: " << hv_v_off << " V";
        testSum["passed"] = false;
    }

    double hv_i_off = 0.0;
    if (!skiphv) hv_i_off = hv->measureCurrent();
    testSum["results"]["HV_I_OFF"] = hv_i_off;
    if (fabs(hv_i_off) < 1e-5)
        logger(logINFO) << " --> HV_I_OFF: " << hv_i_off << " A";
    else {
        logger(logERROR) << " --> HV_I_OFF: " << hv_i_off << " A";
        testSum["passed"] = false;
    }

    double lv_v_off = lv->measureVoltage();
    testSum["results"]["LV_V_OFF"] = lv_v_off;
    if (fabs(lv_v_off) < 0.5)
        logger(logINFO) << " --> LV_V_OFF: " << lv_v_off << " V";
    else {
        logger(logERROR) << " --> LV_V_OFF: " << lv_v_off << " V";
        testSum["passed"] = false;
    }
    double lv_i_off = lv->measureCurrent();
    testSum["results"]["LV_I_OFF"] = lv_i_off;
    if (fabs(lv_i_off) < 0.1)
        logger(logINFO) << " --> LV_I_OFF: " << lv_i_off << " A";
    else {
        logger(logERROR) << " --> LV_I_OFF: " << lv_i_off << " A";
        testSum["passed"] = false;
    }

    if (testSum["passed"])
        logger(logINFO) << " PSINIT passed! :)";
    else
        logger(logERROR) << " PSINIT failed! :(";

    logger(logINFO) << "## End test PSINIT ## "
                    << PBv3Utils::getTimeAsString(
                           std::chrono::system_clock::now());

    return testSum;
}
}  // namespace PBv3ActiveTools
