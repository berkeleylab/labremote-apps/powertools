#ifndef PBTYPEDEF_H
#define PBTYPEDEF_H

#include <stdexcept>
#include <string>
#include <unordered_map>

/**
 *
 * Define the different PB types
 *
 * EC_R3 has two AMACs, which alternate in position
 * Therefore, EC_R3_0 and EC_R3_1 are specified
 * depending on the positions / PB number.
 *
 */

namespace PBTypeDef {

enum class PBType {
    BARREL = -1,
    EC_R0 = 0,
    EC_R1 = 1,
    EC_R2 = 2,
    EC_R3 = 3,
    EC_R4 = 4,
    EC_R5 = 5
};

//! \brief This is needed if the GCC version is older than 6.1 which is
//! installed on CENTOS7 it seems.
// See also
// https://stackoverflow.com/questions/18837857/cant-use-enum-class-as-unordered-map-key/29618545#29618545
struct PBTypeHash {
    template <typename T>
    std::size_t operator()(T t) const {
        return static_cast<std::size_t>(t);
    }
};

//! \brief Mapping `PBType` to `string` for printout
extern const std::unordered_map<PBType, std::string, PBTypeHash>
    mapTypeToString;

//! \brief Get PADID for PBType
int getPBTypePADID(PBType type, int pbNum);

//! \brief Return true for is primary R3 powerboard, false otherwise
bool isPrimaryR3(PBType type, int pbNum);

}  // namespace PBTypeDef
#endif  // PBTYPEDEF_H
