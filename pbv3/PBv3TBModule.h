#ifndef PBV3TBMODULE_H
#define PBV3TBMODULE_H

#include <memory>

#include "ITSDAQCom.h"
#include "PBv3TB.h"

class PBv3TBModule : public PBv3TB {
 public:
    PBv3TBModule(const std::string &ip = "192.168.222.16",
                 uint32_t port = 60003);
    virtual ~PBv3TBModule();

    /** \brief Configure on JSON object
     *
     * Valid keys:
     *  - `ip`: IP address of the ITSDAQ firmware
     *  - `port`: Port to access the ITSDAQ firmware
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json &config);

    /** \brief Initialize testbench
     *
     * Initializes I2C communication with CAL DAC and creates the
     * AMACv2 object assuming padid 0.
     */
    virtual void init();

    std::shared_ptr<AMACv2> getPB(uint8_t pb);

    bool setOFin(uint8_t pbNum, bool value);

    double getVin();
    double getNTC(uint8_t ntc);
    double getActiveTemp();
    double getActiveHum();
    double getActiveDew();
    void loadOn(uint8_t pbNum);
    void loadOff(uint8_t pbNum);
    double setLoad(uint8_t pbNum, double load);
    double getLoad(uint8_t pbNum);

    double getVout(uint8_t pbNum);
    double getIload(uint8_t pbNum);

    void powerHVOn();
    void powerHVOff();
    double getHVout(uint8_t pbNum);

    double getHVoutCurrent(uint8_t pbNum);

    double readCarrierOutput(uint32_t pbNum, PBv3TB::CARRIER_OUTPUT value);

 private:
    /** \name Configuration
     * @{ */

    //! IP address of the ITSDAQ firmware
    std::string m_ip = "192.168.222.16";
    //! Port to access the ITSDAQ firmware
    uint32_t m_port = 60003;

    //! AMAC padid
    std::vector<uint8_t> m_pb_idpads;
    //! Comm ID
    std::vector<uint8_t> m_pb_commid;

    /** @} */

    // Objects
    std::shared_ptr<ITSDAQCom> m_com;
    std::vector<std::shared_ptr<AMACv2>> m_pbs;
};

#endif  // PBV3TBMODULE_H
