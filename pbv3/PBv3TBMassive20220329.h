#ifndef PBV3TBMASSIVE20220329_H
#define PBV3TBMASSIVE20220329_H

#include "PBv3TBMassive.h"

class PBv3TBMassive20220329 : public PBv3TBMassive {
 public:
    PBv3TBMassive20220329(std::shared_ptr<PowerSupplyChannel> lv,
                          std::shared_ptr<PowerSupplyChannel> hv);
    PBv3TBMassive20220329(std::shared_ptr<EquipConf> hw);
    PBv3TBMassive20220329() = default;
    ~PBv3TBMassive20220329() = default;

    //! Configure on JSON object
    /**
     * Valid keys:
     *  - `zturn_adapter`: True when using the ZTurn adapter
     *  - `invertCMDout`: overwrite invertCMDout, set to true for worker 3 when
     * using the Zturn adapter
     *  - `invertCMDin`: overwrite invertCMDin, normally not needed
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json &config);

    /**
     * In addtion to PBV3TBMassive powerOn:
     *  - enable carrier card power
     */
    virtual void powerTBOn();

    /**
     * In addtion to PBV3TBMassive powerOff:
     *  - disable carrier card power
     */
    virtual void powerTBOff();

    //! Determine if a carrier card is plugged in
    /**
     * Presence of a carrier card is determied by reading
     * the contents of a I2C device (mux ch2, address 0x20)
     * on the card. A failed I2C communication defines a
     * missing carrier card.
     */
    bool checkCarrierCard();

 protected:
    //! Initialize devices on the active board
    virtual void initDevices();

 private:
    //! ZTurn adapter is in use
    bool m_zturn_adapter = false;
    //! overwrite invertCMDout
    int m_invertCMDout = -1;
    //! overwrite invertCMDin
    int m_invertCMDin = -1;
};

#endif  // PBV3TBMASSIVE20220329_H
