#include "PBv3QCFlow.h"

#include "PBv3ConfigTools.h"
#include "PBv3TB.h"
#include "PBv3TestTools.h"
#include "PBv3Utils.h"

using namespace nlohmann;

PBv3QCFlow::PBv3QCFlow(uint8_t num, bool doHV, bool hbipc)
    : m_num(num), m_doHV(doHV), m_hbipc(hbipc) {}

uint8_t PBv3QCFlow::num() const { return m_num; }

std::string PBv3QCFlow::serial() const { return m_serial; }

void PBv3QCFlow::setSerial(const std::string &serial) { m_serial = serial; }

json PBv3QCFlow::runAliveTests(std::shared_ptr<PBv3TB> tb) {
    // Reset test states
    m_passAlive = false;
    m_passCom = false;
    m_passOF = false;

    // Run the tests!
    std::cout << std::endl;
    logger(logINFO) << "### Performing alive tests on powerboard " << m_num
                    << " ###";

    // Output structure
    uint32_t test = 0;
    json diagSum;

    diagSum["config"]["component"] = m_serial;
    diagSum["config"]["institution"] = "LBNL_STRIP_POWERBOARDS";
    diagSum["pbNum"] = m_num;
    diagSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    if (m_hbipc) {
        m_passOF = true;
    } else {
        diagSum["tests"][test++] = PBv3TestTools::testOF(m_num, tb);
        m_passOF = diagSum["tests"][test - 1]["passed"];
    }

    if (!m_passOF) {
        logger(logERROR) << "linPOL for powerboard " << m_num
                         << " is non-operational. Skipping to next board.";

        if (tb->getPBPanelType() == PBTypeDef::PBType::EC_R3 &&
            !PBTypeDef::isPrimaryR3(tb->getPBPanelType(), m_num)) {
            logger(logWARNING)
                << "OF test for secondary R3 AMAC failed. This may be "
                   "because the corresponding primary R3 AMAC has a broken "
                   "OFOut (which is connected to the OFIn of the secondary R3 "
                   "AMAC).";
        }
    }

    diagSum["tests"][test++] = PBv3TestTools::scanPADID(m_num, tb, m_hbipc);

    m_passCom = true;
    if (diagSum["tests"][test - 1]["passed"]) {
        // Init com
        logger(logINFO) << "Init AMAC";
        std::shared_ptr<AMACv2> amac = tb->getPB(m_num);

        // set padid according to AMAC type
        int padid = tb->getPBTypePADID(m_num);
        logger(logDEBUG) << "Using padID " << padid << " for type "
                         << (int)tb->getPBPanelType();
        amac->setPADID(padid);

        amac->init();

        diagSum["tests"][test++] = PBv3TestTools::runBER(amac);
        if (!diagSum["tests"][test - 1]["passed"]) {
            m_passCom = false;
            logger(logERROR)
                << "BER test failed for pb " << m_num
                << ". AMAC read/write not working. Will not perform "
                   "full test on this board.";
        }
    } else {
        m_passCom = false;
        logger(logERROR)
            << "PADID test failed for pb " << m_num
            << ". AMAC read/write not working. Will not perform full "
               "test on this board.";
    }

    // Save output
    diagSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    m_passAlive = true;
    for (const auto &test : diagSum["tests"])
        m_passAlive &= test.value<bool>("passed", false);

    return diagSum;
}

nlohmann::json PBv3QCFlow::runTuning(std::shared_ptr<PBv3TB> tb) {
    m_config.clear();
    m_config["component"] = m_serial;

    if (!m_passCom) {
        logger(logWARNING) << "### Skip tuning of powerboard " << m_num
                           << " ###";
        return m_config;
    }

    logger(logINFO) << "### Tuning powerboard " << m_num << " ###";

    std::shared_ptr<AMACv2> amac = tb->getPB(m_num);

    // Main tuning
    m_config.merge_patch(PBv3ConfigTools::tuneVDDBG(amac, tb->getCalDAC()));
    m_config.merge_patch(PBv3ConfigTools::tuneAMBG(amac, tb->getCalDAC()));
    m_config.merge_patch(PBv3ConfigTools::tuneRampGain(amac, tb->getCalDAC()));
    m_config.merge_patch(PBv3ConfigTools::tuneCur10V(amac));
    m_config.merge_patch(PBv3ConfigTools::tuneCur1V(amac));
    m_config.merge_patch(PBv3ConfigTools::tuneNTC(amac));
    m_config.merge_patch(PBv3ConfigTools::calibrateNTC(amac));
    m_config.merge_patch(PBv3ConfigTools::calibrateTemperature(amac));

    PBv3ConfigTools::configAMAC(amac, m_config, false);
    PBv3ConfigTools::saveConfigAMAC(amac, m_config);
    amac->initRegisters();

    PBv3ConfigTools::decorateConfig(m_config);

    return m_config;
}

json PBv3QCFlow::runBasicTests(std::shared_ptr<PBv3TB> tb) {
    m_passBasic = false;

    // Output structure
    json testSum;
    testSum["pbNum"] = m_num;
    testSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    if (!m_passCom) {
        logger(logWARNING) << "### Skip basic tests for powerboard " << m_num
                           << " ###";
        testSum["time"]["end"] =
            PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
        return testSum;
    }

    // Run tuning if needed
    if (!m_config.contains("properties")) runTuning(tb);
    testSum["config"] = m_config;

    std::cout << std::endl;
    logger(logINFO) << "### Performing basic tests on powerboard " << m_num
                    << " ###";

    std::shared_ptr<AMACv2> amac = tb->getPB(m_num);

    // Output structure
    uint32_t test = 0;

    // The tests
    testSum["tests"][test++] = PBv3TestTools::toggleOutput(m_num, tb, m_hbipc);
    testSum["tests"][test++] = PBv3TestTools::testDCDCAdj(m_num, tb);
    testSum["tests"][test++] = PBv3TestTools::temperature(amac, tb);

    testSum["tests"][test++] = PBv3TestTools::testLvEnable(m_num, tb);
    m_passLV = testSum["tests"][test - 1]["passed"];

    if (m_doHV) {
        testSum["tests"][test++] = PBv3TestTools::testHvEnable(m_num, tb);
        m_passHV = testSum["tests"][test - 1]["passed"];

        // two HVMuxes in this case
        if (tb->getPBPanelType() == PBTypeDef::PBType::EC_R4 ||
            tb->getPBPanelType() == PBTypeDef::PBType::EC_R5) {
            // run test
            json hvEnableHVMux2 = PBv3TestTools::testHvEnable(m_num, tb, 2);

            // result merging
            // account for the fact that this is the second HVMux in the test
            // result and add "HV2" instead of "HV" or "HV0" to result
            PBv3TestTools::accountJSONForHVMuxN(hvEnableHVMux2["results"], 2);

            // update result of previous result
            PBv3Utils::mergeResult(testSum["tests"][test - 1], hvEnableHVMux2,
                                   true);

            // test pass is an AND of the two passings
            m_passHV = testSum["tests"][test - 1]["passed"];
        }
    } else {
        m_passHV = false;
    }

    // Save output
    testSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    m_passBasic = true;
    for (const auto &test : testSum["tests"])
        m_passBasic &= test.value<bool>("passed", false);

    return testSum;
}

nlohmann::json PBv3QCFlow::runAdvancedTests(std::shared_ptr<PBv3TB> tb) {
    // Output structure
    json testSum;
    testSum["pbNum"] = m_num;
    testSum["time"]["start"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    if (!m_passCom) {
        logger(logWARNING) << "### Skip advanced tests for powerboard " << m_num
                           << " ###";
        testSum["time"]["end"] =
            PBv3Utils::getTimeAsString(std::chrono::system_clock::now());
        return testSum;
    }

    // Run tuning if needed
    if (!m_config.contains("properties")) runTuning(tb);
    testSum["config"] = m_config;

    logger(logINFO) << "### Performing advanced tests on powerboard " << m_num
                    << " ###";

    std::shared_ptr<AMACv2> amac = tb->getPB(m_num);

    // Start testing
    uint32_t test = 0;

    // testSum["tests"][test++] = PBv3TestTools::calibVinResponse   (amac,
    // tb->getLVPS()); // CAEN PS can not change Vin values
    if (m_passHV) {
        testSum["tests"][test++] = PBv3TestTools::measureHvSense(m_num, tb);
        // For R4 + R5: run HV-sense measurement for second HVMux
        if (tb->getPBPanelType() == PBTypeDef::PBType::EC_R4 ||
            tb->getPBPanelType() == PBTypeDef::PBType::EC_R5) {
            // run test
            json hvSenseHVMux2 = PBv3TestTools::measureHvSense(m_num, tb, 2);

            // result merging
            // account for the fact that this is the second HVMux in the test
            // result and add "HV2" instead of "HV" or "HV0" to result
            PBv3TestTools::accountJSONForHVMuxN(hvSenseHVMux2["results"], 2);

            // update result of previous result
            PBv3Utils::mergeResult(testSum["tests"][test - 1], hvSenseHVMux2,
                                   true);
        }
    }
    testSum["tests"][test++] =
        PBv3TestTools::measureEfficiency(m_num, tb, 0.2, 0.0, 3.6);
    testSum["tests"][test++] = PBv3TestTools::calibrateAMACoffset(amac, false);
    testSum["tests"][test++] =
        PBv3TestTools::calibrateAMACslope(m_num, tb, 0.01, false);
    testSum["tests"][test++] = PBv3TestTools::calibrateAMACCur10V(m_num, tb, 2);
    testSum["tests"][test++] = PBv3TestTools::calibrateAMACCur1V(m_num, tb, 2);
    testSum["tests"][test++] = PBv3TestTools::rampDAC(
        m_num, tb, "DACShuntx", PBv3TB::SHUNTx, AMACv2::AM::SHUNTX);
    testSum["tests"][test++] = PBv3TestTools::rampDAC(
        m_num, tb, "DACShunty", PBv3TB::SHUNTy, AMACv2::AM::SHUNTY);
    testSum["tests"][test++] = PBv3TestTools::rampDAC(
        m_num, tb, "DACCalx", PBv3TB::CALx, AMACv2::AM::CALX);
    testSum["tests"][test++] = PBv3TestTools::rampDAC(
        m_num, tb, "DACCaly", PBv3TB::CALy, AMACv2::AM::CALY);
    testSum["tests"][test++] = PBv3TestTools::temperatureNTC(amac);
    testSum["tests"][test++] = PBv3TestTools::temperatureCTAT(amac);

    testSum["time"]["end"] =
        PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

    return testSum;
}

bool PBv3QCFlow::didAliveTestPass() { return m_passAlive; }