cmake_minimum_required (VERSION 3.12)

project (powertools)

# The version number.
set (powertools_VERSION_MAJOR 2)
set (powertools_VERSION_MINOR 2)
set (powertools_VERSION_PATCH 0)

# Nicer structure for binary files
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_MACOSX_RPATH 1)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/labremote/cmake")

if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
    message(FATAL_ERROR "GCC version must be at least 4.8!")
  endif()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
  if (CMAKE_CXX_COMPILER_VERSION VERSION_LESS 3.2)
    message(FATAL_ERROR "Clang version must be at least 3.2!")
  endif()
else()
  message(WARNING "You are using an unsupported compiler! Compilation has only been tested with Clang and GCC.")
endif()

set(CMAKE_CXX_STANDARD 11)


# labRemote dependency
SET(OLD_BUILD_DOC ${BUILD_DOC})
SET(BUILD_DOC off) # disable labremote documentation
add_subdirectory(labremote)
SET(BUILD_DOC ${OLD_BUILD_DOC})

# Source code
add_subdirectory(pbv3)
add_subdirectory(scripts)
add_subdirectory(doc)

