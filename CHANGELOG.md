# v2.3.3
- Fix compile issue in firmware
# v2.3.2
- Add an option to specify a tag for a test to be saved in the result json file
- Some end-cap related MRs are also included 
# v2.3.1
- Add support for CPX200DP power supply
# v2.3.0
- Add support for CPX400DP power supply
- Minor improvements for massive QC tests (speed up, adjust thresholds for cold tests, add burn-in, etc.)
# v2.2.0
- Some threshold changes for PPB powerboards.
# v2.1.1
- Fix a bug in get input/output current
- Fix a bug in reading eFuse ID
- Increase range of DCDCiP/N for AMACstar
- Implement faster way to tune DCDCiP/N
# v2.1.0
- Speed up input/output current monitoring tuning
# v2.0.0
- Support for AMACstar. 
- Require an institution argument for mass test.
# v1.10.0
- Update TEMPERATURE test settings and pass ranges.
# v1.9.0
- Add invertCMDin/out settings to PBv3TBMassive20210504 to explicitly set polarity.
# v1.8.0
- Update labRemote tag with installation of tools.
# v1.7.0
- Add powersupply initialization and I2C commuication checks (`pbv3_stand_by`)
# v1.6.1
- Fixed Keithley 24xx current range setting in labRemote
